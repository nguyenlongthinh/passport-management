<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<html>
<head>
	<link rel="shortcut icon" type="image/x-icon" href="<c:url value="/resources/img/logo.png"/>">
	<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/bootstrap.min.css"/>" />
	<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/login.css"/>" />
	<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.css" />
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>Login Page</title>
</head>
<body onload='document.loginForm.username.focus();'>
 <div id="mainWrapper">
	<div class="logo"></div>
		<div class="login-block" style="border-top: 15px solid #00B2EE;">
			<h1>Login</h1>
			
				<c:if test="${not empty error}">
					<div class="error">${error}</div>
				</c:if>
				<c:if test="${not empty msg}">
				<div class="msg">${msg}</div>
				</c:if>
	
			<form name='loginForm' action="<c:url value='/j_spring_security_check' />" method='POST'>
				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	
				 <input type="text" value="" placeholder="Username" id="username" name="username"/>
	   			 <input type="password" value="" placeholder="Password" id="password" name="password" />
				
				<input  name="submit" type="submit" class="btn btn-primary btn-md" style="background-color: #00B2EE;" value="Login" />

			</form>
		</div>
	</div>
</body>
</html>