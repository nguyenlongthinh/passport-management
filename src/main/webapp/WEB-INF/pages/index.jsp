<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="shortcut icon" type="image/x-icon" href="<c:url value="/resources/img/logo.png"/>">
<title>Passport Management</title>
<script type='text/javascript' src='https://code.jquery.com/jquery-1.11.0.min.js'></script>

<link rel="stylesheet" type="text/css"
href="<c:url value="/resources/css/bootstrap.min.css"/>" />
<link rel="stylesheet" href="<c:url value="/resources/css/bootstrap-tagsinput.css"/>">
<script src="<c:url value="/resources/js/jquery-2.2.4.min.js"/>"></script>
<script src="<c:url value="/resources/js/bootstrap-tagsinput-angular.js"/>"></script>
<script src="<c:url value="/resources/js/bootstrap-tagsinput-angular.min.js"/>"></script>
<script src="<c:url value="/resources/js/typeahead.bundle.js"/>"></script>
<script src="<c:url value="/resources/js/bloodhound.js"/>"></script>
<script src="<c:url value="/resources/js/typeahead.bundle.min.js"/>"></script>
<script src="<c:url value="/resources/js/typeahead.jquery.js"/>"></script>

<script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>
<script src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>

 <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/jquery.steps.css"/>" />
<link rel="stylesheet" href="<c:url value="/resources/css/ppmagage.css"/>">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="<c:url value="/resources/js/jquery.steps.min.js"/>"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="<c:url value="/resources/js/jquery-ui.js"/>"></script>
<script src="<c:url value="/resources/js/functions.js"/>"></script>
<!-- Form Ajax -->
<script src="<c:url value="/resources/js/jquery.form.min.js"/>"></script>
<meta name="_csrf" content="${_csrf.token}"/>

<!-- default header name is X-CSRF-TOKEN -->
<meta name="_csrf_header" content="${_csrf.headerName}"/>
<link rel="stylesheet" href="<c:url value="/resources/css/jquery.datepick.css"/>">
<script src="<c:url value="/resources/js/jquery.plugin.min.js"/>"></script>
<script src="<c:url value="/resources/js/jquery.datepick.js"/>"></script>
<script src="<c:url value="/resources/js/checkvalue.js"/>"></script>
<script src="<c:url value="/resources/js/information.js"/>"></script>
<script src="<c:url value="/resources/js/datepicker.js"/>"></script>

<!--formden.js communicates with FormDen server to validate fields and submit via AJAX -->
<script type="text/javascript" src="https://formden.com/static/cdn/formden.js"></script>

<!-- Include Date Range Picker -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
<link rel="stylesheet" href="<c:url value="/resources/css/loadpage.css"/>">
<link rel="stylesheet" href="<c:url value="/resources/css/searchinput.css"/>">
<link rel="stylesheet" href="<c:url value="/resources/css/page.css"/>">
<link rel="stylesheet" href="<c:url value="/resources/css/columnShowHide.css"/>">

<style type="text/css">
.editRow{
	position:absolute;
    display:block;
    left:0px;
    top:0px;
    height:24px;
    width: 55px;
    padding:0;
    margin:0;
    border:1px #ddd;
    background-color: white;
    font-weight:normal;
    white-space:nowrap;
}
#editRow{
    display:none;
    z-index:100;
}
</style>
<script src="<c:url value="/resources/js/bootstraptaginput.js"/>"></script>

</head>
<div id="container">
<header>
 <!-- Modal edit information -->
  <div class="modal fade" id="editInfoModal" role="dialog">
    <div class="modal-dialog" style="margin-top: 10%;">    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
         	<h4 class="modal-title" id="myModalLabel" style="margin-left:12%;font-weight:bold; color: red; font-size:25px;">Edit Infomation</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
                      <div class="col-lg-6">
                      <label style="font-size:12px">Personal information:</label> <br>     
                                <div class="form-group">                                            
                                  <input type="text" id="editId" class="form-control fontsize" disabled="" placeholder="Employee ID" name="id" required="required"/>
                              <span id="erroreditId"></span>
                              </div>
                              
                              <div class="form-group">
                                      <input type="text" id="editName" class="form-control fontsize" name="name" placeholder="Name" required="required"/>
                                      <span id="erroreditName"></span>
                              </div>
                              
                              <div class="form-group">
                                  <input type="text" id="editDOB" name="dob" class="form-control fontsize" placeholder="Date of birth" required="required" />
                                  <span id="erroreditDOB"></span>
                              </div>
                              
                              <div class="form-group">
                                  <input class="form-control fontsize" type="email" id="editEmail" name="email" placeholder="Email" required="required"  />
                                  <span id="erroreditEmail"></span>
                              </div>
                              
                               <div class="form-group fontsize" style="font-family: sans-serif;">
                                    <input class="form-control fontsize"  type="text" id="editEmailPm" name="emailpm" placeholder="Alert Recipients"  data-role="tagsinput"/>
                                    <span id="erroreditEmailPm"></span>
                                </div>
                                
                                <div class="form-group">
                                    <input name ="project" type="text" name="city" list="cityname" id="editProject" placeholder="Project" class="form-control fontsize" />
									<datalist id="cityname">
									  <option value="Accedian Embedded">
									  <option value="Accedian EMS">
									  <option value="Accedian SQA">
									  <option value="AOS Dev">
									  <option value="AOS SQA">
									  <option value="AxS APA">
									  <option value="AxS DevOne">
									  <option value="AxS DevTwo">
									  <option value="AxS Modelling">
									  <option value="AxS NG QA">
									  <option value="AxS Plano">
									  <option value="AxS QA">
									  <option value="BOD">
									  <option value="BOD - DC14">
									  <option value="BOD - DC15">
									  <option value="BOD -DC9">
									  <option value="ClearOne">
									  <option value="DC9">
									  <option value="DC9 Resource Pool">
									  <option value="DC9 TSG/TA Data">
									  <option value="DevOps">
									  <option value="DG4 Resources">
									  <option value="Disruptive Firmware">
									  <option value="Disruptive Software - DC14">
									  <option value="Embrionix">
									  <option value="EPP">
									  <option value="ICON DevTwo">
									  <option value="ICON SQA">
									  <option value="IoT Dev Group">
									  <option value="ITK Embedded Dev">
									  <option value="Omena">
									  <option value="OmniVista Development">
									  <option value="OmniVista DevOps">
									  <option value="OmniVista QA">
									  <option value="Onix">
									  <option value="Sensolus">
									  <option value="Text Integration">
									  <option value="VitalQIP AMS">
									  <option value="VitalQIP CE">
									  <option value="VitalQIP Development">
									  <option value="VitalQIP QA">
									</datalist>
                                    <span id="erroreditProject"></span>
                                </div>
                      </div>                          
                      <div class="col-lg-6">
                       <label style="font-size:12px">Passport:</label> <br>                         
                          <div class="form-group">                             
                              <input type="text" class="form-control fontsize" name = "passport" id="editPassport" placeholder="Passport number"/>
                              <span id="erroreditPassport"></span>
                          </div>
                          <div class="form-group">                           
                               <input class="form-control fontsize" name="issuedate" type="text" id="editIssue" placeholder="Issue date" />
                               <span id="erroreditIssue"></span>
                          </div>
                          <div class="form-group">                           
                               <input class="form-control fontsize" name="expiredate" type="text" id="editExpire" placeholder="Expire date" />
                               <span id="erroreditExpire"></span>
                          </div>
                          <div class="form-group">                           
                               <input class="form-control fontsize" name="remainingdays" disabled="" type="text" id="editRemaining" placeholder="Remaining days" />
                               <span id="erroreditRemaining"></span>
                          </div>
                          <div class="form-group">                           
                               <input class="form-control fontsize" name="snooze" type="number" min="0" id="editSnooze" placeholder="Snooze days" />
                               <span id="erroreditSnooze"></span>
                          </div>
                          <label style="font-size:12px">Image:</label> <br>     
                          <div class="form-group changepointer" id="editImage"> 
                          </div>                
                </div>
        </div>
        <div class="modal-footer">
          <button id="submitInfo" type="button" class="btn btn-default" data-dismiss="modal">Submit</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<nav style="background-color: #00B2EE;">
  <nav >
       <div class="pull-right" style="margin-right:1%;margin-top:0.5%;" >           
              <div class="dropdown" >
                      <button class="btn btn-default dropdown-toggle btn-responsive" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                     
                        <c:if test="${pageContext.request.userPrincipal.name != null}">
                            ${pageContext.request.userPrincipal.name}
                        </c:if>
                      </button>
                      <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="dLabel"  style="position: absolute; z-index:1000;">                      
                      <li ><a href="#" data-toggle="modal" data-target="#myModal">Change password</a></li>
                      <li><a href="<c:url value="/logout" />">Logout</a></li>
                      </ul>
              </div>
            </div>
            <div class="pull-left" style="margin-left:1%;margin-top:5px;" onclick="location.reload(true);">           
              <img src="<c:url value="/resources/img/logo.png"/>" style="height:42; width:42;">
            </div>
  </nav>
<nav class="navbar" style="background-color: #00B2EE;">
    <div class="navbar-header" style="margin: auto;">     
        <span style="color:white;text-align:center;word-spacing:5px;font-size:20px;"><b>DG4 PASSPORT MANAGEMENT SYSTEM</b></span> 
    </div>
</nav>
</nav>

</header>
 
<div id="fountainG" style="margin-top:20%;" class="cssload-loader">Loading</div>
<div id="body" style="display:none;">
<body >
<div class="container-fluid" >
<!--notification alert -->
          <div id="result" class="clearfix"></div>
        <div class="row"
            style="margin-top: 0%; font-size: 11.5px; font-weight: bold; margin-bottom: -1px; height: auto">
            <div id="showCreate" class="inputprofile fontsize" style="margin-left:-1%; float: left; max-width:325px;">
            
                    <div class="panel-body">
                        <div class="card-block">
                            <form method="POST" id="add-new-task" enctype="multipart/form-data" action="<%=request.getContextPath()%>/insert"  acceptcharset="UTF-8" > 
                                <div id="wizard" style="max-width: 70%;min-width: 285px;">
                                    <div class="form-group" >
                                        <fieldset class="scheduler-border">
                                          <legend class="scheduler-border" style="color:#00B2EE;margin-bottom:2%;">Create new profile 
                                          <input type="button" title="Hide 'Create New Profile'" id="buttonShowHide" class="btn btn-primary buttonShowHide btn-xs" onclick="showCreate()" value="&rtrif;&rtrif;&rtrif;">
                                          </legend>
                                              <div class="form-group" style="margin-top:-3%;" >                                            
                                                <input type="text" id="id" class="form-control fontsize SubmitForm" placeholder="Employee ID" name="id" required="required"/>
                                            <span id="errorid"></span>
                                            </div>
                                            
                                            <div class="form-group">
                                                    <input type="text" id="name" class="form-control fontsize SubmitForm" name="name" placeholder="Name" required="required"/>
                                                    <span id="errorname"></span>
                                            </div>
                                            
                                            <div class="form-group">
                                                <input type="text" id="datepicker1" name="dob" class="form-control fontsize SubmitForm" placeholder="Date of birth" required="required" />
                                                <span id="errordatepicker1"></span>
                                            </div>
                                            
                                            <div class="form-group">
                                                <input class="form-control fontsize SubmitForm" type="email" id="testemail" name="email" placeholder="Email" required="required"  />
                                                <span id="erroremail"></span>
                                            </div>
                                            
                                             <div class="form-group fontsize" style="font-family: sans-serif;">
                                                <input class="form-control fontsize SubmitForm"  value="" type="text" id="testemailpm" name="emailpm" placeholder="Alert Recipients"  data-role="tagsinput"/>
                                                <span id="erroremailpm"></span>
                                            </div>
                                            
                                            <div class="form-group">
                                                <input name ="project" type="text" name="city" list="cityname" id="project" placeholder="Employee project" class="form-control fontsize SubmitForm" />
												<datalist id="cityname">
												  <option value="Accedian Embedded">
												  <option value="Accedian EMS">
												  <option value="Accedian SQA">
												  <option value="AOS Dev">
												  <option value="AOS SQA">
												  <option value="AxS APA">
												  <option value="AxS DevOne">
												  <option value="AxS DevTwo">
												  <option value="AxS Modelling">
												  <option value="AxS NG QA">
												  <option value="AxS Plano">
												  <option value="AxS QA">
												  <option value="BOD">
												  <option value="BOD - DC14">
												  <option value="BOD - DC15">
												  <option value="BOD -DC9">
												  <option value="ClearOne">
												  <option value="DC9">
												  <option value="DC9 Resource Pool">
												  <option value="DC9 TSG/TA Data">
												  <option value="DevOps">
												  <option value="DG4 Resources">
												  <option value="Disruptive Firmware">
												  <option value="Disruptive Software - DC14">
												  <option value="Embrionix">
												  <option value="EPP">
												  <option value="ICON DevTwo">
												  <option value="ICON SQA">
												  <option value="IoT Dev Group">
												  <option value="ITK Embedded Dev">
												  <option value="Omena">
												  <option value="OmniVista Development">
												  <option value="OmniVista DevOps">
												  <option value="OmniVista QA">
												  <option value="Onix">
												  <option value="Sensolus">
												  <option value="Text Integration">
												  <option value="VitalQIP AMS">
												  <option value="VitalQIP CE">
												  <option value="VitalQIP Development">
												  <option value="VitalQIP QA">
												</datalist>
                                                <span id="project"></span>
                                            </div>
                                        
                                   <label style="font-size:12px">Passport:</label> <br>                                       
                                        <div class="form-group">                                           
                                            <input type="text" class="form-control fontsize SubmitForm" name="passport" id="passport" placeholder="Passport number"/>
                                            <span id="errorpassport"></span>
                                        </div>
                                        <div class="form-group">                                         
                                             <input class="form-control fontsize SubmitForm" name="isuedate" type="text" id="datepicker" placeholder="Issuedate" />
                                             <span id="errordatepicker"></span>
                                        </div>
                                        <div class="form-group" >
                                            <span>Image</span> <input onchange="checkFile()" type="file" name="image" id="image">
                                        </div>
                              <div class="pull-right" >
                                   <input type="reset" id="reset" class="btn btn-sm" value="Reset" style="background-color:#00B2EE;color:white"> 
                                   <input class="btn btn-sm" type="button" onclick="Insert()" value="Submit" style="background-color:#00B2EE;color:white;width:60">  
                              </div>
                                <div class="form-group" style="padding-bottom:4%" >
                               <br/></div>
                               </fieldset>
                              </div>
                                </div>
                                
                           </form>
                           <br/><br/><br/>
                              <div id="wizard" style="max-width: 70%;min-width: 285px;">
                                <fieldset class="scheduler-border">
                                    <legend class="scheduler-border" style="color:#00B2EE;margin-bottom:2px;">Setting
                                    </legend>
                                    <div class="form-group">
                                        <label for="passport" >Alert when </label>
                                        <input type="number" id="expTime" min="1" max="3650" style="width:22%;min-width: 60px;" />days until expiration
                                        <a href="" ><span id="btnExp" class="glyphicon glyphicon-floppy-disk" style="padding-left:1%"></span></a>
                                    </div>
                                    <div class="form-group">
                                        <label for="snooze" style="margin-top: 7;">Alert snooze period:</label>
                                        <input type="number" id="snooze" min="1" max="365" style="margin-top: -6px; width:22%;min-width: 60px;" value="10" /> day(s)
                                        <a href=""><span id="btnSnooze" class="glyphicon glyphicon-floppy-disk" style="padding-left:3%"></span></a>
                                    </div>
                                </fieldset>
                                </div>
                        </div>
                    </div>
              
            </div>
            <div id="contentTable" class="" style="width:calc(100% - 325px);">                   
					<div id=showTable>
                </div>
                <!-- LINK EXPORT -->
                  <!-- Modal -->
               <div class="modal fade" id="myModal1" role="dialog">
                    <div class="modal-dialog" style="margin-top: 10%;">                       
                      <!-- Modal content-->                         
                      <div class="modal-content">
                      <div class="modal-header">
                       	 <h4 class="modal-title" id="myModalLabel" style="margin-left:12%;font-weight:bold; color: red; font-size:25px;">Import new Excel file</h4>
                         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      </div>
                        <div class="modal-body">
                        
                          <form action="<%= request.getContextPath()%>/uploadFile" method="POST" enctype="multipart/form-data" id="importCSV" name="${_csrf.parameterName}"
                   value="${_csrf.token}">
                            <div class="form-group" style="margin-left: 20%;">                                      
                                <input id="csv" style="margin-bottom:5%" type="file" name="file1" onchange="checkLoadCSV()"/>
                                <button id="uploadCSV" type="button" class="btn btn-primary"  style="margin-left:25%" data-dismiss="modal" id="upload" onclick="getLoadCSV()">Upload</button>
                            </div>
                        </form>
                        </div>
                      </div>                             
                    </div>
                </div>
           <!-- MODAL CHANGE PASSWORD -->
                       <div class="modal fade" id="myModal" role="dialog" >
                         <div class="modal-dialog" style="margin-top: 10%;">
                           <div class="modal-content">
                             <div class="modal-header">
                               <h4 class="modal-title" id="myModalLabel" style="margin-left:12%;font-weight:bold; color: red; font-size:25px;">Change Password</h4>
                             <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                             </div>
                             <div class="modal-body">                              
								<form action="<%=request.getContextPath() %>/changepassword" method="POST" id="changepass" name="${_csrf.parameterName}"
								          value="${_csrf.token}" style="width:35%;margin-left:14%;">              
							          <div class="form-group" >
							              <label>Username:</label>
							              <input type='text' name='username' class="form-control fontsize" value="${pageContext.request.userPrincipal.name}" disabled style="width:200%">
							              <input type='hidden' id="username" name='username' class="form-control fontsize " value="${pageContext.request.userPrincipal.name}">
							          </div>
							          <div class="form-group">
							                  <label>Old Password:</label>
							                  <input id="oldpass" type='password' name='password'  class="form-control fontsize" style="width:200%" required="required"/>
							                  <span id="erroroldpass" ></span>								                
							          </div>
							          <div class="form-group">
							                  <label>New Password:</label>
							                  <input type='password' id="newpass" name='newpassword'  class="form-control fontsize"  style="width:200%" required="required"/>								                
							          </div>
							          <div class="form-group">
							                      <label>Retype Password:</label>
							                  <input type='password' name='renewpassword' id="renewpass" class="form-control fontsize" style="width:200%" required="required" onchange="validate()" />
							                  <div id="errorrepass"></div>
							          </div> 
							          <div class="form-group" style="align:center">
							              	  <input onclick="ChangePass()" data-dismiss="modal" class="btn btn-primary" value="Save" style="width:35%;margin-left:50%;"  >
							                  <button type="button" class="btn btn-default" data-dismiss="modal" style="margin-left:90%;margin-top:-20.5%">Close</button>
							          </div>
							      </form>                  
						     </div>
			           </div>
			         </div>
			       </div>
            <div id="dvData" style="margin-left: 25px;">
            <div class="pull-left fontsize">
				<button  title="Show 'Create New Profile'" id="buttonHere" style="display:none; margin-right: 15px; margin-top:10px;" class="btn btn-primary btn-xs" onclick="showCreate()">&ltrif;&ltrif;&ltrif;</button>
			</div>
             <div class="pull-left fontsize col-lg-4" style="min-width: 260px;">
              <label>Filter:</label>
                  <select id="selectbox" name="choose" style="width:75%;height:40px;" >
                      <option value="nofilter">-----All-----</option>
                      <option value="nopassport">No passport profile</option>
                      <option value="expiredate">Expire date</option>
                      <option value="yearonly">DOB with year only</option>
                  </select>
            </div>
             <div class="pull-right fontsize col-lg-7" >                   
              	<div id="search-input" style="width:45%; float:left">
              		<input required class="form-control clearable" style="margin-left: 30px;" onfocus="clearText()" type="text" id="myInput" onkeyup="searchTable()" placeholder="Search for name, email, project ..." title="Type in a name, email, project...">
   				   <img id="clearButton" class="close-icon"  onclick="clearAll()" src="<c:url value="/resources/img/clear.png"/>">
              	</div>
              	              	<div style="width:10%; float: right;">
					<div class="btn-group">
						  <button title="Show/Hide Column(s)" style="border-color:white;" type="button" class="btn btn-default btn-sm" data-toggle="dropdown">
							   <img id="" width="24px" height="24px"  src="<c:url value="/resources/img/config-icon2.png"/>">
						  </button>
					  <ul class="dropdown-menu" style="min-width: 290px; margin: 30px -250px;">
					      <ul class="list-unstyled col-sm-6" id="configDrop1">
					          <li><a href="#" class="small" data-value="option1" tabIndex="-1"><input checked="checked" onclick="configTable(1,'col1')" type="checkbox"/>&nbsp;ID</a></li>
							  <li><a href="#" class="small" data-value="option2" tabIndex="-1"><input checked="checked" onclick="configTable(2,'col2')" type="checkbox"/>&nbsp;Name</a></li>
							  <li><a href="#" class="small" data-value="option3" tabIndex="-1"><input checked="checked" onclick="configTable(3,'col3')" type="checkbox"/>&nbsp;DOB</a></li>
							  <li><a href="#" class="small" data-value="option4" tabIndex="-1"><input checked="checked" onclick="configTable(4,'col4')" type="checkbox"/>&nbsp;Email</a></li>
							  <li><a href="#" class="small" data-value="option5" tabIndex="-1"><input checked="checked" onclick="configTable(5,'col5')" type="checkbox"/>&nbsp;Recipients</a></li>
							  <li><a href="#" class="small" data-value="option6" tabIndex="-1"><input checked="checked" onclick="configTable(6,'col6')" type="checkbox"/>&nbsp;Project</a></li>
					      </ul>
					      <ul class="list-unstyled col-sm-6" id="configDrop2">
					          <li><a href="#" class="small" data-value="option1" tabIndex="-1"><input checked="checked" onclick="configTable(7,'col7')" type="checkbox"/>&nbsp;Passport No.</a></li>
							  <li><a href="#" class="small" data-value="option2" tabIndex="-1"><input checked="checked" onclick="configTable(8,'col8')" type="checkbox"/>&nbsp;Issue date</a></li>
							  <li><a href="#" class="small" data-value="option3" tabIndex="-1"><input checked="checked" onclick="configTable(9,'col9')" type="checkbox"/>&nbsp;Expire date</a></li>
							  <li><a href="#" class="small" data-value="option4" tabIndex="-1"><input checked="checked" onclick="configTable(10,'col10')" type="checkbox"/>&nbsp;Remaining days</a></li>
							  <li><a href="#" class="small" data-value="option5" tabIndex="-1"><input checked="checked" onclick="configTable(11,'col11')" type="checkbox"/>&nbsp;Snooze</a></li>
							  <li><a href="#" class="small" data-value="option6" tabIndex="-1"><input checked="checked" onclick="configTable(12,'col12')" type="checkbox"/>&nbsp;Image</a></li>
					      </ul>
					  </ul>
					  </div>
				</div>    
              	<div style="width:35%;float:right;margin-top: 10px;">
              		<span class="tam"><a href="" class="" id="import" data-toggle="modal" data-target="#myModal1">Import</a> </span>/          
                   <span class="tam">  <a href="" id="exportFile" name="${_csrf.parameterName}" value="${_csrf.token}">Export</a> </span>/                 
                   <span class="tam">  <a href="" class="" id="exportAll">Export All</a></span>
              	</div>                                
                </div>
                <div id="editRow" class="editRow btn-lg">
                	<a id="editInfo" title="Edit information" style="margin-right: 15%">
   	   				   <img data-toggle="modal" data-target="#editInfoModal" style="width:24px; height:24px;" src="<c:url value="/resources/img/pencil-edit-button.png"/>">
                	</a>  
                	<a id="deleteInfo" data-toggle="modal" data-target="#modaldelete" title="Delete" style="margin-right: 3%">
   	   				   <img style="width:24px; height:24px;" src="<c:url value="/resources/img/rubbish-bin-delete-button.png"/>">
                	</a>
                </div>
                <menu id="ctxMenu" class="form_wrapper">
	                <div class="dropdown"> <button class="dropbtn">Hide</button></div>
	                <br />
	                <div class="dropdown" style="display:none">
					  <button class="dropbtn">Show</button>
					  <div class="dropdown-content" id=showMenu>
					  </div>
					</div>
			    </menu>
                <br/><br/><br/>
                <!-- LINK RELOAD TABLE -->             
                <div  style="overflow:auto;">
                <table class="table table-bordered col-md-12 fontsize" cellpadding="0"
                    cellspacing="0" id="datatable" >
                    <thead>
                        <tr class="changepointer" style="font-weight:bold; background-color:#caeffb;color:#215888 ;font-size:13px;">
                            <th id="col1" onmousedown="mouseDown(event, 'col1')" style="text-align: center;" onclick="sort(0)">ID&nbsp; &nbsp;<i class="fa fa-sort"></i></th>
                            <th id="col2" onmousedown="mouseDown(event, 'col2')" style="text-align: center;" onclick="sort(1)">Name&nbsp; &nbsp;<i class="fa fa-sort"></i></th>
                            <th id="col3" onmousedown="mouseDown(event, 'col3')" style="text-align: center;" onclick="sort(2)">DOB&nbsp; &nbsp;<i class="fa fa-sort"></i></th>
                            <th id="col4" onmousedown="mouseDown(event, 'col4')" style="text-align: center;" onclick="sort(3)">Email&nbsp; &nbsp;<i class="fa fa-sort"></i></th>
                            <th id="col5" onmousedown="mouseDown(event, 'col5')" style="text-align: center;" onclick="sort(4)">Recipients&nbsp; &nbsp;<i class="fa fa-sort"></i></th>
                            <th id="col6" onmousedown="mouseDown(event, 'col6')" style="text-align: center;" onclick="sort(5)">Project&nbsp; &nbsp;<i class="fa fa-sort"></i></th>
                            <th id="col7" onmousedown="mouseDown(event, 'col7')" style="text-align: center;" onclick="sort(6)">Passport No.&nbsp; &nbsp;<i class="fa fa-sort"></i></th>
                            <th id="col8" onmousedown="mouseDown(event, 'col8')" style="text-align: center;" onclick="sort(7)">Issue date&nbsp; &nbsp;<i class="fa fa-sort"></i></th>
                            <th id="col9" onmousedown="mouseDown(event, 'col9')" style="text-align: center;" onclick="sort(8)">Expire date&nbsp; &nbsp;<i class="fa fa-sort"></i></th>
                            <th id="col10" onmousedown="mouseDown(event, 'col10')" style="text-align: center;" onclick="sort(9)">Remaining days&nbsp; &nbsp;<i class="fa fa-sort"></i></th>
                            <th id="col11" onmousedown="mouseDown(event, 'col11')" style="text-align: center;" onclick="sort(10)">Snooze&nbsp; &nbsp;<i class="fa fa-sort"></i></th>
                            <th id="col12" onmousedown="mouseDown(event, 'col12')" style="text-align: center; cursor: default;" >Image</th>
                            <th style="text-align: center; cursor: default; display: none;" >Action</th>
                        </tr>
                    </thead>
                    
                    <tbody id="content">
                    </tbody>
                </table>
                </div>
                <div>  
                	<div class="row pull-left fontsize Page navigation example" >
                		<span style="padding:5px 5px 5px 25px;margin: 20px 0;">Show:</span>
                        <select class="fontsize" id="itemperpage" style="margin: 20px 0;" name="choose" >
                            <option value="5" selected="selected">5 items</option>
                            <option value="10">10 items</option>
                            <option value="25">25 items</option>
                            <option value="50">50 items</option>
                            <option value="100">100 items</option>
                        </select>
<!--                    <span id="previous" style="padding:5px 5px 5px 25px"><span class="tam"><a>&#171 Previous</a></span></span>
 -->                    <span style="padding:5px 5px 5px 0px; margin: 20px 0;"> Page</span><input class="fontsize" id="checkpage" name="checkpage"  min="1" style="width:50px;max-height:100px;margin: 20px 0;" />                       
                        <div id="pageNavPosition" style="padding: 5px 5px 5px 5px; margin: 20px 0;">  </div>
<!--                    <span id="next" style="padding:5px 5px 5px 0px">|<span class="tam"> <a>Next &#187</a></span></span>			
-->
                	</div>              
                    <div class="row pull-right fontsize Page navigation example" >                        
                    	<nav aria-label="Page navigation example">
						  <ul class="pagination justify-content-end">
						    <li class="page-item disabled"  id="previousPage" onclick="previousPage();">
						      <a class="page-link" href="#" tabindex="-1">Previous</a>
						    </li>
						    <li class="page-item active" id="listPage1"  onclick="clickHere(1);"><a  href="#">1</a></li>
						    <li class="page-item" id="listPage2" onclick="clickHere(2);"><a  href="#">2</a></li>
						    <li class="page-item" id="listPage3" onclick="clickHere(3);"><a  href="#">3</a></li>
						    <li class="page-item" onclick="nextPage();" id="nextPage">
						      <a class="page-link" href="#">Next</a>
						    </li>
						    <li class="page-item" onclick="lastPage();" id="lastPage">
						      <a class="page-link" href="#" style="color: #215888;">Last</a>
						    </li>
						  </ul>
						</nav>
                    </div>                 
      			</div>
                </div>
                
      <!-- modal show image -->
          <div class="modal fade" id="modalimg" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" style="margin-top: 10%;" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                        <div class="col-sm-10"><h5 id="title" class="modal-title" id="exampleModalLabel"></h5></div>
                        <div class="col-sm-2"><button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                        </div>
                   </div>
                  <div class="modal-body" id="imgct" style="text-align: center">
                  </div>
                  <div class="modal-footer">
                  </div>
                </div>
              </div>
         </div>
         
          <!-- Modal DELETE -->
            <div class="modal fade" id="modaldelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                     <div class="col-sm-10"><h5 class="modal-title" id="exampleModalLabel">Delete Confirm</h5></div>
                     <div class="col-sm-2"><button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button></div>
                  </div>
                  <div class="modal-body">
                    <div class="col-sm-10">
                    <span style="font-size:14px">Are you sure delete id: <span style="color:green;" id ="setid"></span></span>
                    <input id ="index" type="hidden">
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" id = "btdelete" data-dismiss="modal"  onclick="deleteUser()" class="btn btn-primary">Yes</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                  </div>
                </div>
              </div>
            </div>
            </div>
  </div>  
</body>
</div>
<br/>
<br>
<br>
<br>
<br>
<br>
<br>

<div id="footer">
    <footer  style="background-color: #caeffb; position:absolute;bottom:0px; width:100% " >
    <br/>
      <span  style="color:#111111;text-align:center;font-size:12px;"><center>&copy; 2017 <span>DG4. </span>All Right Reserved</center></span> 
      <span  style="color:#111111;text-align:center;font-size:12px;"><center>Contact Support: paphuc@tma.com.vn | dttbinh@tma.com.vn</center></span> 
    <br/>
    </footer>
</div>
</div>
<script type="text/javascript">
		
      // Export file csv to xls (excel)
    $("#exportFile").click(function(e) {
            var rows = [];
            $('#datatable tr:visible:gt(0)').each(function(index, tr) {//get all row visible but not first
               var lines = $('td', tr).not('td:last').map(function(index, td) {
                    return $(td).text();
                });
             
               rows.push(JSON.stringify(lines.get()));
            });

            if(rows.length <=0)
            {
                alertBox('No data export','danger');
            } else if(rows.length==1)
            {
                var token = 1;//case export le 1 row
            } else
            {
                var token = 2;//case export gt 1 row
            }
            console.log(123)
            var token1 = $("meta[name='_csrf']").attr("content");
            var header = $("meta[name='_csrf_header']").attr("content");
            //$(document).ajaxSend(function(e, xhr, options) {
            //    xhr.setRequestHeader(header, token);
            //});
            console.log(document.getElementById('exportFile').getAttribute('value'), header)
            $.ajax({
                        url : 'exportfile',
                        type : 'POST',
                      
                        data : {
                            datas : rows,
                            token : token,
                            _csrf : document.getElementById('exportFile').getAttribute('value')
                        },
                        success : function(data) {
                        if(data == "success")
                            {
                               window.location.href = "<%=request.getContextPath() %>/resources/excel/user.xls";
                            }
                        }
                    });
            return false;
        });
  
    //export all
    $("#exportAll").click(
            function(e) {
                $.ajax({
                            url : 'exportFileAll',
                            type : 'GET',
                            success : function(data) {
                            if(data == "success")
                                {
                                   window.location.href = "<%=request.getContextPath() %>/resources/excel/user.xls";
                                }
                            }
                        });
                return false;
            });
    
</script>
<script src="<c:url value="/resources/js/paging.js"/>"></script>
</html>
