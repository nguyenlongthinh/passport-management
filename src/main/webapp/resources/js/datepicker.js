//create new profile
$(function() {
        $("#datepicker1").datepicker({
            format: 'mm/dd/yyyy',
            container: container,
            todayHighlight: true,
            autoclose: true,
        })
        $('#datepicker1').datepicker('setDate', '01/01/1990')
        $('#datepicker1').val('')
        .on('hide', function () {
		  if (!this.firstHide) {
		    if (!$(this).is(":focus")) {
		      this.firstHide = true;
		      // this will inadvertently call show (we're trying to hide!)
		      this.focus(); 
		    }
		  } else {
		    this.firstHide = false;
		  }
		})
		.on('show', function () {
		  if (this.firstHide) {
		    // careful, we have an infinite loop!
		    $(this).datepicker('hide'); 
		  }
		});
    });

$(function() {
        $("#datepicker").datepicker({
            format: 'mm/dd/yyyy',
            container: container,
            todayHighlight: true,
            autoclose: true,
        })
        .on('hide', function () {
		  if (!this.firstHide) {
		    if (!$(this).is(":focus")) {
		      this.firstHide = true;
		      // this will inadvertently call show (we're trying to hide!)
		      this.focus(); 
		    }
		  } else {
		    this.firstHide = false;
		  }
		})
		.on('show', function () {
		  if (this.firstHide) {
		    // careful, we have an infinite loop!
		    $(this).datepicker('hide'); 
		  }
		});
        $("#recipient-dob").datepicker({
            format: 'mm/dd/yyyy',
            container: container,
            todayHighlight: true,
            autoclose: true,
        }).on('hide', function () {
  		  if (!this.firstHide) {
  		    if (!$(this).is(":focus")) {
  		      this.firstHide = true;
  		      // this will inadvertently call show (we're trying to hide!)
  		      this.focus(); 
  		    }
  		  } else {
  		    this.firstHide = false;
  		  }
  		})
  		.on('show', function () {
  		  if (this.firstHide) {
  		    // careful, we have an infinite loop!
  		    $(this).datepicker('hide'); 
  		  }
  		});
        $("#recipient-issue").datepicker({
            format: 'mm/dd/yyyy',
            container: container,
            todayHighlight: true,
            autoclose: true,
        }).on('hide', function () {
  		  if (!this.firstHide) {
  		    if (!$(this).is(":focus")) {
  		      this.firstHide = true;
  		      // this will inadvertently call show (we're trying to hide!)
  		      this.focus(); 
  		    }
  		  } else {
  		    this.firstHide = false;
  		  }
  		})
  		.on('show', function () {
  		  if (this.firstHide) {
  		    // careful, we have an infinite loop!
  		    $(this).datepicker('hide'); 
  		  }
  		});
    });

//DATA PICKER:
$(function() {
    $("#datepicker").datepicker({
        format: 'mm/dd/yyyy',
        container: container,
        todayHighlight: true,
        autoclose: true,
    }).on('hide', function () {
		  if (!this.firstHide) {
		    if (!$(this).is(":focus")) {
		      this.firstHide = true;
		      // this will inadvertently call show (we're trying to hide!)
		      this.focus(); 
		    }
		  } else {
		    this.firstHide = false;
		  }
		})
		.on('show', function () {
		  if (this.firstHide) {
		    // careful, we have an infinite loop!
		    $(this).datepicker('hide'); 
		  }
		});
});

//edit new profile
$(function() {
        $("#editDOB").datepicker({
            format: 'mm/dd/yyyy',
            container: container,
            todayHighlight: true,
            autoclose: true,
        })
        $('#editDOB').datepicker('setDate', '01/01/1990')
        $('#editDOB').val('')
        .on('hide', function () {
		  if (!this.firstHide) {
		    if (!$(this).is(":focus")) {
		      this.firstHide = true;
		      // this will inadvertently call show (we're trying to hide!)
		      this.focus(); 
		    }
		  } else {
		    this.firstHide = false;
		  }
		})
		.on('show', function () {
		  if (this.firstHide) {
		    // careful, we have an infinite loop!
		    $(this).datepicker('hide'); 
		  }
		});
    });

$(function() {
        $("#editIssue").datepicker({
            format: 'mm/dd/yyyy',
            container: container,
            todayHighlight: true,
            autoclose: true,
        })
        .on('hide', function () {
		  if (!this.firstHide) {
		    if (!$(this).is(":focus")) {
		      this.firstHide = true;
		      // this will inadvertently call show (we're trying to hide!)
		      this.focus(); 
		    }
		  } else {
		    this.firstHide = false;
		  }
		})
		.on('show', function () {
		  if (this.firstHide) {
		    // careful, we have an infinite loop!
		    $(this).datepicker('hide'); 
		  }
		});
    });

$(function() {
    $("#editDOB").datepicker({
        format: 'mm/dd/yyyy',
        container: container,
        todayHighlight: true,
        autoclose: true,
    })
    $('#editDOB').datepicker('setDate', '01/01/1990')
    $('#editDOB').val('')
    .on('hide', function () {
	  if (!this.firstHide) {
	    if (!$(this).is(":focus")) {
	      this.firstHide = true;
	      // this will inadvertently call show (we're trying to hide!)
	      this.focus(); 
	    }
	  } else {
	    this.firstHide = false;
	  }
	})
	.on('show', function () {
	  if (this.firstHide) {
	    // careful, we have an infinite loop!
	    $(this).datepicker('hide'); 
	  }
	});
});

$(function() {
    $("#editExpire").datepicker({
        format: 'mm/dd/yyyy',
        container: container,
        todayHighlight: true,
        autoclose: true,
    })
    .on('hide', function () {
	  if (!this.firstHide) {
	    if (!$(this).is(":focus")) {
	      this.firstHide = true;
	      // this will inadvertently call show (we're trying to hide!)
	      this.focus(); 
	    }
	  } else {
	    this.firstHide = false;
	  }
	})
	.on('show', function () {
	  if (this.firstHide) {
	    // careful, we have an infinite loop!
	    $(this).datepicker('hide'); 
	  }
	});
});
