<%@page import="com.model.Snooze"%>
<%@page import="com.dao.SnoozeDao"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="shortcut icon" type="image/x-icon" href="<c:url value="/resources/img/logo.png"/>">
<title>Snooze</title>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/bootstrap.min.css"/>" />
<script src="<c:url value="/resources/js/jquery-2.2.4.min.js"/>"></script>
<script src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
</head>
<body>
	<div class="container">
	    <br><br>
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6 text-center">
				<h4>INFORMATION</h4>
					<div class="row text-center">
						<div class ="col-sm-6">User Name :</div>
						<div class ="col-sm-6">${ users.name }</div>
					</div>
					<div class="row text-center">
						<div class ="col-sm-6">Email :</div>
						<div class ="col-sm-6">${ users.email }</div>
					</div>
					<div class="row text-center">
						<div class ="col-sm-6">Reasson :</div>
						<div class ="col-sm-6">${ reson }</div>
					</div>
					<div class="row text-center">
						<div class ="col-sm-6">Snooze days:</div>
						<div class ="col-sm-6">${ snooze }</div>
					</div>
					<br><br>
		
			</div>
			<div class="col-md-3"></div>
		</div>
		
		<%
			    SnoozeDao snoozeDao = new SnoozeDao();
			    Snooze snooze = snoozeDao.getSnooze();
			    String num = snooze.getList();
		    %>
		      <script type="text/javascript">		      
		            
		      </script>
		    <%
		%>
		
		<script type="text/javascript">
		var mxn = <%= num %>;
		var date = new Date();
		$(document).ready(function() {
			if((date.getTime() - mxn) >= (3600*24*1000)){
				alert("Hết hạn xác nhận. Vui lòng kiểm tra lại mail mới!");
			}
			else {
				var id = "${users.id}";
	    	    var sn = "${snooze}";		 
   	    		$.ajax({
					url : 'updateSnooze',
					type : 'GET',
					data:{
					    i:id,
					    snooze:sn
					},
					success : function(data) {
						alert(data);
						window.close();
					},
					error : function(data) {
						alert("error");
					}
				});
			}
		    	     
		});
		</script>
	</div>
</body>
</html>