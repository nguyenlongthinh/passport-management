$(document).ready(function () {
	$(".SubmitForm").keypress(function(event) {
	    if (event.which == 13) {
	    	Insert();
	    }
	});
	
	configTable(1, "col1");
	configTable(2, "col2");
	configTable(3, "col3");
	configTable(4, "col4");
	configTable(5, "col5");
	configTable(6, "col6");
	configTable(7, "col7");
	configTable(8, "col8");
	configTable(9, "col9");
	configTable(10, "col10");
	configTable(11, "col11");
	configTable(12, "col12");
});
function configTable(liNumber, col){
	if(liNumber < 7){
		if(document.getElementById("configDrop1").getElementsByTagName("LI")[liNumber-1].getElementsByTagName("input")[0].checked == true){
			showCol(col);
		}
		else hideCol(col);
	}
	else {
		if(document.getElementById("configDrop2").getElementsByTagName("LI")[liNumber-7].getElementsByTagName("input")[0].checked == true){
			showCol(col);
		}
		else hideCol(col);
	}		
}

function showTable(){
	showCol("col1");
	showCol("col2");
	showCol("col3");
	showCol("col4");
	showCol("col5");
	showCol("col6");
	showCol("col7");
	showCol("col8");
	showCol("col9");
	showCol("col10");
	showCol("col11");
	showCol("col12");
	$('#dvData').show();
	$('#showTable').hide();
}
function hideCol(col){
	document.getElementById(col).setAttribute("style","display:none");
	 $('tr td:nth-child('+col.substring(3, 5)+')').hide();
	 
	 var rows = $('#datatable thead  th[style="display:none"]').get();
	 if(rows.length == 12){
		$('#dvData').hide(); 
		var execBtn = document.createElement('p');
	    execBtn.setAttribute("onclick", "showTable()");
	    execBtn.setAttribute("class", "clicktoShow fontsize");
     	execBtn.innerHTML = "<p style='cursor:pointer; color:red;'><span class='glyphicon glyphicon-hand-right'></span> Click to show table</p>";
     	document.getElementById('showTable').appendChild(execBtn);
	 }
}
function showCol(col){ 
	document.getElementById(col).setAttribute("style","display:'block'");
	 $('tr td:nth-child('+col.substring(3, 5)+')').show();
}

function mouseDown(event, col) {
	if (event.which == 3)
	      {
		var notepad = document.getElementById(col);
		var notepad1 = document.getElementById("body");
		notepad.addEventListener("contextmenu",function(event){
		    event.preventDefault();
		    var ctxMenu = document.getElementById("ctxMenu");
		    ctxMenu.style.display = "block";    	    
		    ctxMenu.style.left = (event.pageX - 10)+"px";
		    ctxMenu.style.top = (event.pageY - 70)+"px";
		    ctxMenu.style.fontsize = "1em";
		},false);
		notepad1.addEventListener("click",function(event){
		    var ctxMenu = document.getElementById("ctxMenu");
		    ctxMenu.style.display = "";
		    ctxMenu.style.left = "";
		    ctxMenu.style.top = "";
		},false);
		document.getElementById('ctxMenu').getElementsByTagName("div")[0].setAttribute("onclick","hideCol('"+col+"')");
		document.getElementById("showMenu").innerHTML = "";
		 var rows = $('#datatable thead  th[style="display:none"]').get();
		 if(rows.length > 0){
			 document.getElementById('ctxMenu').getElementsByTagName("div")[1].setAttribute("style","");
			 for(var i = 0; i < rows.length; i++){
				 colId = rows[i].getAttribute('id');
				 var myCol = document.getElementById(colId);
				 var execBtn = document.createElement('a');
		        execBtn.setAttribute("onclick", "showCol('"+colId+"')");
	        	execBtn.innerHTML = "Column "+ myCol.innerHTML.substring(0, myCol.innerHTML.length - 20);
	        	document.getElementById('showMenu').appendChild(execBtn);
			 }			 
		 }
		 else {
			 document.getElementById('ctxMenu').getElementsByTagName("div")[1].setAttribute("style","display:none");
		 }
	  }
	} 	

function clearAll(){
	document.getElementById('myInput').value='';
	 var table, tr, i;
     table = document.getElementById("datatable");
     tr = table.getElementsByTagName("tr");
     for (i = 1; i < tr.length; i++) {
    	 tr[i].setAttribute("style","");
    	 tr[i].setAttribute("issearched","true");

     }
     var table, rows;
     table = document.getElementById("datatable");
     rows = table.getElementsByTagName("TR");
     pager = new Pager(rows.length,$("#itemperpage").val());
     pager.init('datatable'); 
     pager.showPageNav('pager', 'pageNavPosition');
     listPage = 0;
     $("#listPage1").show();
     $("#listPage2").show();
     $("#listPage3").show();
     
     if(totalpage >= 3){
   	    document.getElementById("listPage1").getElementsByTagName("a")[0].innerHTML = 1;
       	document.getElementById("listPage2").getElementsByTagName("a")[0].innerHTML = 2;
       	document.getElementById("listPage3").getElementsByTagName("a")[0].innerHTML = 3;
       	
       	document.getElementById("listPage1").setAttribute("class","page-item active");	
       	document.getElementById("listPage2").setAttribute("class","page-item");
       	document.getElementById("listPage3").setAttribute("class","page-item");
       	if(totalpage == 3){
       		document.getElementById('nextPage').setAttribute("class","page-item disabled");	
       	}
     }
     else if(totalpage == 2){
       	$("#listPage3").hide();
     	    document.getElementById("listPage1").getElementsByTagName("a")[0].innerHTML = 1;
       	document.getElementById("listPage2").getElementsByTagName("a")[0].innerHTML = 2;
       	
       	document.getElementById("listPage1").setAttribute("class","page-item active");	
       	document.getElementById("listPage2").setAttribute("class","page-item");
       	document.getElementById('nextPage').setAttribute("class","page-item disabled");	

     } else if(totalpage == 1){
       	$("#listPage2").hide();
       	$("#listPage3").hide();
     	    document.getElementById("listPage1").getElementsByTagName("a")[0].innerHTML = 1;	                	
       	document.getElementById("listPage1").setAttribute("class","page-item active");	
       	document.getElementById('nextPage').setAttribute("class","page-item disabled");	
     } else {
   	  $("#listPage1").hide();
   	  $("#listPage2").hide();
   	  $("#listPage3").hide();
   	  document.getElementById('nextPage').setAttribute("class","page-item disabled");
     }
     pager.showPage(1); 
}

//Show/hide create
var hide = false;
function showCreate(event){
	if(hide == false){
		document.getElementById('showCreate').setAttribute("style","display:none;");
		document.getElementById('buttonHere').setAttribute("style","display:show;margin-right: 15px; margin-top:10px;");
		document.getElementById('contentTable').setAttribute("style","width:100%;");
		hide = true;
		}
	else {
		document.getElementById('showCreate').setAttribute("style","display:; width:325px;");
		document.getElementById('contentTable').setAttribute("style","width:calc(100% - 325px); float:left;");
		document.getElementById('buttonHere').setAttribute("style","display:none;");
		hide = false;	
	}	
}


function downImage(path){	
    var token = $("meta[name='_csrf']").attr("content");
    var header = $("meta[name='_csrf_header']").attr("content");
    $(document).ajaxSend(function(e, xhr, options) {
       xhr.setRequestHeader(header, token);
    });
	$.ajax({
        url : 'showIMG',
        type : 'POST',
        data : {
            path : path
        },
        success : function(data) { 
        	var execBtn = document.createElement('a');
        	execBtn.setAttribute("id", "downIMG");
        	execBtn.setAttribute("href","data:image/jpg;base64,"+data);
        	execBtn.setAttribute("download", path);
        	execBtn.setAttribute("onclick", 'onClick()');
        	execBtn.innerHTML = "!223";
        	document.body.appendChild(execBtn);
        	document.getElementById('downIMG').click();
        }
	});
		}
function onClick() {
	  document.getElementById("downIMG").remove();
	  searchTable();
	}
//export all
var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
    $(document).ready(function () {

        function exportTableToCSV($table, filename) {
            var $rows = $table.find('tr[isSearched="true"]:has(td)')

                // Temporary delimiter characters unlikely to be typed by keyboard
                // This is to avoid accidentally splitting the actual contents
                ,tmpColDelim = String.fromCharCode(11) // vertical tab character
                ,tmpRowDelim = String.fromCharCode(0) // null character

                // actual delimiter characters for CSV format
                ,colDelim = '","'
                ,rowDelim = '"\r\n"';

                // Grab text from table into CSV formatted string
                var csv = '"';               
                csv += formatRows($rows.map(grabRow)); 
                csv += '"' ;
                // Data URI
                var csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

            // For IE (tested 10+)
            if (window.navigator.msSaveOrOpenBlob) {
                var blob = new Blob([decodeURIComponent(encodeURI(csv))], {
                    type: "text/csv;charset=utf-8;"
                });
                navigator.msSaveBlob(blob, filename);
            } else {
                $(this)
                    .attr({
                        'download': filename
                        ,'href': csvData
                        //,'target' : '_blank' //if you want it to open in a new window
                });
            }

            // Format the output so it has the appropriate delimiters
            function formatRows(rows){
                return rows.get().join(tmpRowDelim)
                    .split(tmpRowDelim).join(rowDelim)
                    .split(tmpColDelim).join(colDelim);
            }
            // Grab and format a row from the table
            function grabRow(i,row){                 
                var $row = $(row);
                //for some reason $cols = $row.find('td') || $row.find('th') won't work...
                var $cols = $row.find('td'); 
                if(!$cols.length) $cols = $row.find('th');  

                return $cols.map(grabCol)
                            .get().join(tmpColDelim);
            }
            // Grab and format a column from the table 
            function grabCol(j,col){
                var $col = $(col),
                    $text = $col.text();

                return $text.replace('"', '""'); // escape double quotes

            }
        }

        // This must be a hyperlink
        $("#export").click(function (event) {
            var outputFile = 'user'
            outputFile = outputFile.replace('.csv','') + '.csv'             
            // CSV
            exportTableToCSV.apply(this, [$('#dvData > table'), outputFile]);            
        });
    });
    $(document).ready(function () {
        	
        function exportTableToCSVAll($table, filename) {
            var $rows = $table.find('tr:has(td)')

                // Temporary delimiter characters unlikely to be typed by keyboard
                // This is to avoid accidentally splitting the actual contents
                ,tmpColDelim = String.fromCharCode(11) // vertical tab character
                ,tmpRowDelim = String.fromCharCode(0) // null character

                // actual delimiter characters for CSV format
                ,colDelim = '","'
                ,rowDelim = '"\r\n"';

                // Grab text from table into CSV formatted string
                var csv = '"';               
                csv += formatRows($rows.map(grabRow)); 
                csv += '"' ;
                //csv += rowDelim + '"' ;

                // Data URI
                var csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

            // For IE (tested 10+)
            if (window.navigator.msSaveOrOpenBlob) {
                var blob = new Blob([decodeURIComponent(encodeURI(csv))], {
                    type: "text/csv;charset=utf-8;"
                });
                navigator.msSaveBlob(blob, filename);
            } else {
                $(this)
                    .attr({
                        'download': filename
                        ,'href': csvData
                        //,'target' : '_blank' //if you want it to open in a new window
                });
            }

            // Format the output so it has the appropriate delimiters
            function formatRows(rows){
                return rows.get().join(tmpRowDelim)
                    .split(tmpRowDelim).join(rowDelim)
                    .split(tmpColDelim).join(colDelim);
            }
            // Grab and format a row from the table
            function grabRow(i,row){                 
                var $row = $(row);
                //for some reason $cols = $row.find('td') || $row.find('th') won't work...
                var $cols = $row.find('td'); 
                if(!$cols.length) $cols = $row.find('th');  

                return $cols.map(grabCol)
                            .get().join(tmpColDelim);
            }
            // Grab and format a column from the table 
            function grabCol(j,col){
                var $col = $(col),
                    $text = $col.text();

                return $text.replace('"', '""'); // escape double quotes

            }
        }

        // This must be a hyperlink
        $("#exportallCSV").click(function (event) {
            var outputFile = 'user'
            outputFile = outputFile.replace('.csv','') + '.csv'             
            // CSV
            exportTableToCSVAll.apply(this, [$('#dvData > table'), outputFile]);            
        });
    });


// check pass to change
$(document).ready(function() {
    $('#oldpass').change(function() {
        $.ajax({
            url : 'checkoldpass',
            type : 'get',
            data : {
                password : $('#oldpass').val(),
                username: $('#username').val()
            },
            success : function(data) {
                 if (data == "exist") {
                    $('#erroroldpass').html("<span style='color:red'>Password incorrect!!!</span>");
                    document.getElementById("oldpass").value = "";
                   
                } else if (data == "noexist") {
                    $('#erroroldpass').html("");
                }
            }
        });
    });
});


    function validate(){
         var newpass = $("#newpass").val();
        var renewpass = $("#renewpass").val();
    if(newpass !=renewpass){
    $("#errorrepass").html("<span style='color:red;width:200%;'>Retype Password should be same new password!</span>");
    document.getElementById("renewpass").value = "";
    }
    if(newpass ==renewpass){
        $("#errorrepass").html("");
       
    return false;
    }
    return true;
    }

//getChangpass
function ChangePass(){
    var token = $("meta[name='_csrf']").attr("content");
    var header = $("meta[name='_csrf_header']").attr("content");
    $(document).ajaxSend(function(e, xhr, options) {
        xhr.setRequestHeader(header, token);
    });
    $("#changepass").ajaxForm({            
        success:function(data) {
              var oldpass = $("#oldpass").val();
                var newpass = $("#newpass").val();
                var renewpass = $("#renewpass").val();
              if(oldpass == ""){
                alertBox('Empty old password! Please check before change password','danger');
              }
              else if(newpass == ""){
                    alertBox('Empty new password! Please check before change password','danger');
                  }
              else if((newpass !="" ) && (newpass != renewpass)){
                  alertBox('Retype Password should be same new password!','danger'); 
              }
              else{
              alertBox('Change password success','success'); 
              }
             },
           dataType:"text"
    }).submit();
 }

//LOAD FILE 
$("#card").hide();
    var loadFile = function(event) {
        var reader = new FileReader();
        reader.onload = function() {
            var output = document.getElementById('output');
            output.src = reader.result;
        };
        reader.readAsDataURL(event.target.files[0]);
        $("#card").show();
    };
//INSERT USER
function Insert(){
    var token = $("meta[name='_csrf']").attr("content");
    var header = $("meta[name='_csrf_header']").attr("content");
    $(document).ajaxSend(function(e, xhr, options) {
        xhr.setRequestHeader(header, token);
    });
        var id = $("#id").val();
        var dob = $("datepicker1").val();
        var name = $("#name").val();
        var em = $("#testemail").val();
    	if(id == ""){
            $("#errorid").html("<span style='color:red'>ID empty</span>");
         }
         else checkId();
    	
         if(dob == ""){
             $("#errordatepicker1").html("<span style='color:red'>DOB empty</span>");
         } else checkDOB();
         
         if(name == ""){
             $("#errorname").html("<span style='color:red'>Name empty</span>");
         } 
         if(em == ""){
             $('#erroremail').html("<span style='color:red'>Email empty</span>");
         } else  checkEmail();
              
        if(($("#errorid").text() == '') && ($("#erroremail").text() == '') && ($("#errorname").text() == '') && ($("#errordatepicker1").text() == '')){
        	 $("#add-new-task").ajaxForm({
                 success:function(data) {
                     var table = document.getElementById("datatable");
                     var rows = table.getElementsByTagName("TR");
                 if(data == "exists"){
                 alertBox('Data exist! Please enter new profile!','danger'); 
                             }    
                     
                         var html = '';
                         var counter = JSON.parse(data);
                         for( i = 0;i<counter.length; i++ ){
                             if(counter[i].project =="null"){
                                 counter[i].project="";
                             }
                             if(counter[i].passport =="null"){
                                 counter[i].passport="";
                             }
                             if(counter[i].isueDate =="null"){
                                 counter[i].isueDate="";
                             }
                             if(counter[i].expDate =="null"){
                                 counter[i].expDate="";
                             }
                             if(counter[i].emailpm =="null"){
                                 counter[i].emailpm="";
                             }
                             
                             if(counter[i].emailpm !="null"){
                             var list = counter[i].emailpm.split(";");
                             }

                         //tru remainning day
                              var now = new Date();
                              var date1 =counter[i].expDate.split('/');        
                              var exDate = new Date(date1[2],date1[0]-1,date1[1]);   
                              var difftime = exDate.getTime() - now.getTime();
                              var diffDays = Math.ceil(difftime / (1000 * 3600 * 24)); 
                                 if(counter[i].expDate == ""){
                                     var diffDayss= "N/A";
                                 }
                                 else if(diffDays >=0){
                                    var diffDayss=diffDays; 
                                }else{
                                    var diffDayss="Expired";
                                }
                                var trimmedString = counter[i].path;
                                 if( counter[i].path.length > 14 )
                                 var trimmedString = counter[i].path.substring(0, 14) + "..." ;

                                 html='<tr id="tr-'+(rows.length-1)+'" issearched="true">';
                                 html+='<td>'+counter[i].id+'</td>';
                                 html+='<td>'+counter[i].name+'</td>';
                                 html+='<td>'+counter[i].dob+'</td>';
                                 html+='<td>'+counter[i].email+'</td>';
                                 html+='<td>';
                                 html+=list[0];
                                 for(j=1;j<list.length;j++){
                                     html+=';<br>';
                                     html+=list[j];                          
                                 }
                                 html+='</td>';
                                 html+='<td>'+counter[i].project+'</td>';    
                                 html+='<td>'+counter[i].passport+'</td>';
                                 html+='<td>'+counter[i].isueDate+'</td>';
                                 html+='<td>'+counter[i].expDate+'</td>';
                                 html+='<td >';
                                 if(diffDayss == diffDays){
                                     html += diffDayss;
                                 }
                                 else{
                                     html+='<div style="color:red">'+diffDayss+'</div>';     
                                 }
                                 html+='</td>';
                                 html+='<td>'+counter[i].snooze+'</td>';
                                 html+='<td class="changepointer"><a data-toggle="modal" href="" data-target="#modalimg" title="Show image" style ="color:green" onclick = "showIMG('+counter[i].id+','+(rows.length-1)+')">'+trimmedString+'</a>&nbsp;&nbsp;';
                                     if(counter[i].path != "No Image"){
                                         html += '<a onclick="downImage(\'' + counter[i].path + '\')"><i  class="glyphicon glyphicon-download-alt"></i></a>';
                                     }
                                 html+='</td>';
                                 html += '<td class="changepointer showAction"><a id="'+ i+ '" onclick="editInfo('+(rows.length-1)+')"  title="Update" style="margin-right: 3%"><span class="glyphicon glyphicon-pencil"></span> </a> '
                                 +'<a id="'+ i+ '" onclick="updateInfo('+(rows.length-1)+')" style="display:none"  title="Save" style="margin-right: 3%"><span class="glyphicon glyphicon-floppy-disk"></span> </a> '
                                 +'<a id="'+i+'" data-toggle="modal" data-target="#modaldelete" onclick="loadInfoDelete(\'' + counter[i].id + '\','+(rows.length-1)+')"  title="Xoa" style="margin-right: 3%"><span class="glyphicon glyphicon-trash"></span> </a>'
                                 +'<a id="'+ i+ '" onclick="reloadInfo('+(rows.length-1)+')" style="display:none" title="Reload" style="margin-right: 3%"><span class="glyphicon glyphicon-remove"></span> </a> </td>';
                                 html += '</tr>';
                                 $("#content").prepend(html);
                                 alertBox('Insert information is success','success'); 
                             }
                         	$("#id").val('');
                         	$("input[name=dob]").val('');
                            $("#name").val('');
                            $("#testemail").val('');
                            $("#testemailpm").val('');
                            $("#project").val('');
                            $("#passport").val('');
                            $("datepicker").val('');
                            $("#image").val('');                                                 
                            $(".bootstrap-tagsinput").val('');

                             var table, rows;
                             table = document.getElementById("datatable");
                             rows = table.getElementsByTagName("TR");
                             pager = new Pager(rows.length,$("#itemperpage").val());
                             pager.init('datatable'); 
                             pager.showPageNav('pager', 'pageNavPosition'); 
                             pager.showPage(1); 
                             configTable(1, "col1");
                         	configTable(2, "col2");
                         	configTable(3, "col3");
                         	configTable(4, "col4");
                         	configTable(5, "col5");
                         	configTable(6, "col6");
                         	configTable(7, "col7");
                         	configTable(8, "col8");
                         	configTable(9, "col9");
                         	configTable(10, "col10");
                         	configTable(11, "col11");
                         	configTable(12, "col12");
                  },dataType:"text"
               }).submit(); 
        }
       
}
//Check Empty CSV 
$(document).ready(function () {
    $("#uploadCSV").click(function () {     
        if($("#csv").val() == ''){
            alertBox('No Excel file!','danger'); 
            document.getElementById('csv').value=""; 
        }
        
    });
});
//Check Load CSV
function checkLoadCSV() {
                var a = $("#csv").val().split('.');             
                if(a[1] !== "xls"){
                    alertBox('File is not CSV!','danger'); 
                    $("#csv").val('');
                }
            }
//get CSV
function getLoadCSV(){
    var token = $("meta[name='_csrf']").attr("content");
    var header = $("meta[name='_csrf_header']").attr("content");
    $(document).ajaxSend(function(e, xhr, options) {
        xhr.setRequestHeader(header, token);
    });
    $("#importCSV").ajaxForm({          
        success:function(data) {           
            location.reload();
            alertBox("Update successfully","success");
            document.getElementById('csv').value=""; 
             },
           dataType:"text"
    }).submit();
    

 }

//CHANGE TEXT IN FILTER
$(document).ready(function () {
    $("#selectbox").click(function () {    	 
         $("select option[value='expiredate']").text("Expiry date in "+$("#expTime").val()+" day(s)");
    });
});
//FILTER
$(document).ready(function() {
    $('#selectbox').change(function(event){
        pager.destroy('pageNavPosition');        
        if($('#selectbox').val() == "nofilter")
            {       
                var table, rows;
                table = document.getElementById("datatable");
                rows = table.getElementsByTagName("TR");
                for (i = 1; i < rows.length; i++) {
                    rows[i].style.display = "";
                    rows[i].setAttribute("isSearched", "true"); 
                    rows[i].setAttribute("isSort", "");
                }
                pager = new Pager(rows.length,$("#itemperpage").val());             
                pager.init('datatable'); 
                pager.showPageNav('pager', 'pageNavPosition'); 
                document.getElementById('checkpage').value=1;
                pager.showPage(1);
            }
        else if($('#selectbox').val() == "exppassport")
        {
            var  table, tr, td, i;
              table = document.getElementById("datatable");
              tr = table.getElementsByTagName("tr");
              var lenght = 0;
              var firstRecord = null;
              var isSetfirstrecord = false;
              var lastRecord = 0;             
               for (i = 1; i < tr.length; i++) {
                   var now = new Date();
                   var date1 =tr[i].getElementsByTagName("td")[8].innerHTML.split('/');
                   var exDate = new Date(date1[2],date1[0]-1,date1[1]);        
                   var difftime = (exDate.getTime() - now.getTime());
                   var diffDays = Math.ceil(difftime / (1000 * 3600 * 24)); 
                   var tam=0;
                  if (diffDays <= tam) {                     
                    tr[i].style.display = "";
                    tr[i].setAttribute("isSearched", "true"); 
                    lenght++;
                     if(!isSetfirstrecord){
                        firstRecord = i - 1;
                        isSetfirstrecord = true;
                    }               
                    lastRecord = i-1;
                  } 
                  else {
                    tr[i].style.display = "none";
                    tr[i].setAttribute("isSearched", "false");
                  }
              }    
              pager.destroy('pageNavPosition');
              pager = new Pager(lenght,$("#itemperpage").val(), firstRecord, lastRecord);               
              pager.init(); 
              pager.showPageNav('pager', 'pageNavPosition');
              document.getElementById('checkpage').value=1;
              pager.showPage(1);            
        }   
        else if($('#selectbox').val() == "yearonly")
        {
            var  table, tr, td, i, k=0;
              table = document.getElementById("datatable");
              tr = table.getElementsByTagName("tr");
              var lenght = 0;
              var firstRecord = null;
              var isSetfirstrecord = false;
              var lastRecord = 0;
              
              for (i = 1; i < tr.length; i++) {
                  td = tr[i].getElementsByTagName("td")[2].innerHTML;
                  if (td.search("/") < 0 && td != '') {
                      k++;
                    tr[i].style.display = "";
                    tr[i].setAttribute("isSearched", "true");
                   // tr[i].setAttribute("isFil", );
                    lenght++;
                     if(!isSetfirstrecord){
                        firstRecord = i - 1;
                        isSetfirstrecord = true;
                    }               
                    lastRecord = i-1;
                  } 
                  else {
                    tr[i].style.display = "none";
                    tr[i].setAttribute("isSearched", "false");
                  }
              }   
              pager.destroy('pageNavPosition');
              pager = new Pager(lenght,$("#itemperpage").val(), firstRecord, lastRecord);               
              pager.init(); 
              pager.showPageNav('pager', 'pageNavPosition');
              document.getElementById('checkpage').value=1;
              pager.showPage(1);            
        }
        else if($('#selectbox').val() == "nopassport")
        {
              var  table, tr, td, i;
              table = document.getElementById("datatable");
              tr = table.getElementsByTagName("tr");
              var lenght = 0;
              var firstRecord = null;
              var isSetfirstrecord = false;
              var lastRecord = 0;
              for (i = 1; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[6].innerHTML;
                  if (td == "") {                    
                    tr[i].style.display = "";
                    tr[i].setAttribute("isSearched", "true"); 
                    lenght++;
                     if(!isSetfirstrecord){
                        firstRecord = i - 1;
                        isSetfirstrecord = true;
                    }               
                    lastRecord = i-1;
                  } 
                  else {
                    tr[i].style.display = "none";
                    tr[i].setAttribute("isSearched", "false");
                  }
              }   
              pager.destroy('pageNavPosition');
              pager = new Pager(lenght,$("#itemperpage").val(), firstRecord, lastRecord);               
              pager.init(); 
              pager.showPageNav('pager', 'pageNavPosition'); 
              document.getElementById('checkpage').value=1;
              pager.showPage(1);
        }
        else if($('#selectbox').val() == "expiredate")
        {
              var  table, tr, td, i;
              table = document.getElementById("datatable");
              tr = table.getElementsByTagName("tr");
              var lenght = 0;
              var firstRecord = null;
              var isSetfirstrecord = false;
              var lastRecord = 0;             
               for (i = 1; i < tr.length; i++) {
                   var now = new Date();
                   var date1 =tr[i].getElementsByTagName("td")[8].innerHTML.split('/');
                   var exDate = new Date(date1[2],date1[0]-1,date1[1]);             
                   var difftime = Math.abs(exDate.getTime() - now.getTime());
                   var diffDays = Math.ceil(difftime / (1000 * 3600 * 24)); 
                   var tam=$("#expTime").val();
                  if (diffDays <= tam) {                     
                    tr[i].style.display = "";
                    tr[i].setAttribute("isSearched", "true"); 
                    lenght++;
                     if(!isSetfirstrecord){
                        firstRecord = i - 1;
                        isSetfirstrecord = true;
                    }               
                    lastRecord = i-1;
                  } 
                  else {
                    tr[i].style.display = "none";
                    tr[i].setAttribute("isSearched", "false");
                  }
              }    
              pager.destroy('pageNavPosition');
              pager = new Pager(lenght,$("#itemperpage").val(), firstRecord, lastRecord);               
              pager.init(); 
              pager.showPageNav('pager', 'pageNavPosition');
              document.getElementById('checkpage').value=1;
              pager.showPage(1); 
        }       
    });
});   
    
    // Export file csv to xls (excel)
    $("#exportFile").click(function(e) {
            var rows = [];
            $('#datatable tr:visible:gt(0)').each(function(index, tr) {//get all row visible but not first
               var lines = $('td', tr).not('td:last').map(function(index, td) {
                    return $(td).text();
                });
               
               rows.push(JSON.stringify(lines.get()));
            });

            if(rows.length <=0)
            {
                alertBox('No data export','danger');
            } else if(rows.length==1)
            {
                var token = 1;//case export le 1 row
            } else 
            {
                var token = 2;//case export gt 1 row
            }
            console.log(123)
            var token1 = $("meta[name='_csrf']").attr("content");
            var header = $("meta[name='_csrf_header']").attr("content");
            //$(document).ajaxSend(function(e, xhr, options) {
            //  xhr.setRequestHeader(header, token);
            //});
            console.log(document.getElementById('exportFile').getAttribute('value'), header)
            $.ajax({
                        url : 'exportfile',
                        type : 'POST',
                        
                        data : {
                            datas : rows,
                            token : token,
                            _csrf : document.getElementById('exportFile').getAttribute('value')
                        },
                        success : function(data) {
                        if(data == "success")
                            {
                               window.location.href = "<%=request.getContextPath() %>/resources/excel/user.xls";
                            }
                        }
                    });
            return false;
        });
    
    //export all
    $("#exportAll").click(
            function(e) {
                $.ajax({
                            url : 'exportFileAll',
                            type : 'GET',
                            success : function(data) {
                            if(data == "success")
                                {
                                   window.location.href = "<%=request.getContextPath() %>/resources/excel/user.xls";
                                }
                            }
                        });
                return false;
            });

// script load info 'user for editing -->
//<!-- FOR UPDATE BY MODAL -->
    $("#userEditInfoForm")
            .submit(
                    function(e) {
                        $("#content").html("");
                        e.preventDefault();
                        $.ajax({
                                    type : "GET",
                                    url : "updateUserInfo",
                                    data : {
                                        name : $('#recipient-name').val(),
                                        project : $('#recipient-project').val(),
                                        dob : $('#recipient-dob').val(),
                                        pass : $('#recipient-pass').val(),
                                        issue : $('#recipient-issue').val(),
                                        img : $('#recipient-img').val(),
                                        id : $('#recipient-id').val(),
                                        email : $('#recipient-email').val(),
                                        row : $('#recipient-row').val()
                                    },
                                    success : function(result) {
                                        //alert
                                         alertBox('Update Successful!','success'); 
                                        var list = JSON.parse(result[0]);
                                        var html = '';
                                        for (var i = 0; i < list.length; i++) {
                                            var counter = list[i];
                                            if (counter.id == $('#recipient-id').val()) {
                                                html = '<tr id="updated" style="background: #c5d9ea">';
                                            } else {
                                                html = '<tr>';
                                            }
                                             var now = new Date();
                                             var date1 =list[i].expDate.split('/');           
                                             var exDate = new Date(date1[2],date1[0]-1,date1[1]);   
                                             var difftime = exDate.getTime() - now.getTime();
                                             var diffDays = Math.ceil(difftime / (1000 * 3600 * 24)); 
                                                if(counter.expDate == ""){
                                                    var diffDayss = "N/A";
                                                }
                                                else if(diffDays >= 0){
                                                   var diffDayss = diffDays; 
                                               }else{
                                                   var diffDayss = "Expired";
                                               }

                                               var trimmedString = counter.path;
                                                if( counter.path.length > 14 )
                                                var trimmedString = counter.path.substring(0, 14) + "..." ;

                                            html += '<td>' + counter.id+ '</td>';
                                            html += '<td>' + counter.name+ '</td>';
                                            html += '<td>' + counter.project+ '</td>';
                                            html += '<td>' + counter.dob+ '</td>';
                                            html += '<td>' + counter.email+ '</td>';
                                            html += '<td>' + counter.passport+ '</td>';
                                            html += '<td>' + counter.isueDate+ '</td>';
                                            html += '<td>' + counter.expDate+ '</td>';
                                             if(diffDayss != diffDays)
                                                html+='<div style="color:red">'+diffDayss+'</div>';
                                            else html+='<div>'+diffDayss+'</div>';
                                            html+='</td>';
                                            html+='<td>'+counter.snooze+'</td>';
                                            html+='<td class="changepointer"><a data-toggle="modal" href="" data-target="#modalimg" title="Show image" style ="color:green" onclick = "showIMG('+counter.id+','+i+')">'+trimmedString+'</a>&nbsp;&nbsp;';
                                                if(counter.path != "No Image"){
                                                    html += '<a onclick="downImage(\'' + counter.path + '\')"><i  class="glyphicon glyphicon-download-alt"></i></a>';
                                                }
                                            html+='</td>';
                                            html += '<td><a id="'+ i+ '" onclick="loadInfoEdit('+counter.id+ ')"  data-toggle="modal" data-target="#exampleModal" title="Chỉnh sửa" style="margin-right: 3%"><span class="glyphicon glyphicon-pencil"></span> </a> <a id="'+i+ '" data-toggle="modal" data-target="#modaldelete" onclick="loadInfoDelete(\'' + counter.id + '\')"  title="Xoa" style="margin-right: 3%"><span class="glyphicon glyphicon-remove"></span> </a></td>';
                                            html += '</tr>';
                                            $("#content").append(html);
                                        }

                                    },
                                    error : function(result) {
                                        alert('errorff' + dataform);
                                    }
                                });
                    });
//ALERT
function alertBox(message, type){//type: danger, success, warning.. 
    $('<div class="alert alert-'+type+' ppalert" id="success-alert">'+message+'</div>')
    .appendTo('#result').trigger(
            'showalert').fadeOut(
            5000);
    return false;
}
//LOAD DATA TABLE:
$(window).on('load', function() {
	$(document).ajaxStart(function(){
		  $("#fountainG").show();
		  $("#body").hide();
		}).ajaxStop(function() {
		  $("#fountainG").hide();
		  $("#body").show();
		});
        $.ajax({
            url : 'loaddata',
            type : 'GET',
            data : {
                data : 'index'
            },
            success : function(data) {
                //alert(data);
                var jsonData = JSON.parse(data);
                var html = '';
                var array =  jsonData;
                
                function sort_expDate(a, b) {
                    if(a.expDate.search("/") < 0){
                        var xDate = new Date(1111,1,1); 
                    }
                    else{
                        var date1 =a.expDate.split('/');
                        var xDate = new Date(date1[2],date1[1]-1,date1[0]); 
                    }
                    if(b.expDate.search("/") < 0){
                        var yDate = new Date(1111,1,1); 
                    }
                    else{
                        var date2 =b.expDate.split('/');
                        var yDate = new Date(date2[2],date2[1]-1,date2[0]); 
                    }       
                    return xDate.getTime() - yDate.getTime();
                }
                array.sort(sort_expDate);
                for (var i = 0; i < array.length; i++){  
                    var counter = array[i];
                    
                if(counter.emailpm != "null"){
                    var list = counter.emailpm.split(";");
                    }
                if(counter.project =="null"){
                    counter.project="";
                }
                if(counter.passport =="null"){
                    counter.passport="";
                }
                if(counter.isueDate =="null"){
                    counter.isueDate="";
                }
                if(counter.expDate =="null"){
                    counter.expDate="";
                }

                 var now = new Date();
                 var date1 =array[i].expDate.split('/');          
                 var exDate = new Date(date1[2],date1[0]-1,date1[1]);   
                 var difftime = exDate.getTime() - now.getTime();
                 var diffDays = Math.ceil(difftime / (1000 * 3600 * 24)); 
                    if(counter.expDate == ""){
                        var diffDayss = "N/A";
                    }
                    else if(diffDays >= 0){
                       var diffDayss = diffDays; 
                   }else{
                       var diffDayss = "Expired";
                   }
                    
                    var dataItem = JSON.stringify(counter);
                    var trimmedString = counter.path;
                    if( counter.path.length > 14 )
                    var trimmedString = counter.path.substring(0, 14) + "..." ;

                    html='<tr id="tr-'+i+'" issearched="true">';
                    html+='<td>'+counter.id+'</td>';
                    html+='<td>'+counter.name+'</td>';
                    html+='<td>'+counter.dob+'</td>';
                    html+='<td>'+counter.email+'</td>';
                    html+='<td >';
                        html+=list[0];
                        for(j=1;j<list.length;j++){
                            html+=';<br>';
                            html+=list[j];                          
                        }
                    html+='</td>';
                    html+='<td>'+counter.project+'</td>';    
                    html+='<td>'+counter.passport+'</td>';
                    html+='<td>'+counter.isueDate+'</td>';
                    html+='<td>'+counter.expDate+'</td>';
                   
                    if(diffDayss != diffDays){
                    	 html+='<td style="color:red">'+diffDayss;
                    }                       
                    else html+='<td>'+diffDayss;
                    html+='</td>';
                    html+='<td>'+counter.snooze+'</td>';
                    html+='<td class="changepointer"><a data-toggle="modal" href="" data-target="#modalimg" title="Show image" style ="color:green" onclick = "showIMG('+counter.id+','+i+')">'+trimmedString+'</a>&nbsp;&nbsp;';
                        if(counter.path != "No Image"){
                            html += '<a onclick="downImage(\'' + counter.path + '\')"><i  class="glyphicon glyphicon-download-alt"></i></a>';
                        }
                    html+='</td>';                    
                    html += '<td  class="showAction"><a id="'+ i+ '" onclick="editInfo('+ i + ')"  title="Update" style="margin-right: 3%"><span class="glyphicon glyphicon-pencil"></span> </a> '
                    +'<a id="'+ i+ '" onclick="updateInfo('+ i + ')" style="display:none"  title="Save" style="margin-right: 3%"><span class="glyphicon glyphicon-floppy-disk"></span> </a> '
                    +'<a id="'+i+'" data-toggle="modal" data-target="#modaldelete" onclick="loadInfoDelete(\'' + counter.id + '\', '+i+')"  title="Xoa" style="margin-right: 3%"><span class="glyphicon glyphicon-trash"></span> </a>'
                    +'<a id="'+ i+ '" onclick="reloadInfo('+ i + ')" style="display:none" title="Reload" style="margin-right: 3%"><span class="glyphicon glyphicon-remove"></span> </a> </td>';
                    html += '</tr>';  
                    $("#content").append(html);
                   // document.write("<br>" + array[item].name);  
                }

                var table, rows;
                table = document.getElementById("datatable");
                rows = table.getElementsByTagName("TR");
                //warning
                pager = new Pager(rows.length,$("#itemperpage").val());             
                pager.init('datatable'); 
                pager.showPageNav('pager', 'pageNavPosition'); 
                document.getElementById('checkpage').value=1;
                if(totalpage == 3){
                	document.getElementById('nextPage').setAttribute("class","page-item disabled");	
                }
                if(totalpage == 2){
                	$("#listPage3").hide();
                	document.getElementById('nextPage').setAttribute("class","page-item disabled");	
                }
                else if(totalpage == 1){
                	$("#listPage3").hide();	  
                	$("#listPage2").hide();	
                	document.getElementById('nextPage').setAttribute("class","page-item disabled");	
                }
                pager.showPage(1);          
            	configTable(1, "col1");
            	configTable(2, "col2");
            	configTable(3, "col3");
            	configTable(4, "col4");
            	configTable(5, "col5");
            	configTable(6, "col6");
            	configTable(7, "col7");
            	configTable(8, "col8");
            	configTable(9, "col9");
            	configTable(10, "col10");
            	configTable(11, "col11");
            	configTable(12, "col12");
            }
        });
    });

//CHON SO ITEM
$(document).ready(function() {
    $('#itemperpage').change(function(event){	        
                pager.destroy('pageNavPosition');
                var table, rows, length1 = 0;
                table = document.getElementById("datatable");
                rows = table.getElementsByTagName("TR");
                for (var i = 1; i < (rows.length); i++) {   
                    if(rows[i].getAttribute('isSearched') === "true"){
                        length1++;                       
                    }
                }	
                  pager.destroy('pageNavPosition');
                  pager = new Pager(length1,$("#itemperpage").val());               
                  pager.init(); 
                  pager.showPageNav('pager', 'pageNavPosition'); 
                  document.getElementById('checkpage').value=1;
                  listPage = 0;
                  $("#listPage1").show();
                  $("#listPage2").show();
                  $("#listPage3").show();
                  
                  if(totalpage >= 3){
                	    document.getElementById("listPage1").getElementsByTagName("a")[0].innerHTML = 1;
	                	document.getElementById("listPage2").getElementsByTagName("a")[0].innerHTML = 2;
	                	document.getElementById("listPage3").getElementsByTagName("a")[0].innerHTML = 3;
	                	
	                	document.getElementById("listPage1").setAttribute("class","page-item active");	
	                	document.getElementById("listPage2").setAttribute("class","page-item");
	                	document.getElementById("listPage3").setAttribute("class","page-item");
	                	if(totalpage == 3){
	                		document.getElementById('nextPage').setAttribute("class","page-item disabled");	
	                	}
                  }
                  else if(totalpage == 2){
	                	$("#listPage3").hide();
	              	    document.getElementById("listPage1").getElementsByTagName("a")[0].innerHTML = 1;
	                	document.getElementById("listPage2").getElementsByTagName("a")[0].innerHTML = 2;
	                	
	                	document.getElementById("listPage1").setAttribute("class","page-item active");	
	                	document.getElementById("listPage2").setAttribute("class","page-item");
	                	document.getElementById('nextPage').setAttribute("class","page-item disabled");	

                  } else if(totalpage == 1){
	                	$("#listPage2").hide();
	                	$("#listPage3").hide();
	              	    document.getElementById("listPage1").getElementsByTagName("a")[0].innerHTML = 1;	                	
	                	document.getElementById("listPage1").setAttribute("class","page-item active");	
	                	document.getElementById('nextPage').setAttribute("class","page-item disabled");	
                  } else {
                	  $("#listPage1").hide();
                	  $("#listPage2").hide();
                	  $("#listPage3").hide();
                	  document.getElementById('nextPage').setAttribute("class","page-item disabled");
                  }          	
                  pager.showPage(1);
    });
});
    
$(window).on('load',function() {
                        $.ajax({
                            url : 'loadExpDay',
                            type : 'GET',
                            success : function(data) {
                                if(data){
                                    $("#expTime").val(data);//set value for input expire day
                                }
                            }
                        });
});
//Load Snooze:
$(window).on('load',function() {
    $.ajax({
        url : 'loadSnooze',
        type : 'get',
        success : function(data) {
            $("#snooze").val(data);
        }
    });
});

//PAGE SIZE
$(function(){
    var pressed = false;
    var start = undefined;
    var startX, startWidth;

    $("table td").mousedown(function(e) {

        start = $(this);
        pressed = true;
        startX = e.pageX;
        startWidth = $(this).width();
        //this.style.cursor="resize";
    });

    $(document).mousemove(function(e) {
        if(pressed) {
            $(start).width(startWidth+(e.pageX-startX));
        }
    });
    $(document).mouseup(function() {
        if(pressed) {
            pressed = false;
        }
    });
}); 
//SORT 
function sortTable(f,n){
	var rows = $('#datatable tbody  tr[isSearched="true"]').get();
	rows.sort(function(a, b) {	
		var A = getVal(a);
		var B = getVal(b);
		if(n == 1 || n == 5){
			if(A.replace("Đ", "F") < B.replace("Đ", "F")) {
				return -1*f;
			}
			if(A.replace("Đ", "F") > B.replace("Đ", "F")) {
				return 1*f;
			}
		}
		else 
		if(n == 0  || n == 3 || n == 4 || n == 6 || n == 9 || n == 10 ){	
			if(A == "N/A" || A == "Expired"){
				A = "0";
			}
			if(B == "N/A" || B == "Expired"){
				B = "0";
			}
			for(var j = A.length; j < 9; j++)
			{
				A = "0"+A; 
			}
			for(var k = B.length; k < 9; k++)
			{
				B = "0"+B; 
			}
			if(A < B) {
				return -1*f;
			}
			if(A > B) {
				return 1*f;
			}
		}
		else if( n == 2   ){
			if(String(A).search("/") < 0){
                var xDate = new Date(A,1,1); 
            }
            else{
                var date1 =A.split('/');
                var xDate = new Date(date1[2],date1[0],date1[1]-1); 
            }
            if(String(B).search("/") < 0){
                var yDate = new Date(B,1,1); 
            }
            else{
                var date2 =B.split('/');
                var yDate = new Date(date2[2],date2[0],date2[1]-1);
            }
            if (xDate.getTime() < yDate.getTime()) {
            	return -1*f;
            }
            if (xDate.getTime() > yDate.getTime()) {
            	return 1*f;
            }
		}
		else if(n == 7 || n == 8){
			if(String(A).search("/") < 0){
                var xDate = new Date(1,1,1); 
            }
            else{
                var date1 =A.split('/');
                var xDate = new Date(date1[2],date1[0],date1[1]-1); 
            }
            if(String(B).search("/") < 0){
                var yDate = new Date(1,1,1); 
            }
            else{
                var date2 =B.split('/');
                var yDate = new Date(date2[2],date2[0],date2[1]-1);
            }
            if (xDate.getTime() < yDate.getTime()) {
            	return -1*f;
            }
            if (xDate.getTime() > yDate.getTime()) {
            	return 1*f;
            }
		}
		
		return 0;
	});

	function getVal(elm){
		var v = $(elm).children('td').eq(n).text().toUpperCase();		
		return v;
	}

	$.each(rows, function(index, row) {
		$('#datatable').children('tbody').append(row);
	});
}
//var f_sl = 1;
var f_nm = 1;
var count = 1;
function sort(a) {
	pager.destroy('pageNavPosition');
	var length1 = 0, k = -1;
	var table = document.getElementById("datatable");
	var rows = table.getElementsByTagName("TR");
	 for (var i = 1; i < (rows.length); i++) {   
       if(rows[i].getAttribute('isSearched') === "true"){
           length1++;
           rows[i].style.display = ""; 
           rows[i].setAttribute("isSort", "sort-"+ ++k);
       }
   }
	for( var i = 0; i < 11; i++)
      rows[0].getElementsByTagName("TH")[i].getElementsByTagName("i")[0].className ="fa fa-sort";
	
	if(count == 0){
      rows[0].getElementsByTagName("TH")[a].getElementsByTagName("i")[0].className ="fa fa-sort-asc";
      count = 1;
  }
  else {
      rows[0].getElementsByTagName("TH")[a].getElementsByTagName("i")[0].className ="fa fa-sort-desc";
      count = 0;
  }
	f_nm *= -1;
//    var n = $(this).prevAll().length;
    sortTable(f_nm,a);
	  if(k == rows.length -2)
	  {
        pager.destroy('pageNavPosition');
        pager = new Pager(length1,$("#itemperpage").val());               
        pager.init(); 
        pager.showPageNav('pager', 'pageNavPosition'); 
        document.getElementById('checkpage').value=1;
        pager.showPage(1);
  }
  else {
        pager.destroy('pageNavPosition');
        pager = new Pager(length1,$("#itemperpage").val(), null, null, true);             
        pager.init(); 
        pager.showPageNav('pager', 'pageNavPosition'); 
        document.getElementById('checkpage').value=1;
        pager.showPage(1);
  }
	}
//SEARCH
function searchTable() {
      var input, filter, table, tr, td, i;
      input = document.getElementById("myInput");
      filter = input.value.toUpperCase();
      table = document.getElementById("datatable");
      tr = table.getElementsByTagName("tr");
      var lenght = 0;
      var firstRecord = null;
      var isSetfirstrecord = false;
      var lastRecord = 0;
      for (i = 1; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[0];
        td1 = tr[i].getElementsByTagName("td")[1];
        td2 = tr[i].getElementsByTagName("td")[2];
        td3 = tr[i].getElementsByTagName("td")[3];
        td4 = tr[i].getElementsByTagName("td")[4];
        td5 = tr[i].getElementsByTagName("td")[5];
        td6 = tr[i].getElementsByTagName("td")[6];
        td7 = tr[i].getElementsByTagName("td")[7];
        td8 = tr[i].getElementsByTagName("td")[8];

        if (td) {            
          if ((td.innerHTML.toUpperCase().indexOf(filter) > -1) || (td1.innerHTML.toUpperCase().indexOf(filter) > -1) 
                  || (td2.innerHTML.toUpperCase().indexOf(filter) > -1) || (td3.innerHTML.toUpperCase().indexOf(filter) > -1)
                  || (td4.innerHTML.toUpperCase().indexOf(filter) > -1) || (td5.innerHTML.toUpperCase().indexOf(filter) > -1)
                  || (td6.innerHTML.toUpperCase().indexOf(filter) > -1) || (td7.innerHTML.toUpperCase().indexOf(filter) > -1)
                  || (td8.innerHTML.toUpperCase().indexOf(filter) > -1)) {
            tr[i].style.display = "";
            tr[i].setAttribute("isSearched", "true"); 
            lenght++;
            if(!isSetfirstrecord){
                firstRecord = i -1;
                isSetfirstrecord = true;
            }               
            lastRecord = i-1;
          } else {
            tr[i].style.display = "none";
            tr[i].setAttribute("isSearched", "false");
          }
        }
      }   
      pager.destroy('pageNavPosition');
      pager = new Pager(lenght,$("#itemperpage").val(), firstRecord, lastRecord);               
      pager.init(); 
      pager.showPageNav('pager', 'pageNavPosition'); 
      listPage = 0;
      $("#listPage1").show();
      $("#listPage2").show();
      $("#listPage3").show();
      
      if(totalpage >= 3){
    	    document.getElementById("listPage1").getElementsByTagName("a")[0].innerHTML = 1;
        	document.getElementById("listPage2").getElementsByTagName("a")[0].innerHTML = 2;
        	document.getElementById("listPage3").getElementsByTagName("a")[0].innerHTML = 3;
        	
        	document.getElementById("listPage1").setAttribute("class","page-item active");	
        	document.getElementById("listPage2").setAttribute("class","page-item");
        	document.getElementById("listPage3").setAttribute("class","page-item");
        	if(totalpage == 3){
        		document.getElementById('nextPage').setAttribute("class","page-item disabled");	
        	}
        	else document.getElementById('nextPage').setAttribute("class","page-item");	

      }
      else if(totalpage == 2){
        	$("#listPage3").hide();
      	    document.getElementById("listPage1").getElementsByTagName("a")[0].innerHTML = 1;
        	document.getElementById("listPage2").getElementsByTagName("a")[0].innerHTML = 2;
        	
        	document.getElementById("listPage1").setAttribute("class","page-item active");	
        	document.getElementById("listPage2").setAttribute("class","page-item");
        	document.getElementById('nextPage').setAttribute("class","page-item disabled");	

      } else if(totalpage == 1){
        	$("#listPage2").hide();
        	$("#listPage3").hide();
      	    document.getElementById("listPage1").getElementsByTagName("a")[0].innerHTML = 1;	                	
        	document.getElementById("listPage1").setAttribute("class","page-item active");	
        	document.getElementById('nextPage').setAttribute("class","page-item disabled");	
      } else {
    	  $("#listPage1").hide();
    	  $("#listPage2").hide();
    	  $("#listPage3").hide();
    	  document.getElementById('nextPage').setAttribute("class","page-item disabled");
      }          	
      pager.showPage(1);      
    }   
    // SETTING EXPRIDATE
    $(document).ready(function() {
    $("#btnExp").click(function() {
                if($('#expTime').val() <= 1)
                    {
                    alertBox("Value must be more than or equal to 1","danger");
                    return false;
                    }
                else if($('#expTime').val() > 3650)
                {
                alertBox("Value must be less than or equal to 3650","danger");
                return false;
                }
                else if ($('#expTime').val() != "" && $('#expTime').val() != null) {
                    $.ajax({
                        url : 'editExpTime',
                        type : 'get',
                        data : {
                        time : $('#expTime').val()},
                        success : function(data) {
                        if (data == "success") {
                            alertBox("Update! expire day succes","success");
                        } else {
                            alertBox("Update! expire day Fail","danger");
                        }
                    }
                });
            }
        return false;
        });
    });
    // SETTING EXPRIDATE
    $(document).ready(function() {
    $("#btnSnooze").click(function(e) {
        e.preventDefault()
        var sn = $('#snooze').val();
        if(sn<0){
            alertBox('Snooze must be more than 1 day!!','danger');
        }
        else if(sn > 365){
            alertBox('Snooze must be less than 365 days!!','danger');
        }
        else if ($('#snooze').val() != ""&& $('#snooze').val() != null) {
        $.ajax({
                url : 'setSnooze',
                type : 'get',
                data : {
                 snooze : $('#snooze').val()
                },
                success : function(data) {
                    
                   alertBox('Set Snooze Success!!','success');
                   $("#snooze").val(data);
                }
            });
        }
        });
    });
 
    
    //LOAD INFO EDIT
    function loadInfoEdit(id, row) {
        $.ajax({
            url : 'loadInfoEdit',
            type : 'GET',
            data : {
                id : id,
                row : row
            },
            success : function(data) {
                if (data) {
                    var userInfo = data[0].split(",");
                    $("#userEditInfoForm").show();
                    
                    $("#recipient-id").val(userInfo[0]);
                    $("#recipient-name").val(userInfo[1]);
                    $("#recipient-project").val(userInfo[5]);
                    $("#recipient-dob").val(userInfo[3]);
                    $("#recipient-email").val(userInfo[4]);
                    $("#recipient-pass").val(userInfo[5]);
                    $("#recipient-issue").val(userInfo[6]);
                    $("#recipient-img").val(userInfo[8]);

                    $("#recipient-row").val(data[1]);
                } else {
                    alertBox('Failed!','danger'); 
                }
            }
        });
        return false;
    }
    
    //Load all data lists:
    function loadDataList(id,i) {
        $.ajax({
            url : 'load',
            type : 'GET',
            data : {
                ms : id
            },
            success : function(data) {
                var html ='';
                var counter = JSON.parse(data);
                if(counter.emailpm != "null"){
                    var list = counter.emailpm.split(";");
                    }
                if(counter.project =="null"){
                    counter.project="";
                }
                if(counter.passport =="null"){
                    counter.passport="";
                }
                if(counter.isueDate =="null"){
                    counter.isueDate="";
                }
                if(counter.expDate =="null"){
                    counter.expDate="";
                }
                
                if(counter.emailpm != "null"){
                    var list = counter.emailpm.split(";");
                    }
                
	             var now = new Date();
	             var date1 =counter.expDate.split('/');           
	             var exDate = new Date(date1[2],date1[0]-1,date1[1]);   
	             var difftime = exDate.getTime() - now.getTime();
	             var diffDays = Math.ceil(difftime / (1000 * 3600 * 24)); 
	                if(counter.expDate == ""){
	                    var diffDayss= "N/A";
	                }
	                else if(diffDays >= 0){
	                   var diffDayss=diffDays; 
	               }else{
	                   var diffDayss="Expired";
	               }
	               var trimmedString = counter.id + counter.path.substring(counter.path.length - 4, counter.path.length);
	                if( trimmedString.length > 14 )
	                var trimmedString = trimmedString.substring(0, 14) + "..." ;

                html+='<td>'+counter.id+'</td>';
                html+='<td>'+counter.name+'</td>';
                html+='<td>'+counter.dob+'</td>';
                html+='<td>'+counter.email+'</td>';
                html+='<td >';
                for(j=0;j<list.length;j++){
                    html+=list[j];
                    html+=';<br>';
                }
                html+='</td>';
                html+='<td>'+counter.project+'</td>';    
                html+='<td>'+counter.passport+'</td>';
                html+='<td>'+counter.isueDate+'</td>';
                html+='<td>'+counter.expDate+'</td>';
                html+='<td >';
                    if(diffDayss == diffDays){
                        html += diffDayss;
                    }
                    else{html+='<div style="color:red">'+diffDayss+'</div>';
                        
                    }
                html+='</td>';
                html+='<td>'+counter.snooze+'</td>';
                html+='<td class="changepointer"><a data-toggle="modal" href="" data-target="#modalimg" title="Show image" style ="color:green" onclick = "showIMG('+counter.id+','+i+')">'+trimmedString+'</a>&nbsp;&nbsp;';
                        if(counter.path != "No image"){
                            html += '<a onclick="downImage(\'' + counter.path + '\')"><i  class="glyphicon glyphicon-download-alt"></i></a>';
                        }
                    html+='</td>';
                html += '<td class="showAction"><a id="'+ i+ '" onclick="editInfo('+ i + ')"  title="Update" style="margin-right: 3%"><span class="glyphicon glyphicon-pencil"></span> </a> '
                +'<a id="'+ i+ '" onclick="updateInfo('+ i + ')" style="display:none"  title="Save" style="margin-right: 3%"><span class="glyphicon glyphicon-floppy-disk"></span> </a> '
                +'<a id="'+i+'" data-toggle="modal" data-target="#modaldelete" onclick="loadInfoDelete(\'' + counter.id + '\','+i+')"  title="Xoa" style="margin-right: 3%"><span class="glyphicon glyphicon-trash"></span> </a>'
                +'<a id="'+ i+ '" onclick="reloadInfo('+ i + ')" style="display:none" title="Reload" style="margin-right: 3%"><span class="glyphicon glyphicon-remove"></span> </a> </td>';
                //Append HTML row:
                $("#datatable").find("#tr-"+i).html(html);
                //
                //
              //show link in current row
                $("#datatable tbody tr").find("td:nth-child(10)").find("a").show();
                
                //hide button to edit
                $("#datatable tbody tr td:nth-child(10)").find("a:eq(1), a:eq(3)").hide();
                
                //set default html(for date picker)
                $("#datatable tbody tr td:nth-child(3), td:nth-child(7)").each(function() {
                    
                    if($(this).find('input').length){
                        $(this).html($(this).find('input').val());
                    }
                });
                
                //set border error
                $("#datatable tr").removeClass();//set nonborder all tr
                $("#datatable tr td").removeClass();//set nonborder all td column 7
                $("#datatable tr td:last-child").addClass("showAction");

                //set default for all td(style)
//                $("#datatable").find("tbody>tr:eq("+row+") td").addClass("default-table");
                //border
//                $("#datatable tr:eq("+row+")").addClass("write-border");
                pager.destroy('pageNavPosition');
                var table, rows;
                table = document.getElementById("datatable");
                rows = table.getElementsByTagName("TR");
                
                //warning
                pager = new Pager(rows.length,$("#itemperpage").val());             
                pager.init('datatable'); 
                pager.showPageNav('pager', 'pageNavPosition'); 
                pager.showPage(1);
            	configTable(1, "col1");
            	configTable(2, "col2");
            	configTable(3, "col3");
            	configTable(4, "col4");
            	configTable(5, "col5");
            	configTable(6, "col6");
            	configTable(7, "col7");
            	configTable(8, "col8");
            	configTable(9, "col9");
            	configTable(10, "col10");
            	configTable(11, "col11");
            	configTable(12, "col12");
            }
        });
    }
    
    $("#card").hide();
        var loadFile = function(event) {
            var reader = new FileReader();
            reader.onload = function() {
                var output = document.getElementById('output');
                output.src = reader.result;
            };
            reader.readAsDataURL(event.target.files[0]);
            $("#card").show();
        };
        

        
         //get Json IMG:
           function getLoadImg(id,image){               

               var token = $("meta[name='_csrf']").attr("content");

               var header = $("meta[name='_csrf_header']").attr("content");

               $(document).ajaxSend(function(e, xhr, options) {

                   xhr.setRequestHeader(header, token);

               });

                $('#formimg').attr('action','/PassPort_Management/reloadimg');

                $("#formimg").ajaxForm({
                       success:function(data) {
                               //remove pic:
                               $("#pic").attr("src","");
                               $("#pic").attr("src",data+"/resources/img/"+image);
                               //remove div:
                               $("#add").html('<img class="img-fluid" src="'+data+'/resources/img/'+image+'">');
//                               $("#imgct").html('<img class="img-fluid" src="'+data+'/resources/img/'+image+'">');

                               $('#formimg').attr('action','/PassPort_Management/updateIMG');
                               $("#upimg").html('Change Your Image!!');
                        },
                        error: function(data){
                        	alert("Reload IMG error"+data)
                        },
                      dataType:"text"

                      }).submit();

            }
    
        //RESET:
         $("#reset").click(function(){
                alert("The paragraph was clicked.");
         });
        //SHOW IMAGE:
         function showIMG(id,index) {
             var token = $("meta[name='_csrf']").attr("content");
             var header = $("meta[name='_csrf_header']").attr("content");
             $(document).ajaxSend(function(e, xhr, options) {
                xhr.setRequestHeader(header, token);
             });
                $.ajax({
                    url : 'show',
                    type : 'POST',
                    data : {
                        id : id
                    },
                    success : function(data) {
                        $("#imgct").html("");
                        var counter = JSON.parse(data);
                        if(counter.path === "No Image"){
                            $("#title").text('No Image');
                            $("#imgct").html("<div id='add'><span id='remove' class ='text-center' style='color:red'> No Image Please Insert Your Image!! </span></div>");
                            var html = '<br><br><label style="color:green">';
                            html+='Change your image</label>';
                            html+='<br>';
                            html+='<form method = "POST" id="formimg" enctype="multipart/form-data" action="<%= request.getContextPath() %>/updateIMG" name="${_csrf.parameterName}" value="${_csrf.token}">';
                            html+='<center>';
                            html+='<input name="id" id="i" type ="hidden" value ="'+counter.id+'">';
                            html+='<input name="image" onchange="checkLoadImg()" id="im" type= "file">';
                            html+='<span id = "error"></span>';
                            html+='<br>';
                            html+='<input type="button" class="btn btn-primary" data-dismiss="modal" onclick="updateIMG('+index+')" value="Change Image" />';
                            html+='</center>';
                            html+='</form>';
                            $("#imgct").append(html);
                        }
                        else {
                        	var temple = "";
                        	$.ajax({
                                url : 'showIMG',
                                type : 'POST',
                                data : {
                                    path : counter.path
                                },
                                success : function(data) {
                                	temple = data;
                                	$("#title").text(counter.path);
                                    $("#imgct").html("");
                                    var html='';
                                    html ='<div id="add"><img  class="img-responsive" src="data:image/jpg;base64,'+temple+'"/></div>';
//                                    html+='';
                                    html+='<br><br><br>';
                                    html+='<label style="color:green">';
                                    html+='Change your image</label>';
                                    html+='<br>';
                                    html+='<form method = "POST" id="formimg" enctype="multipart/form-data" action="<%= request.getContextPath() %>/updateIMG" name="${_csrf.parameterName}" value="${_csrf.token}">';
                                    html+='<center>';
                                    html+='<input name="id" id="i" type ="hidden" value ="'+counter.id+'">';
                                    html+='<input name="image" onchange="checkLoadImg()" id="im" type= "file">';
                                    html+='<span id = "error"></span>';
                                    html+='<br>';
                                    html+='<input type="button" class="btn btn-primary" data-dismiss="modal" onclick="updateIMG('+index+')" value="Change Image" />';
                                    html+='</center>';
                                    html+='</form>';
                                    $("#imgct").append(html);
                                }                  
                        	});
                             
                        }
                        
                    }
                });
            }
        //Edit IMG
            function updateIMG(index) {
                var token = $("meta[name='_csrf']").attr("content");
                var header = $("meta[name='_csrf_header']").attr("content");
                $(document).ajaxSend(function(e, xhr, options) {
                    xhr.setRequestHeader(header, token);
                });
                var img=$("#im").val();
                if(img === ""){
                   alertBox('No Image!','danger'); 
                }else{
                    $("#formimg").ajaxForm({
                    success:function(data) {
                        $("#imgct").html("");
                        var counter = JSON.parse(data);
                        if(counter.path === "No Image"){
                             $("#title").text('No Image');
                             $("#imgct").html("<div id='add'><span id='remove' class ='text-center' style='color:red'> No Image Please Insert Your Image!! </span></div>");
                             var html = '<br><br><label style="color:green">';
                             html+='Change your picture</label>';
                             html+='<br>';
                             html+='<form method = "POST" id="formimg" enctype="multipart/form-data" action="<%= request.getContextPath() %>/updateIMG" name="${_csrf.parameterName}" value="${_csrf.token}">';
                             html+='<center>';
                             html+='<input name="id" id="i" type ="hidden" value ="'+counter.id+'">';
                             html+='<input name="image" onchange="checkLoadImg()" id="im" type= "file">';
                             html+='<br>';
                             html+='<input type="button" class="btn btn-primary" data-dismiss="modal" onclick="updateIMG('+index+')" value="Change Image" />';
                             html+='</center>';
                             html+='</form>';
                             $("#imgct").append(html);
                             //load data:
                             loadDataList(counter.id,index);                                  
                        }
                        else {
                             $("#title").text(counter.path);
                             $("#imgct").html("");
                             var html='';
                             html+='<img id="pic" class="img-fluid" src ="/PassPort_Management/resources/img/'+counter.path+'">';
                             html+='<br><br><br>';
                             html+='<label id ="upimg" style="color:green">';
                             html+='IMAGE UPDATED!!!</label>';
                             html+='<br>';
                             html+='<form method = "POST" id="formimg" enctype="multipart/form-data" action="<%= request.getContextPath() %>/updateIMG" name="${_csrf.parameterName}" value="${_csrf.token}">';
                             html+='<center>';
                             html+='<input name="id" id="i" type ="hidden" value ="'+counter.id+'">';
                             html+='<input name="image" onchange="checkLoadImg()" id="im" type= "file">';
                             html+='<br>';
                             html+='<input type="button" class="btn btn-primary" data-dismiss="modal" onclick="updateIMG('+index+')" value="Change Image" />';
                             html+='</center>';
                             html+='</form>';
                             $("#imgct").append(html);
                             //load data:
                             loadDataList(counter.id,index);
                        }
//                                alertBox('Image updated!','success'); 
                     },
                     dataType:"text"
                   }).submit();
                }
             }

//RELOAD TABLE {reload style of table}
//COVERT ISSUE TO EXP
        $(document).ready(function() {
            //REload
            $("#reloadTable").click(function(){
                //set default button
                $("#datatable").find("tbody>tr td:nth-child(10)").find("a:eq(1)").hide();//hide all link update
                $("#datatable").find("tbody>tr td:nth-child(10)").find("a:eq(0)").show();//show all link edit
                
                //set default html(for date picker)
                $("#datatable tbody tr td:nth-child(3), td:nth-child(7)").each(function() {
                    
                    if($(this).find('input').length){
                        $(this).html($(this).find('input').val());
                    }
                });
                
                //set default for all td(style)
                $("#datatable").find("tbody>tr").css("border", "");
                $("#datatable").find("tbody>tr td").css("backgroundColor", "#FFFFFF");
                $("#datatable").find("tbody>tr td").css("color", "#333333");
                $("#datatable").find("tbody>tr td").css({cursor: "default"});
                
                //set default aff td (contentedit table)
                $("#datatable").find("tbody>tr td").prop('contenteditable', false);
                
                return false;
            });
        });

//CHECK AWSOME
        function checkAwsome(name, email, passport, issuedate, row)
        {
            if(checkNullValue(name, row, "1", "warning", "Name not null. Please enter it again") || 
                    checkNullValue(email, row, "3", "warning", "Email not null. Please enter it again"))
                    {
                        return true;
                    }

                //validate email
                if(!validateEmail(email))
                    {
                        //border
                        $("#datatable tr:eq("+row+") td:eq(3)").addClass("danger-border");
                        alertBox("Email invalid !!, Please enter it again.", "danger");
                        return true;
                    }
                //validate pasport 
                if(!validatePassport(passport) && passport.replace(/<br>/g, '') !="")
                {
                    //border
                    $("#datatable tr:eq("+row+") td:eq(6)").addClass("danger-border");
                    alertBox("Passport invalid !!, Please enter it again.", "danger");
                    return true;
                }
                
                //check exist, email passport
                var emailExist = $("#datatable tbody tr > td:contains('"+email+"')").length;
                var passportExist = $("#datatable tbody tr > td:contains('"+passport+"')").length;
                if(emailExist > 1)//emailExist !=1 => email exist
                    {
                        //border
                        $("#datatable tr:eq("+row+") td:eq(3)").addClass("error-border");
                        alertBox("Email EXIST !!, Please enter it again.", "danger");
                        return true;
                    }
                if(passportExist > 1 && passport.replace(/<br>/g, '') !="")
                    {
                        //border
                        $("#datatable tr:eq("+row+") td:eq(6)").addClass("error-border");
                        alertBox("Passport EXIST !!, Please enter it again.", "danger");
                        return true;
                    }
                return false;
        }
        
        //FUNCTION CHECK
        function validateEmail(email) {//validate email
              var re = /^[A-Za-z0-9+_.-]+@(.+)$/;
              return re.test(email);
            }
        
        function validatePassport(passport) {//validate passport
              var re = /[A-Z][0-9]{7}/;
              return re.test(passport);
            }
        function checkNullValue(value, row, cell, type, message) {
            if(value.replace(/<br>/g, '') == "")
            {
                //border
                $("#datatable tr:eq("+row+") td:eq('"+cell+"')").addClass("warning-border");
                alertBox(message, type);
                return true;
            }
            return false;
        }
