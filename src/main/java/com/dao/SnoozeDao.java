package com.dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import com.model.Snooze;

public class SnoozeDao {
	//Delimiter used in CSV file
   public static final String COMMA_DELIMITER = ",";
   public static final String NEW_LINE_SEPARATOR = "\n";
   String fileName =System.getProperty("user.home") + "/passport/data/snooze.csv";
   //Check Snooze:
   public boolean checkSnooze(String fileSnooze){
			File f = new File(fileSnooze);
			if(f.exists() && !f.isDirectory()) { 
				return true;
			} else {
				return false;
			}
	}
	//get Snooze
	public Snooze getSnooze(){
	 		String line = "";
	 		Snooze snooze = new Snooze();
	 		try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
	 			while ((line = br.readLine()) != null) {
	 				String[] country = line.split(",");
	 				snooze = new Snooze(country[0],country[1]);
	 			}
	
	 		} catch (Exception e) {
	 			e.printStackTrace();
	 		}
	 		return snooze;
	 	}
   
    //Create Snooze:
	public void createSnooze(Snooze snooze){
            File f = new File(fileName);
		    //check fileName
			FileWriter fw = null;		
			try {
				fw = new FileWriter(fileName);
				//Write a new user object list to the CSV file
				fw.append(snooze.getTime());
				fw.append(COMMA_DELIMITER);
				fw.append(snooze.getList());
				fw.append(NEW_LINE_SEPARATOR);
				System.out.println("CSV file was created successfully SNO0ZE!!!");
				
			} catch (Exception e) {
				System.out.println("Error in CsvFileWriter SNO0ZE !!!");
				e.printStackTrace();
			} finally {
				
				try {
					fw.flush();
					fw.close();
				} catch (IOException e) {
					System.out.println("Error while flushing/closing fileWriter !!!");
	                e.printStackTrace();
				}
				
			}
	}

}
