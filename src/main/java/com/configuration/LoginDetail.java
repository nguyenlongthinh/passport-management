 package com.configuration;  
 import java.util.Collection;  
 import java.util.List;  
 import org.springframework.security.core.GrantedAuthority;  
 import org.springframework.security.core.userdetails.UserDetails;  
 public class LoginDetail implements UserDetails{
	 
      private String userName;  
      private String password;  
      private List<GrantedAuthority> authorities;  
      public LoginDetail(String userName, String password, List<GrantedAuthority> authorities){  
           this.userName = userName;  
           this.password = password;  
           this.authorities = authorities;  
      } 
        
      public LoginDetail(String userName, String password) {
		this.userName = userName;
		this.password = password;
	}

	@Override  
      public Collection<? extends GrantedAuthority> getAuthorities() {  
           return authorities;  
      }  
      @Override  
      public String getPassword() {  
           return password;  
      }  
      @Override  
      public String getUsername() {  
           return userName;  
      } 
 
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@Override  
      public boolean isAccountNonExpired() {  
           return true;  
      }  
      @Override  
      public boolean isAccountNonLocked() {  
           return true;  
      }  
      @Override  
      public boolean isCredentialsNonExpired() {  
           return true;  
      }  
      @Override  
      public boolean isEnabled() {  
           return true;  
      }  
 }  