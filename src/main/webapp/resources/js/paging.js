//Tra ve trang cuoi
var listPage = 0;
function lastPage(){
	var maxListPage = Math.ceil(totalpage/3);
	listPage = maxListPage - 1;
	nextPage();
    pager.showPage(totalpage);
	document.getElementById("listPage1").setAttribute("class","page-item");	
	document.getElementById("listPage2").setAttribute("class","page-item");
	document.getElementById("listPage3").setAttribute("class","page-item");	
	if(totalpage % 3 == 0){
		document.getElementById("listPage3").setAttribute("class","page-item active");	
	}
	else if(totalpage % 3 == 2){
		document.getElementById("listPage2").setAttribute("class","page-item active");	
	}
	else {
		document.getElementById("listPage1").setAttribute("class","page-item active");	
	}
	document.getElementById('nextPage').setAttribute("class","page-item disabled");	
	if(totalpage <= 3){
		document.getElementById('previousPage').setAttribute("class","page-item disabled");	
	}
	listPage--;
}


function clickHere(data){
	document.getElementById("listPage1").setAttribute("class","page-item");	
	document.getElementById("listPage2").setAttribute("class","page-item");
	document.getElementById("listPage3").setAttribute("class","page-item");	
	document.getElementById("listPage"+data).setAttribute("class","page-item active");		
	var maxListPage = Math.ceil(totalpage/3) ;
	if(listPage < maxListPage){
		document.getElementById('checkpage').value = data + listPage*3;
	    pager.showPage(data + listPage*3);
	}else{
		document.getElementById('checkpage').value = 1 + listPage*3 - data;
	    pager.showPage(listPage*3 + data - 3);
	}
	if((listPage == 0) || (totalpage <= 3) )
		document.getElementById('previousPage').setAttribute("class","page-item disabled");	
	else document.getElementById('previousPage').setAttribute("class","");
}

function nextPage(){
	listPage++;
	var maxListPage = Math.ceil(totalpage/3) ;
	
	if(listPage > maxListPage){
		listPage = maxListPage;		
    }
	
	if(listPage < maxListPage){
		document.getElementById('previousPage').setAttribute("class","");
		document.getElementById("listPage1").getElementsByTagName("a")[0].innerHTML = 1 + listPage*3;
		document.getElementById("listPage2").getElementsByTagName("a")[0].innerHTML = 2 + listPage*3;
		document.getElementById("listPage3").getElementsByTagName("a")[0].innerHTML = 3 + listPage*3;
		
		document.getElementById("listPage1").setAttribute("class","page-item active");	
		document.getElementById("listPage2").setAttribute("class","page-item");
		document.getElementById("listPage3").setAttribute("class","page-item");	
		
		document.getElementById('checkpage').value = 1 + listPage*3;
	    pager.showPage(1 + listPage*3);
	}
	else{
		if(listPage == 0){
				$("#listPage1").hide();
				$("#listPage2").hide();
				$("#listPage3").hide();	
				document.getElementById('nextPage').setAttribute("class","page-item disabled");	
				document.getElementById('previousPage').setAttribute("class","page-item disabled");	

		}
		else {
			document.getElementById('nextPage').setAttribute("class","page-item disabled");	
			if(totalpage <= 3){
				document.getElementById('previousPage').setAttribute("class","page-item disabled");	
			}
			if(totalpage%3 == 0){
				document.getElementById('previousPage').setAttribute("class","");
				document.getElementById("listPage1").getElementsByTagName("a")[0].innerHTML = totalpage - 2;
				document.getElementById("listPage2").getElementsByTagName("a")[0].innerHTML = totalpage -1;
				document.getElementById("listPage3").getElementsByTagName("a")[0].innerHTML = totalpage;
				
				document.getElementById("listPage1").setAttribute("class","page-item active");	
				document.getElementById("listPage2").setAttribute("class","page-item");
				document.getElementById("listPage3").setAttribute("class","page-item");	
				
				document.getElementById('checkpage').value = totalpage - 2;
			    pager.showPage(totalpage - 2);
			}
			else if(totalpage%3 == 2){
				document.getElementById("listPage1").setAttribute("class","page-item active");	
				document.getElementById("listPage2").setAttribute("class","page-item");
				document.getElementById("listPage3").setAttribute("class","page-item");	
				
				document.getElementById('previousPage').setAttribute("class","");
				document.getElementById("listPage1").getElementsByTagName("a")[0].innerHTML = totalpage - 1;
				document.getElementById("listPage2").getElementsByTagName("a")[0].innerHTML = totalpage;
				 $("#listPage3").hide();				
				
				document.getElementById('checkpage').value = totalpage - 1;
			    pager.showPage(totalpage - 1);
			}
			else {
				document.getElementById("listPage1").setAttribute("class","page-item active");	
				document.getElementById("listPage2").setAttribute("class","page-item");
				document.getElementById("listPage3").setAttribute("class","page-item");	
				
				document.getElementById('previousPage').setAttribute("class","");
				document.getElementById("listPage1").getElementsByTagName("a")[0].innerHTML = totalpage;
				 $("#listPage2").hide();
				 $("#listPage3").hide();
				
				document.getElementById('checkpage').value = totalpage;
			    pager.showPage(totalpage);
			}			
		}
		
	}
}

function previousPage(){
	listPage--;
	$("#listPage1").show();
	$("#listPage2").show();
	$("#listPage3").show();	
	document.getElementById('nextPage').setAttribute("class","");

	if(listPage < 0){
		listPage = 0;
	}	
	else if(listPage >= 1){
		document.getElementById('previousPage').setAttribute("class","");
		document.getElementById("listPage1").getElementsByTagName("a")[0].innerHTML = 1 + listPage*3;
		document.getElementById("listPage2").getElementsByTagName("a")[0].innerHTML = 2 + listPage*3;
		document.getElementById("listPage3").getElementsByTagName("a")[0].innerHTML = 3 + listPage*3;
		
		document.getElementById("listPage1").setAttribute("class","page-item active");	
		document.getElementById("listPage2").setAttribute("class","page-item");
		document.getElementById("listPage3").setAttribute("class","page-item");	
		
		document.getElementById('checkpage').value = 1 + listPage*3;
	    pager.showPage(1 + listPage*3);
	}
	//truong hop listPage = 0
	else {
			if(totalpage > 3){
				document.getElementById('nextPage').setAttribute("class","page-item");	
			}			
			document.getElementById('previousPage').setAttribute("class","");
			document.getElementById("listPage1").getElementsByTagName("a")[0].innerHTML = 1;
			document.getElementById("listPage2").getElementsByTagName("a")[0].innerHTML = 2;
			document.getElementById("listPage3").getElementsByTagName("a")[0].innerHTML = 3;
			
			document.getElementById("listPage1").setAttribute("class","page-item active");	
			document.getElementById("listPage2").setAttribute("class","page-item");
			document.getElementById("listPage3").setAttribute("class","page-item");	
			
			document.getElementById('checkpage').value = 1;
		    pager.showPage(1);			
			document.getElementById('previousPage').setAttribute("class","page-item disabled");	

		
	}
}


function Pager(rowlength, itemsPerPage,firstRecord, lastRecord, sort) {
    this.rowlength = rowlength;
    this.itemsPerPage = itemsPerPage;
    this.currentPage = 1;
    this.pages = 0;
    this.inited = false;
    this.firstRecord = firstRecord;
    this.lastRecord = lastRecord;
    this.sort = sort;
    
    this.showRecords = function(from, to) {
    	var k=0;
    	if(this.sort != null)
		{
    		this.hideOtherRecordsOfFilter(from, to);
		}
    	else if(this.firstRecord == null && this.lastRecord == null && this.sort ==null){
    		var table = document.getElementById("datatable");
    		for (var i = 0; i < this.rowlength; i++) {
	            if ((i < from) || (i > to)) {
	            	if(table.rows[i+1])
	            	table.rows[i+1].style.display = "none";	            
	            }             	 
	            else if(table.rows[i+1]){	            	 
	            	 if(table.rows[i+1].getAttribute('isSearched') === "false"){
	            		 table.rows[i+1].style.display = "none";
	                 }
	            	 else table.rows[i+1].style.display = "";
	            }
	        }
    	}
    	else{
    		this.hideOtherRecordsOfSearch(from, to);
    	}
    }
    
    this.hideOtherRecordsOfFilter = function(from, to){
    	var count = itemsPerPage;
//    	var table = document.getElementById("datatable");
    	var rows = $('#datatable tbody  tr[isSearched="true"]').get();
        for (var i = 0; i < this.rowlength; i++) {
        	if ((i < from) || (i > to)) 
        			rows[i].style.display = "none";
            else rows[i].style.display = "";           
        }
    }
    
    this.getElement = function(value){
    	
    	var table = document.getElementById("datatable");
		for (var i = 0; i < this.rowlength; i++) {
            if(table.rows[i+1].getAttribute('isSort') == value)
            	return table.rows[i+1];
        }
    }
    
    this.hideOtherRecordsOfSearch = function(from, to){
    	var count = itemsPerPage;
    	
    	var t =-1;
    	
        for (var i = this.firstRecord; i <= this.lastRecord; i++) {
           
	           if(document.getElementById('tr-'+i).getAttribute('isSearched') === "true"){
	        	  t++;
	           }
	           
	           if ((t < from) || (t > to)){
	        	   document.getElementById('tr-'+i).style.display = "none";
	           }
	           else if(document.getElementById('tr-'+i).getAttribute('isSearched') === "true"){
	        	   	count --;
	        	   	document.getElementById('tr-'+i).style.display = "";
	        	   	if (count == 0){
	        	   		for (var j = i+1; j<= lastRecord; j++)
	        	   			document.getElementById('tr-'+j).style.display = "none";
		        	   	return;
	        	   	}
	        	   		
        	   }
	          
        }
    }
    
    this.showPage = function(pageNumber) {
    	
    	if (! this.inited) {
    		alert("not inited");
    		return;
    	}

//        var oldPageAnchor = document.getElementById('pg'+this.currentPage);
//        oldPageAnchor.className = 'pg-normal';
//        
        this.currentPage = pageNumber;
//        var newPageAnchor = document.getElementById('pg'+this.currentPage);
//       newPageAnchor.className = 'pg-selected';
        
        var from = (pageNumber - 1) * Number(itemsPerPage) ;
        var to = from + Number(itemsPerPage) - 1;
        this.showRecords(from, to);
//        console.log(from,to);
    }   
    
    this.prev = function() {
        if (this.currentPage > 1)
            this.showPage(this.currentPage - 1);
    }
    
    this.next = function() {
        if (this.currentPage < this.pages) {
            this.showPage(this.currentPage + 1);
        }
    }                        
    
    this.init = function() {
    	
        var records = (this.rowlength - 1); 
        this.pages = Math.ceil(records / itemsPerPage);
 //       this.pages = this.rowlength;
        this.inited = true;
    }

    this.showPageNav = function(pagerName, positionId) {
    	if (! this.inited) {
    		alert("not inited");
    		return;
    	}
    	var element = document.getElementById(positionId);   	

    	var pagerHtml;
    	
    	pagerHtml= '<span onclick="' + pagerName + '.prev();" class="pg-normal"></span>';// &#171 Prev </span> | 
        for (var page = 1; page <= this.pages; page++) 
        	{
        		pagerHtml += '<span id="pg' + page + '" class="pg-normal" onclick="' + pagerName + '.showPage(' + page + ');" style="display:none">' + page + '</span> ';
        		}
//        for (var i = 1; i < (rows.length); i++) {   
//            if(rows[i].getAttribute('isSearched') === "true"){
//                length1++;                       
//            }
//        }
//		var a =  Math.ceil((this.rowlength-1)/$("#itemperpage").val()); 
		totalpage = Number(this.pages);
        pagerHtml += '<span> of '+Number(this.pages)+'</span>';   
        //pagerHtml += '<span onclick="'+pagerName+'.next();" class="pg-normal"> Next &#187;</span>';            
        
        element.innerHTML = pagerHtml;
    }
    
    this.destroy = (positionId) => {
    	document.getElementById(positionId).innerHTML = '';
    }
}