package com.controller;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import com.configuration.LoginDetail;
import com.model.User;

public class CsvWriterController {
	
	//Delimiter used in CSV file
	private static final String COMMA_DELIMITER = ",";
	private static final String NEW_LINE_SEPARATOR = "\n";
	
	private static final String FILE_HEADER = "id,name,dob,email,alert recipent,project, passport, isuedate,expiredate,remaining,snooze,image";

	public static void writeCsvFile(String fileName,String id, String name, String dob, String email,String emailpm,String project,  String passport, String isuedate,
			String expdate,String remaning,int snooze, String path)  {
		//Create new user objects
		User user = new User(id, name, dob ,email,emailpm,project,passport,isuedate,expdate,remaning,snooze,path);	

		File f = new File(fileName);
		//check fileName
		//file exist
		if(f.exists() && !f.isDirectory()) { 
			try
			{
			    FileWriter fw = new FileWriter(fileName,true); //the true will append the new data
					//Write a new user object list to the CSV file
						fw.append(user.getId());
						fw.append(COMMA_DELIMITER);
						fw.append(user.getName());
						fw.append(COMMA_DELIMITER);
						fw.append(String.valueOf(user.getDob()));
						fw.append(COMMA_DELIMITER);
						fw.append(user.getEmail());
						fw.append(COMMA_DELIMITER);
						fw.append(user.getEmailpm());
						fw.append(COMMA_DELIMITER);
						fw.append(user.getProject());
						fw.append(COMMA_DELIMITER);
						fw.append(user.getPassport());
						fw.append(COMMA_DELIMITER);
						fw.append(String.valueOf(user.getIsueDate()));
						fw.append(COMMA_DELIMITER);
						fw.append(String.valueOf(user.getExpDate()));
						fw.append(COMMA_DELIMITER);
						fw.append(String.valueOf(user.getRemaining()));
						fw.append(COMMA_DELIMITER);
						fw.append(String.valueOf(snooze));
						fw.append(COMMA_DELIMITER);
						fw.append(user.getPath());
						fw.append(NEW_LINE_SEPARATOR);
		
			    fw.close();
			}
			catch(IOException ioe)
			{
			    System.err.println("IOException: " + ioe.getMessage());
			}
		} else {
				
			
			FileWriter fw = null;
					
			try {
				fw = new FileWriter(fileName);
				
				  fw.append(FILE_HEADER.toString());
				  fw.append(NEW_LINE_SEPARATOR);
						
						//Write a new user object list to the CSV file
							fw.append(user.getId());
							fw.append(COMMA_DELIMITER);
							fw.append(user.getName());
							fw.append(COMMA_DELIMITER);
							fw.append(String.valueOf(user.getDob()));
							fw.append(COMMA_DELIMITER);
							fw.append(user.getEmail());
							fw.append(COMMA_DELIMITER);
							fw.append(user.getEmailpm());
							fw.append(COMMA_DELIMITER);
							fw.append(user.getProject());
							fw.append(COMMA_DELIMITER);
							fw.append(user.getPassport());
							fw.append(COMMA_DELIMITER);
							fw.append(String.valueOf(user.getIsueDate()));
							fw.append(COMMA_DELIMITER);
							fw.append(String.valueOf(user.getExpDate()));
							fw.append(COMMA_DELIMITER);
							fw.append(String.valueOf(user.getRemaining()));
							fw.append(COMMA_DELIMITER);
							fw.append(String.valueOf(snooze));
							fw.append(COMMA_DELIMITER);
							fw.append(user.getPath());
							fw.append(NEW_LINE_SEPARATOR);
				System.out.println("CSV file was created successfully !!!");
				
			} catch (Exception e) {
				System.out.println("Error in CsvFileWriter !!!");
				e.printStackTrace();
			} finally {
				
				try {
					fw.flush();
					fw.close();
				} catch (IOException e) {
					System.out.println("Error while flushing/closing fileWriter !!!");
	                e.printStackTrace();
				}
				
			}
		}
		
		
	}
	

	public static void writeCsvFileAdmin(String fileAdmin,String username, String password)  {
		//Create new user objects
		LoginDetail user = new LoginDetail(username, password);

		File f = new File(fileAdmin);
		//check fileName
		//file exist
		if(f.exists() && !f.isDirectory()) { 
			try
			{
			    FileWriter fw = new FileWriter(fileAdmin,true); //the true will append the new data
					
					//Write a new user object list to the CSV file
						fw.append(user.getUsername());
						fw.append(COMMA_DELIMITER);
						fw.append(user.getPassword());
						fw.append(NEW_LINE_SEPARATOR);
		
			    fw.close();
			}
			catch(IOException ioe)
			{
			    System.err.println("IOException: " + ioe.getMessage());
			}
		} else {
				
			
			FileWriter fw = null;
					
			try {
				fw = new FileWriter(fileAdmin);
				
				//Write a new user object list to the CSV file
				fw.append(user.getUsername());
				fw.append(COMMA_DELIMITER);
				fw.append(user.getPassword());
				fw.append(NEW_LINE_SEPARATOR);

				System.out.println("CSV file was created successfully !!!");
				
			} catch (Exception e) {
				System.out.println("Error in CsvFileWriter !!!");
				e.printStackTrace();
			} finally {
				
				try {
					fw.flush();
					fw.close();
				} catch (IOException e) {
					System.out.println("Error while flushing/closing fileWriter !!!");
	                e.printStackTrace();
				}
				
			}
		}
		
		
	}
}