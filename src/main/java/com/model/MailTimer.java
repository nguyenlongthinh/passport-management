package com.model;

public class MailTimer {
	private int time;
	

	public MailTimer() {
	}

	public MailTimer(int time) {
		this.time = time;
	}

	public int getTime() {
		return time;
	}

	public void setTime(int time) {
		this.time = time;
	}
	
	
}
