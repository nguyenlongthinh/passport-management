package com.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import org.apache.poi.ss.formula.eval.ConcatEval;

import com.dao.ExpTimeDao;
import com.dao.SnoozeDao;
import com.dao.UserDao;
import com.model.Snooze;
import com.model.User;


//
public class SentMail extends TimerTask implements ServletContextListener {   
    static String fileName =  System.getProperty("user.home") + "/passport/data/user.csv";
    public static String gt ="";
    public static String id ="";
    public static String re ="";

    String fileNameTimer = System.getProperty("user.home") + "/passport/data/timer.csv";
    String fileNameSnooze = System.getProperty("user.home") + "/passport/data/snooze.csv";
    public static List<String> users;
    public static int timer ;
    //init
    public static String Name(String email) throws FileNotFoundException, IOException{
        String name="";
        String line = "";
        String cvsSplitBy = ",";
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            while ((line = br.readLine()) != null) {
                String[] user = line.split(cvsSplitBy);
                if (user[3].equals(email)) {
                     name=user[1];
                }
                }
            }
        return name;
    }
    
    public static String Expire(String email) throws FileNotFoundException, IOException{
        String name="";
        String line = "";
        String cvsSplitBy = ",";
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            while ((line = br.readLine()) != null) {
                String[] user = line.split(cvsSplitBy);
                if (user[3].equals(email)) {
                     name=user[8];
                }
                }
            }
        return name;
    }
    
    public void init(){
        File f = new File(fileNameTimer);
        String line = "";
        ExpTimeDao edao = new ExpTimeDao();
        if(edao.checkTimer(fileNameTimer)){
            timer = edao.getTimer();//get timer from timer.csv
        }else {
            edao.createExpTime(fileNameTimer);//default 60 day
            timer = edao.getTimer();
        }
    }
   
    public void run() {
        String line = "";
        String cvsSplitBy = ",";
        //Set Snooze:
        Random r = new Random();
        int randomInt = r.nextInt(1000) + 1;
        Date getToday = new Date();
        SnoozeDao snDao = new SnoozeDao();
        Snooze snooze2 = snDao.getSnooze();
        snooze2.setList(String.valueOf(getToday.getTime()));
        snDao.createSnooze(snooze2);
        UserDao uDao = new UserDao();
        List<User> list = uDao.getAllUser();        
        List<User> list2 = new ArrayList<>();
        //Fect:
        for(User user:list){
            if(user.getSnooze()==0){
               user.setSnooze(0);
            }
            else{
                user.setSnooze(user.getSnooze()-1);
            }
            list2.add(user);
        }
        
        File file = new File(fileName);
        if(file.exists()){
            file.delete();
        }
        //Update User:
        for(User user2:list2){            
            CsvWriterController.writeCsvFile(fileName, user2.getId(), user2.getName(), user2.getDob(), user2.getEmail(),user2.getEmailpm(),
            user2.getProject(), user2.getPassport(), user2.getIsueDate(), user2.getExpDate(),user2.getRemaining(), user2.getSnooze(), user2.getPath());
        }
        String snoozesdays = "";
        try (BufferedReader br = new BufferedReader(new FileReader(fileNameSnooze))) {     	
            while ((line = br.readLine()) != null) {            	
                //Update Snooze:
                // use comma as separator
                String[] snooze = line.split(cvsSplitBy);
                snoozesdays = snooze[0];
            }
        }catch (Exception e) {
            System.out.println(e);
        }
        //        
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
        	line = br.readLine();        	
            while ((line = br.readLine()) != null) {            	
                //Update Snooze:
                // use comma as separator
                String[] user = line.split(cvsSplitBy);
                String exday = user[8];
                if(user[4].trim().equals("")) {
                	user[4] = "nullEmail";               	
                }
                int fisnooze = Integer.parseInt(user[10]);
                if(fisnooze<=0){
                    // check passport
                    if (user[6].equals("") || user[7].equals("")) {
                        id = user[0];
                        gt = "Passport rong";
                        re = "passportnull";
                        //gui mail snooze:                        
                        sendMail(user[3],user[4],1,0,exday, snoozesdays);
                    }
                    // check day <60 
                    if(!user[8].equals(""))
                    {
                    	Date today = new Date(System.currentTimeMillis());
                        SimpleDateFormat timeFormat = new SimpleDateFormat("dd/MM/yyyy");
                        String s = timeFormat.format(today.getTime());//                      System.out.println("ngay hien tai "+s);
                        
                        String nowday = s;
                        DateFormat parser = new SimpleDateFormat("dd/MM/yyyy");
                        Date date1, date2;

                        date1 = (Date) parser.parse(exday);
                        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                        date2 = (Date) parser.parse(nowday);
                        Calendar c1 = Calendar.getInstance();
                        Calendar c2 = Calendar.getInstance();

                        c1.setTime(date1);
                        c2.setTime(date2);
                        long a = (c1.getTime().getTime() - c2.getTime().getTime()) / (24 * 3600 * 1000);
                        if (a <= timer) {
                            System.out.println("thoi gian het han con "+a);
                            //
                            id = user[0];
                            re = "expdate";
                            gt = "Sap het han";
                            System.out.println("Sent!!");
                            //gui mail snooze:                            
                            sendMail(user[3],user[4],2,a,exday, snoozesdays);
                            System.out.println("Sent!!");   
                        }
                        if (a <0) {
                            int tam=-1;
//                            sendMailsnooze(user[3],4,0,exday);
                            sendMail(user[3],user[4],2,tam,exday, snoozesdays);
                        }
	                }
	            }
            }
        }catch (Exception e) {
            System.out.println(e);
        }
    }
//    public static void sendMailsnooze(String email,int para, long han, String exp) throws AddressException, FileNotFoundException, IOException {
//    			String to = email; // sender email   
//                String from = "tmatestmail1@tma.com.vn"; // receiver email
//                String host = "smtp.tma.com.vn"; // mail server host
//                String text1 = "\nAs you may know, people working in TMA have great chance to go onsite at anytime of the year. Hence, please ensure your passport available when needed.";
//                String text2 = "\nIn DG4, we have a system which stores passport information of members so that it can remind people when the passport is going to be expired OR if someone doesn't have passport yet.";
//                String text3 = "If you are aware of the notification and proceeding on it, you can snooze this notification for [xx] days (according to policy of DG4 Passport Management)";
//                System.out.println("han123...."+han);
//
//                Properties properties = System.getProperties();
//                properties.setProperty("mail.smtp.host", host);
//
//                Session session = Session.getDefaultInstance(properties); // default                                                                // session
//
//                try {
//                    MimeMessage message = new MimeMessage(session); // email message
//                    message.setFrom(new InternetAddress(from)); // setting header fields
//                message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));   
//                    //noi dung mail gui
//                    //truong hop passport null
//                    String a=Name(email);           
//                    //truong hop qua han 60 ngay
//                    if(para==4)
//                    {
//                        //lay gia tri Snooze:
//                        SnoozeDao snDao = new SnoozeDao();
//                        Snooze snooze = snDao.getSnooze();
//                        String date = snooze.getTime();
//                        message.setSubject("TMA - DG4 Passport Management System Reminder "+"("+gt+")"); // subject line
//                        // actual mail body
//                        message.setText("Dear "+email+","+text1+text2+": http://10.1.0.13/passport/goconfirm?id="+id+"&re="+re+"\nMa xac nhan cua ban: "+snooze.getList());
//                        System.out.println("Sent Snooze:"+gt);
//                    }
//                    // Send message
//                    Transport.send(message);
//                    System.out.println("Email Sent SENTNOOZE...."+email);
//                } catch (MessagingException mex) {
//                    mex.printStackTrace();
//                }
//            }
 
    public static void sendMail(String email,String emailpm, int para, long han, String exp, String snoozedays) throws AddressException, FileNotFoundException, IOException {
        String to = email; // sender email
        String[] recipientList = emailpm.split(";");
        String Subject = "TMA - DG4 Passport Management System Reminder";
        String text1 = "\n\n   As you may know, people working in TMA have great chance to go onsite at anytime of the year. Hence, please ensure your passport available when needed.";
        String text2 = "\n   In DG4, we have a system which stores passport information of members so that it can remind people when the passport is going to be expired OR if someone doesn't have passport yet.";
        String text3 = "\n   If you are aware of the notification and proceeding on it, you can snooze this notification for "+snoozedays+" days (according to policy of DG4 Passport Management)";
        String snoozetext = "\n   To snooze the reminder, please use the following link:";
        
        String expire1 = "\n   You receive this email because your passport is going to be expired in "+han+" days.";
        String expire2 = "Please proceed to extend or renew your passport. After done, please send your passport information to ";
        String noavailable = "\n   You receive this email because you haven't update your passport information in the system yet. If you don't have a passport yet, please register one ASAP. If you already have one, please send you passport information to ";
        
        String conclusion = "\n\nThank you for your cooperation!\nDG4 Passport Management Team";
        
        SnoozeDao snDao = new SnoozeDao();
        Snooze snooze = snDao.getSnooze();
        String date = snooze.getTime();
        

	        InternetAddress[] recipientAddress = new InternetAddress[recipientList.length];
	        int counter = 0;
	        for (String recipientt : recipientList) {
	            recipientAddress[counter] = new InternetAddress(recipientt.trim());
	            counter++;
	        } 
        
        String from = "tmatestmail@tma.com.vn"; // receiver email
        String host = "smtp.tma.com.vn"; // mail server host

          Properties properties = System.getProperties();
         properties.setProperty("mail.smtp.host", host);
        Session session = Session.getDefaultInstance(properties); // default                                                                // session

        try {
            MimeMessage message = new MimeMessage(session); // email message
            message.setFrom(new InternetAddress(from)); // setting header fields
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
           //message.setRecipients(Message.RecipientType.TO, recipientAddress);
            if(emailpm != "nullEmail"){
                message.addRecipients(Message.RecipientType.CC, recipientAddress);
            }
            
            //noi dung mail gui
            //truong hop passport null
            String a=Name(email);
            String expire=Expire(email);
            if(para==1)
            {
                message.setSubject(Subject); // subject line   
                // actual mail body
                message.setText("Dear "+email+","+text1+text2+noavailable+emailpm+"."+text3+snoozetext+" http://10.1.0.13/passport/goconfirm?id="+id+"&re="+re+conclusion);
            }
            //truong hop issue date null
            if(para==3)
            {
                message.setSubject(Subject); // subject lin
                // actual mail body
                message.setText("Dear "+email+","+text1+text2+noavailable+emailpm+"."+text3+snoozetext+" http://10.1.0.13/passport/goconfirm?id="+id+"&re="+re+conclusion);
            }
            //truong hop qua han 60 ngay
            if(para==2)
            { if(han >=0){
                message.setSubject(Subject); // subject line   
                // actual mail body
                message.setText("Dear "+email+","+text1+text2+expire1+date+expire2+emailpm+"."+text3+snoozetext+" http://10.1.0.13/passport/goconfirm?id="+id+"&re="+re+conclusion);
            }
            else if(han==-1){
                message.setSubject(Subject); // subject line   
                // actual mail body
                message.setText("Dear "+email+","+text1+text2+expire1+date+expire2+emailpm+"."+text3+snoozetext+" http://10.1.0.13/passport/goconfirm?id="+id+"&re="+re+conclusion);
            }
            }
            // Send message
            Transport.send(message);
            System.out.println("Email Sent successfully...."+email);
        } catch (MessagingException mex) {
            mex.printStackTrace();
        }
    }
    //them email vao danh sach email da gui
    public static List<String> Sent(String email){
        users = new ArrayList<String>();
        users.add(email);
        return users;
    }
   
    public static Date getTomorrowTime() {    	
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Asia/Ho_Chi_Minh"));
        calendar.set(Calendar.HOUR_OF_DAY, 12);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);

        Date alarmTime = calendar.getTime();

        return alarmTime;
    }

    // call this method from your servlet init method
    public static void startTask() {
        SentMail task = new SentMail();
        Timer timer = new Timer();
        timer.schedule(task, getTomorrowTime());
    }
   
      @Override
      public void contextDestroyed(ServletContextEvent arg0) {
        System.out.print("Shutdown system");
      }

      @Override
      public  void contextInitialized(ServletContextEvent arg0) {
          init();
          startTask();
      }     

}
