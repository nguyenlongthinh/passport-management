package com.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.configuration.LoginDetail;
import com.dao.LoginDao;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
public class LoginController {
	String fileAdmin = System.getProperty("user.home") + "/passport/data/admin.csv";
	
	 BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
	  
	@RequestMapping(value = {"/index", "/"}  ,method = RequestMethod.GET)
	public ModelAndView welcomePage() {

		ModelAndView model = new ModelAndView();
		model.addObject("title", "Spring Security Custom Login Form");
		model.addObject("message", "This is welcome page!");
		model.setViewName("index");
		return model;

	}

	@RequestMapping(value = "/admin**", method = RequestMethod.GET)
	public ModelAndView adminPage() {

		ModelAndView model = new ModelAndView();
		model.addObject("title", "Spring Security Custom Login Form");
		model.addObject("message", "This is protected page!");
		model.setViewName("index");

		return model;

	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login(@RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "logout", required = false) String logout) {

		ModelAndView model = new ModelAndView();
		if (error != null) {
			model.addObject("error", "Invalid username and password!");
		}

		if (logout != null) {
			model.addObject("msg", "You've been logged out successfully.");
		}
		model.setViewName("login");

		return model;

	}
	
	   @RequestMapping(value="/logout", method = RequestMethod.GET)
	    public String logoutPage (HttpServletRequest request, HttpServletResponse response) {
	        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	        if (auth != null){   
	            new SecurityContextLogoutHandler().logout(request, response, auth);
	        }
	        return "redirect:/login?logout";
	    }
	
	
	
	//CHECK OLD PASS TO CHANGE
	@RequestMapping(value = "/checkoldpass", method = RequestMethod.GET)
	@ResponseBody
	public String checkOLDPass(@RequestParam(value="password") String oldpass,@RequestParam(value="username") String username) {
		String flag = "noexist";
		String line = ""; 

		
		List<LoginDetail> listall = new ArrayList<LoginDetail>();
				try (BufferedReader br = new BufferedReader(new FileReader(fileAdmin))) {
					while ((line = br.readLine()) != null) {
						// use comma as separator
						String[] admin = line.split(",");
						listall.add(new LoginDetail(admin[0], admin[1]));
					}

				} catch (Exception e) {
				}
				for (int i = 0; i < listall.size(); i++) {
					
					boolean test=passwordEncoder.matches(oldpass, listall.get(i).getPassword());
					if ((listall.get(i).getUsername().equals(username)) && (test == true)){
						
//						System.out.println(listall.get(i).getPassword());
//						System.out.println(passwordEncoder.encode("admin"));
//						System.out.println(passwordEncoder.encode(oldpass));
						
						return flag = "noexist";	
					}
					else  {
						flag = "exist";
					}
				}
						
		return flag;
	}
	
	
	@RequestMapping(value = "/changepassword", method = RequestMethod.POST)
	public String ChangePassword(HttpServletRequest request,ModelMap mm){
		String change ;
	
		String username=request.getParameter("username");
		String currentPassword=request.getParameter("password");
		String newpass=request.getParameter("newpassword");
		String repass=request.getParameter("renewpassword");
		
		String flag = "noexist";
		String line = ""; 
		
		List<LoginDetail> listall = new ArrayList<LoginDetail>();
				try (BufferedReader br = new BufferedReader(new FileReader(fileAdmin))) {
					while ((line = br.readLine()) != null) {
						// use comma as separator
						String[] admin = line.split(",");
						listall.add(new LoginDetail(admin[0], admin[1]));
					}

				} catch (Exception e) {
				}
				for (int i = 0; i < listall.size(); i++) {
					
					boolean test=passwordEncoder.matches(currentPassword, listall.get(i).getPassword());
					if ((listall.get(i).getUsername().equals(username)) && (test == true) && (newpass.equals(repass) && (!newpass.equals("")))){
						LoginDao udao = new LoginDao();	
						change = udao.updatePass(username, passwordEncoder.encode(newpass));
						flag = "noexist";
					
					}

					else  {
						flag = "exist";
					}
				}

		return "index";

		}
	
	//Insert User:
			@RequestMapping(value = "/insertAccount", method = RequestMethod.POST)
			@ResponseBody
			public String AddAccount(HttpServletRequest request, ModelMap mm) throws JsonProcessingException{
				String username = request.getParameter("username");
				String password = request.getParameter("password");				
				
				System.out.println(username);
				System.out.println(password);
				
				String jsons = null;
				String line = "";
				String cvsSplitBy = ",";
				try (BufferedReader br = new BufferedReader(new FileReader(fileAdmin))) {
					while ((line = br.readLine()) != null) {
						// use comma as separator
						String[] admin = line.split(cvsSplitBy);
						if (admin[0].equals(username)) {
							return jsons="exists";
						}
					}

				} catch (Exception e) {
				}
				

			    CsvWriterController.writeCsvFileAdmin(fileAdmin,username,passwordEncoder.encode(password));
				
				String jsonsA = null;
				List<LoginDetail> listall = new ArrayList<>();
				listall.add(new LoginDetail(username,passwordEncoder.encode(password)));
				ObjectMapper mapper = new ObjectMapper();
				jsonsA = mapper.writeValueAsString(listall);
				return jsonsA;
				
		    }
}
