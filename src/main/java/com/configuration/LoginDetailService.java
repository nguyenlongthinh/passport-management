 package com.configuration;  
 import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;  
 import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.core.GrantedAuthority;  
 import org.springframework.security.core.authority.SimpleGrantedAuthority;  
 import org.springframework.security.core.userdetails.UserDetails;  
 import org.springframework.security.core.userdetails.UserDetailsService;  
 import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.configuration.LoginDetail;
import com.model.User;  

 public class LoginDetailService implements UserDetailsService{  
	 
	 String fileAdmin =   System.getProperty("user.home") + "/passport/data/admin.csv";
	 
	  BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
	 
      @Override  
      public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException { 
    		List<LoginDetail> listall = new ArrayList<>();
    	  String line = "";
    		String cvsSplitBy = ",";
    		try (BufferedReader br = new BufferedReader(new FileReader(fileAdmin))) {
    			while ((line = br.readLine()) != null) {	
    				String[] admin = line.split(cvsSplitBy); 
    				listall.add(new LoginDetail(admin[0], admin[1]));
    	//  System.out.println(passwordEncoder.encode(admin[1]));
    				
    	for (int i = 0; i < listall.size(); i++) {
    	if (listall.get(i).getUsername().equals(userName)) {
    		 String password = listall.get(i).getPassword();
             List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();  
             SimpleGrantedAuthority authority = new SimpleGrantedAuthority("ROLE_USER");  
             authorities.add(authority);  
            LoginDetail userDetail = new LoginDetail(userName, password, authorities);  
             return userDetail;
      	 
    	}
    	  if(listall.get(i).getUsername().equalsIgnoreCase(userName)) throw new UsernameNotFoundException("User name not found"); 
    	  
    	}  
      }  
 }
      catch (Exception e) {
  	}

	return null;

	}
      
      
 }
