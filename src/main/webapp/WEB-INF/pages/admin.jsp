<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head>
<title>Home</title>
<script src="<c:url value="/resources/js/paging.js"/>"></script>
<link rel="stylesheet" type="text/css"
href="<c:url value="/resources/css/bootstrap.min.css"/>" />
<link rel="stylesheet" href="<c:url value="/resources/css/bootstrap-tagsinput.css"/>">
<script src="<c:url value="/resources/js/jquery-2.2.4.min.js"/>"></script>
<script src="<c:url value="/resources/js/bootstrap-tagsinput.js"/>"></script>
<script src="<c:url value="/resources/js/bootstrap-tagsinput-angular.js"/>"></script>
<script src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/jquery.steps.css"/>" />
<link rel="stylesheet" href="/resources/demos/style.css"><link rel="stylesheet" href="/resources/demos/a.css"> 
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/jquery.steps.css"/>" />
<link rel="stylesheet" href="/resources/css/style.css">
<link rel="stylesheet" href="<c:url value="/resources/css/ppmagage.css"/>">
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/jquery.steps.css"/>" />
<link rel="stylesheet" href="/resources/css/style.css">
<link rel="stylesheet" href="<c:url value="/resources/css/ppmagage.css"/>">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="<c:url value="/resources/js/jquery.steps.min.js"/>"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="<c:url value="/resources/js/jquery-ui.js"/>"></script>
<script src="<c:url value="/resources/js/functions.js"/>"></script>
<!-- Form Ajax -->
<script src="<c:url value="/resources/js/jquery.form.min.js"/>"></script>

<style type="text/css">
fieldset.scheduler-border {
	border: 1px groove #888888 !important;
	padding: 1em 1em 0em 1em !important;
	margin: -20 0 0.5em 0 !important;
	width: auto;
	-webkit-box-shadow: 0px 0px 0px 0px #000;
	box-shadow: 0px 0px 0px 0px #000;
}
legend.scheduler-border {
	font-size: 1.2em !important;
	font-weight: bold !important;
	text-align: left !important;
	width: auto;
	padding: -3 10px;
	border-bottom: none;
}

legend.scheduler-border {
	font-size: 1.2em !important;
	font-weight: bold !important;
	text-align: left !important;
	width: auto;
	padding: 0 10px;
	border-bottom: none;
}

   .settingStyle {
	border: 1px groove #000099 !important;
	margin: -20 0 0.5em 0 !important;
	width: auto;
	-webkit-box-shadow: 0px 0px 0px 0px #000;
	box-shadow: 0px 0px 0px 0px #000;
    }
	.celltt{
		color: red;
	}
   .pg-normal {
       color: black;
       font-weight: normal;
       text-decoration: none;    
       cursor: pointer;    
   }
   .pg-selected {
       color: black;
       font-weight: bold;        
       text-decoration: underline;
       cursor: pointer;
   }
   .form-control{
   height:28px;
   }
</style>
</head>

<body>
<c:url value="/j_spring_security_logout" var="logoutUrl" />
	<form action="${logoutUrl}" method="post" id="logoutForm">
		<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />
	</form>
	<script>
		function formSubmit() {
			document.getElementById("logoutForm").submit();
		}
	</script>

	
<div class="container-fluid" style="width:100%;">
<!--notification alert -->
      	<div id="result" class="clearfix"></div>
		<div class="row"
			style="margin-top: 1%; font-size: 12px; font-weight: bold; margin-bottom: -1px; height: auto">
			<div class="col-md-3 " style="padding-right: 5%;">
				<div class="card">
					<div class="btn btn-primary" style="width: 100%;">
						<center>
							<b>Information</b>
						</center>
					</div>
					<div class="panel-body">
						<div class="card-block">
							<form method="post" id="addAccount" action="<%=request.getContextPath() %>/insertAccount"> 
								<div id="wizard" style="width: 110%;margin: -5%;">
									<div class="form-group" >
										<br>
										<fieldset class="scheduler-border">									
										  <legend class="scheduler-border" style="color:red;margin-bottom:2px;">Profile</legend>
											  <div class="form-group">
                                                <label for="name" style="margin-top:-10px;">User Name</label>
                                                <input type="text" id="username" class="form-control" placeholder="" name="username" required="required"/>
                                            <span id="errorid"></span>
                                            </div>
										    <div class="form-group">
                                                   <label for="name" style="margin-top:-8px;">Passport</label>
                                                   <input type="text" id="password" class="form-control" name="password" placeholder="" required="required"/>
                                                   <span id="errorname"></span>
                                            </div>
										</fieldset>
									</div>
								</div>
                                <input class="btn btn-primary btn-sm" type="submit" onsubmit="InsertAccount()" value="Submit" style="margin-left: 25px;width: 40%;margin-top: 5%;">   
                                <input type="reset" id="reset" class="btn btn-primary btn-sm" value="Reset" style="margin-top: 5%;">   
                          <input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />
                          
                           </form>
                        </div>
                    </div>
                </div>
            </div>
			
			<div class="col-md-9" style="margin-left: -4%;width:75%;">
			<div class="row" style="width:108%; border:1px solid #ccc;">
		<div class="btn btn-primary" style="width:100%;overflow: auto;" >
			<center><b>USER INFORMATION</center></b>
		    <c:if test="${pageContext.request.userPrincipal.name != null}">
			<h5 class="pull-right" style="margin-top: -2%; margin-bottom: -10%; color:white;">
				Welcome : ${pageContext.request.userPrincipal.name} | <a
					href="javascript:formSubmit()" style="color: yellow;"> Logout</a>
			</h5>
			</c:if>
			
		</div>		
			<div class="col-md-12">
				
			<br/>
			<input type="text" id="myInput" onkeyup="searchTable()" placeholder="Search..." title="Type in a name">
				<!-- LINK EXPORT -->
				  <!-- Modal -->
					  <div class="modal fade" id="myModal" role="dialog">
						    <div class="modal-dialog">					    
						      <!-- Modal content-->					      
						      <div class="modal-content">
						        <div class="modal-body">
						        <button type="button" class="close" data-dismiss="modal">&times;</button>
						          <h4 style="color: red; margin-left:25%; margin-bottom:10%"><b>Import new CSV file</b></h4>

						          <form action="<%= request.getContextPath()%>/uploadFile" method="POST" enctype="multipart/form-data" id="importCSV" >
								    <div class="form-group" style="margin-left: 20%;">								       
								        <input id="csv" style="margin-bottom:5%" type="file" name="file1" onchange="checkLoadCSV()"/>
										<button id="uploadCSV"  type="button" class="btn btn-primary"  style="margin-left:25%" data-dismiss="modal" id="upload" onclick="getLoadCSV()">Upload</button>
								    </div>
								</form>
						        </div>
						      </div>						      
						    </div>
					  </div>
					  
				<!-- LINK RELOAD TABLE -->
			    <br/><br/>
				<table class="table table-striped table-bordered " cellpadding="0"
					cellspacing="0" id="datatable" style="margin-right: 5px; font-size:13px;">
					<thead>
						<tr style="font-weight:bold; background:#FFFFCC">
							<td style="text-align: center;" onclick="sortTable(0)">User Name</td>
							<td style="text-align: center;" onclick="sortTable(1)">Password</td>
							<td style="text-align: center;" onclick="sortNumber(2)">Level</td>
							<td style="text-align: center;" onclick="sortDate(3)">Created_At</td>
							<td style="text-align: center;" >Action</td>
						</tr>
					</thead>
					
					<tbody id="content">
					</tbody>
				</table>
					<div class="row">
						<span style="padding:5px 5px 5px 25px">Show:</span>
						<select id="itemperpage" name="choose" >
							<option value="5">5 items</option>
							<option value="10">10 items</option>
							<option value="25">25 items</option>
							<option value="50">50 items</option>
							<option value="100"  selected="selected">100 items</option>
						</select>
						<span id="previous" style="padding:5px 5px 5px 25px">&#171 Previous |</span>
						<span style="padding:5px 5px 5px 0px">Page</span><input id="checkpage" name="checkpage" onKeyUp="numbercOnly(this)" min="1" style="width:4%" />						
						<div id="pageNavPosition" style="padding: 5px 5px 5px 5px; ">  </div>
						<span id="next" style="padding:5px 5px 5px 0px">| Next &#187</span>
					</div>
				</div>
			</div>
	  </div>
	  <!-- modal show image -->
		  <div class="modal fade" id="modalimg" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
				        <div class="col-sm-10"><h5 id="title" class="modal-title" id="exampleModalLabel"></h5></div>
				        <div class="col-sm-2"><button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                    <span aria-hidden="true">&times;</span></button>
	                    </div>
			       </div>
			      <div class="modal-body" id="imgct" style="text-align: center">
			      </div>
			      <div class="modal-footer">
			      </div>
			    </div>
			  </div>
	     </div>
	     
	      <!-- Modal DELETE -->
			<div class="modal fade" id="modaldelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			         <div class="col-sm-10"><h5 class="modal-title" id="exampleModalLabel">Delete Confirm</h5></div>
			         <div class="col-sm-2"><button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button></div>
			      </div>
			      <div class="modal-body">
			        <div class="col-sm-10">
			        <span style="font-size:14px">Are you sure delete id: <span style="color:green;" id ="setid"></span></span>
			        <input id ="index" type="hidden">
			        </div>
			      </div>
			      <div class="modal-footer">
			        <button type="button" id = "btdelete" data-dismiss="modal"  onclick="deleteUser()" class="btn btn-primary">Yes</button>
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
			      </div>
			    </div>
			  </div>
			</div>
			</div>
  </div>
  	
  
</body>

<!-- save expire day -->
<script type="text/javascript">
//INSERT ACCOUNT
function InsertAccount(){
	 var username = $("#username").val();     
     var password = $("#password").val();
        	$("#addAccount").ajaxForm({
    		    success:function(data) {
    		    	var table = document.getElementById("datatable");
    				var rows = table.getElementsByTagName("TR");    					    
    	    	if(data == "exists"){
                $( '<div class="alert alert-danger" style="width:30%;position:fixed;z-index:1000;margin-top:10%;margin-left:40%;height:15%;line-height:50%;text-align:center"><br/><br/><br/><br/><b>DATA EXIST! PLEASE ENTER NEW PROFILE!!!!!!</b></div>')
                                 .appendTo('#result')
                                 .trigger('showalert')
                                 .fadeOut(5000);
    	    		    	}    
    		    	
    		    	    var html = '';
    					var counter = JSON.parse(data);
    					for( i = 0;i<counter.length; i++ ){   				
							    html='<tr id="tr-'+(rows.length-1)+'" issearched="true">';
							    html+='<td>'+counter[i].userName+'</td>';
							    html+='<td>'+counter[i].password+'</td>';	
							    html+='<td>'+counter[i].password+'</td>';
							    html+='<td>'+counter[i].password+'</td>';
							    html+='<td><a data-toggle="modal" data-target="#modalimg" title="Show image" style ="color:green" onclick = "showIMG('+counter[i].id+','+(rows.length-1)+')">'+counter[i].path+'</a></td>';
							    html += '<td><a id="'+ i+ '" onclick="editInfo('+(rows.length-1)+')"  title="Update" style="margin-right: 3%"><span class="glyphicon glyphicon-pencil"></span> </a> '
								+'<a id="'+ i+ '" onclick="updateInfo('+(rows.length-1)+')" style="display:none"  title="Save" style="margin-right: 3%"><span class="glyphicon glyphicon-floppy-disk"></span> </a> '
								+'<a id="'+i+'" data-toggle="modal" data-target="#modaldelete" onclick="loadInfoDelete('+ counter[i].id + ','+(rows.length-1)+')"  title="Xoa" style="margin-right: 3%"><span class="glyphicon glyphicon-trash"></span> </a>'
								+'<a id="'+ i+ '" onclick="reloadInfo('+(rows.length-1)+')" style="display:none" title="Reload" style="margin-right: 3%"><span class="glyphicon glyphicon-remove"></span> </a> </td>';
								html += '</tr>';
							    $("#content").prepend(html);
	    				        $( '<div class="alert alert-success ppalert" id="success-alert">Insert information is success!!!</div>').appendTo('#result').trigger('showalert').fadeOut(5000);
	    					}
	    					var table, rows;
	    					table = document.getElementById("datatable");
	    					rows = table.getElementsByTagName("TR");
	    					pager = new Pager(rows.length,$("#itemperpage").val());
	    				    pager.init('datatable'); 
	    				    pager.showPageNav('pager', 'pageNavPosition'); 
	    				    pager.showPage(1); 
    		     },dataType:"text"
    		  }).submit();   
}
//Check Empty CSV 
//CHANGE TEXT IN FILTER
$(document).ready(function () {
	$("#uploadCSV").click(function () {		
		if($("#csv").val() == ''){
			$( '<div class="alert alert-danger ppalert" id="success-alert">No CSV file!!!</div>')
            .appendTo('#result')
            .trigger('showalert')
            .fadeOut(5000);
		}
	});
});
//Check Load CSV
function checkLoadCSV() {
				var a = $("#csv").val().split('.');				
				if(a[1] !== "csv"){
					alert('File is not csv!');
					$("#csv").val('');
				}
			}
//get CSV
function getLoadCSV(){
     	$("#importCSV").ajaxForm({
	    success:function(data) {
	    	alertBox("Update! expire day succes","success");
		     },
		   dataType:"text"
		   }).submit();
 }
//CHANGE TEXT IN FILTER
$(document).ready(function () {
	$("#selectbox").click(function () {
		 $("select option[value='expiredate']").text("Expiry date in "+$("#expTime").val());
	});
});
//FILTER
$(document).ready(function() {
	$('#selectbox').change(function(event){
		pager.destroy('pageNavPosition');
		
		if($('#selectbox').val() == "nofilter")
			{		
				var table, rows;
				table = document.getElementById("datatable");
				rows = table.getElementsByTagName("TR");
				for (i = 1; i < rows.length; i++) {
					rows[i].style.display = "";
					rows[i].setAttribute("isSearched", "true"); 
				}
				pager = new Pager(rows.length,$("#itemperpage").val());				
			    pager.init('datatable'); 
			    pager.showPageNav('pager', 'pageNavPosition'); 
			    document.getElementById('checkpage').value=1;
			    pager.showPage(1);
			}
		else if($('#selectbox').val() == "yearonly")
		{
			var  table, tr, td, i, k=0;
			  table = document.getElementById("datatable");
			  tr = table.getElementsByTagName("tr");
			  var lenght = 0;
			  var firstRecord = null;
			  var isSetfirstrecord = false;
			  var lastRecord = 0;
			  
			  for (i = 1; i < tr.length; i++) {
				  td = tr[i].getElementsByTagName("td")[2].innerHTML;
			      if (td.search("/") < 0) {
			    	  k++;
			        tr[i].style.display = "";
			        tr[i].setAttribute("isSearched", "true");
			       // tr[i].setAttribute("isFil", );
			        lenght++;
			         if(!isSetfirstrecord){
			        	firstRecord = i - 1;
			        	isSetfirstrecord = true;
			        }	        	
			        lastRecord = i-1;
			      } 
			      else {
			        tr[i].style.display = "none";
			        tr[i].setAttribute("isSearched", "false");
			      }
			  }	  
			  pager.destroy('pageNavPosition');
			  pager = new Pager(lenght,$("#itemperpage").val(), firstRecord, lastRecord);				
			  pager.init(); 
			  pager.showPageNav('pager', 'pageNavPosition');
			  document.getElementById('checkpage').value=1;
			  pager.showPage(1);			
		}
		else if($('#selectbox').val() == "nopassport")
		{
	    	  var  table, tr, td, i;
			  table = document.getElementById("datatable");
			  tr = table.getElementsByTagName("tr");
			  var lenght = 0;
			  var firstRecord = null;
			  var isSetfirstrecord = false;
			  var lastRecord = 0;
			  for (i = 1; i < tr.length; i++) {
			    td = tr[i].getElementsByTagName("td")[6].innerHTML;
			      if (td == "") {			    	 
			        tr[i].style.display = "";
			        tr[i].setAttribute("isSearched", "true"); 
			        lenght++;
			         if(!isSetfirstrecord){
			        	firstRecord = i - 1;
			        	isSetfirstrecord = true;
			        }	        	
			        lastRecord = i-1;
			      } 
			      else {
			        tr[i].style.display = "none";
			        tr[i].setAttribute("isSearched", "false");
			      }
			  }	  
			  pager.destroy('pageNavPosition');
			  pager = new Pager(lenght,$("#itemperpage").val(), firstRecord, lastRecord);				
			  pager.init(); 
			  pager.showPageNav('pager', 'pageNavPosition'); 
			  document.getElementById('checkpage').value=1;
			  pager.showPage(1);
		}
		else if($('#selectbox').val() == "expiredate")
		{
			  var  table, tr, td, i;
			  table = document.getElementById("datatable");
			  tr = table.getElementsByTagName("tr");
			  var lenght = 0;
			  var firstRecord = null;
			  var isSetfirstrecord = false;
			  var lastRecord = 0;			  
			   for (i = 1; i < tr.length; i++) {
				   var now = new Date();
				   var date1 =tr[i].getElementsByTagName("td")[8].innerHTML.split('/');
				   var exDate = new Date(date1[2],date1[1]-1,date1[0]);			 	
				   var difftime = Math.abs(exDate.getTime() - now.getTime());
				   var diffDays = Math.ceil(difftime / (1000 * 3600 * 24)); 
				   var tam=$("#expTime").val();
			      if (diffDays <= tam) {			    	 
			        tr[i].style.display = "";
			        tr[i].setAttribute("isSearched", "true"); 
			        lenght++;
			         if(!isSetfirstrecord){
			        	firstRecord = i - 1;
			        	isSetfirstrecord = true;
			        }	        	
			        lastRecord = i-1;
			      } 
			      else {
			        tr[i].style.display = "none";
			        tr[i].setAttribute("isSearched", "false");
			      }
			  }	   
			  pager.destroy('pageNavPosition');
			  pager = new Pager(lenght,$("#itemperpage").val(), firstRecord, lastRecord);				
			  pager.init(); 
			  pager.showPageNav('pager', 'pageNavPosition');
			  document.getElementById('checkpage').value=1;
			  pager.showPage(1); 
		}		
	});
});
// next
$(document).ready(function() {   
			$('#next').click(function() {				
				if($('#checkpage').val() >= totalpage){
					  $( '<div class="alert alert-danger ppalert" id="success-alert">Invalid!!!</div>')
                      .appendTo('#result')
                      .trigger('showalert')
                      .fadeOut(5000);
					  document.getElementById('checkpage').value=totalpage;
					  pager.showPage(totalpage);
  									}
				else{
					var nextpage = Number($('#checkpage').val()) + 1;
					document.getElementById('checkpage').value=nextpage;
					pager.showPage(nextpage);
					
				}
			});
	});
	
//Previous 
$(document).ready(function() {   
			$('#previous').click(function() {				
				if($('#checkpage').val() <= 1){
					  $( '<div class="alert alert-danger ppalert" id="success-alert">Invalid!!!</div>')
                      .appendTo('#result')
                      .trigger('showalert')
                      .fadeOut(5000);
					  document.getElementById('checkpage').value=1;
					  pager.showPage(1);
  									}
				else{
					var previouspage = Number($('#checkpage').val()) - 1;
					document.getElementById('checkpage').value=previouspage;
					pager.showPage(previouspage);
					
				}
			});
	});
    //check page
	    function numbercOnly(e)
	{
	    var val = e.value.replace(/[^\d]/g, "");
	    if(val != e.value)
	        e.value = val;
	}
	$(document).ready(function() {   
			$('#checkpage').change(function() {	
				if($('#checkpage').val() > totalpage){
					  $( '<div class="alert alert-danger ppalert" id="success-alert">Invalid!!!</div>')
                      .appendTo('#result')
                      .trigger('showalert')
                      .fadeOut(5000);
					  document.getElementById('checkpage').value=totalpage;
					  pager.showPage(totalpage);
  									}
				else if($('#checkpage').val() < 1){
					  $( '<div class="alert alert-danger ppalert" id="success-alert">Invalid!!!</div>')
                      .appendTo('#result')
                      .trigger('showalert')
                      .fadeOut(5000);
					  document.getElementById('checkpage').value=1;
					  pager.showPage(1);
  									} 
				else pager.showPage($('#checkpage').val());
			});
	});
	
	// Export file csv to xls (excel)
	$("#exportFile").click(function(e) {
			var rows = [];
			$('#datatable tr:visible:gt(0)').each(function(index, tr) {//get all row visible but not first
			   var lines = $('td', tr).not('td:last').map(function(index, td) {
			        return $(td).text();
			    });
			   
			   rows.push(JSON.stringify(lines.get()));
			});

			if(rows.length <=0)
			{
				alertBox('No data export','danger');
			} else if(rows.length==1)
			{
				var token = 1;//case export le 1 row
			} else 
			{
				var token = 2;//case export gt 1 row
			}
			
			$.ajax({
						url : 'exportfile',
						type : 'POST',
						data : {
							datas : rows,
							token : token
						},
						success : function(data) {
						if(data == "success")
							{
							   window.location.href = "<%=request.getContextPath() %>/resources/excel/user.xls";
							}
						}
					});
			return false;
		});
	
	//export all
	$("#exportAll").click(
			function(e) {
				$.ajax({
							url : 'exportFileAll',
							type : 'POST',
							success : function(data) {
							if(data == "success")
								{
								   window.location.href = "<%=request.getContextPath() %>/resources/excel/user.xls";
								}
							}
						});
				return false;
			});
</script>

<!-- script load info 'user for editing -->
<!-- FOR UPDATE BY MODAL -->
<script type="text/javascript">
	$("#userEditInfoForm")
			.submit(
					function(e) {
						$("#content").html("");
						e.preventDefault();
						$.ajax({
									type : "POST",
									url : "updateUserInfo",
									data : {
										name : $('#recipient-name').val(),
										project : $('#recipient-project').val(),
										dob : $('#recipient-dob').val(),
										pass : $('#recipient-pass').val(),
										issue : $('#recipient-issue').val(),
										img : $('#recipient-img').val(),
										id : $('#recipient-id').val(),
										email : $('#recipient-email').val(),
										row : $('#recipient-row').val()
									},
									success : function(result) {
										//alert
										 $('<div class="alert alert-success ppalert" id="success-alert">Update! Successful</div>').appendTo('#result').trigger('showalert')
							                .fadeOut(5000);
										var list = JSON.parse(result[0]);
										var html = '';
										for (var i = 0; i < list.length; i++) {
											var counter = list[i];
											if (counter.id == $('#recipient-id').val()) {
												html = '<tr id="updated" style="background: #c5d9ea">';
											} else {
												html = '<tr>';
											}
											html += '<td>' + counter.id+ '</td>';
											html += '<td>' + counter.name+ '</td>';
											html += '<td>' + counter.project+ '</td>';
											html += '<td>' + counter.dob+ '</td>';
											html += '<td>' + counter.email+ '</td>';
											html += '<td>' + counter.passport+ '</td>';
											html += '<td>' + counter.isueDate+ '</td>';
											html += '<td>' + counter.expDate+ '</td>';
											html+='<td><a data-toggle="modal" data-target="#modalimg" title="Show image" style ="color:green" onclick = "showIMG('+counter.id+')">'+counter.path+'</a></td>';
											html += '<td><a id="'+ i+ '" onclick="loadInfoEdit('+counter.id+ ')"  data-toggle="modal" data-target="#exampleModal" title="Chỉnh sửa" style="margin-right: 3%"><span class="glyphicon glyphicon-pencil"></span> </a> <a id="'+i+ '" data-toggle="modal" data-target="#modaldelete" onclick="loadInfoDelete('+ counter.id+ ')"  title="Xoa" style="margin-right: 3%"><span class="glyphicon glyphicon-remove"></span> </a></td>';
											html += '</tr>';
											$("#content").append(html);
										}

									},
									error : function(result) {
										alert('errorff' + dataform);
									}
								});
					});

</script>
<script type="text/javascript">
	$("#card").hide();
	var loadFile = function(event) {
		var reader = new FileReader();
		reader.onload = function() {
			var output = document.getElementById('output');
			output.src = reader.result;
		};
		reader.readAsDataURL(event.target.files[0]);
		$("#card").show();
	};
</script>

<script>
	$(function() {
		$("#datepicker").datepicker();
		$("#recipient-dob").datepicker();
		$("#recipient-issue").datepicker();
	});
</script>


<script>
	$(function() {
		$("#datepicker1").datepicker();
	});
</script>
<script type="text/javascript">
//covert issue date to expdate
function iToE(obj, row){
	var issue = $(obj).val();
	$.ajax({
		url : 'iToE',
		type : 'POST',
		data : {
			issue : issue
		},
		success : function(data) {
			if(data)
			{
				$("#datatable tr:eq("+row+") td:eq(8)").html("");//reemove text in td
				$("#datatable tr:eq("+row+") td:eq(9)").html("");
				$("#datatable tr:eq("+row+") td:eq(8)").html(data);		
				 
			    //tru remaining day
			       var remaining=  $("#datatable tr:eq("+row+") td:eq(8)").html();
				    var now = new Date();
					 var date1 =remaining.split('/'); 		  
					 var exDate = new Date(date1[2],date1[1]-1,date1[0]);	
					 var difftime = exDate.getTime() - now.getTime();
					 var diffDays = Math.ceil(difftime / (1000 * 3600 * 24)); 
					  	if(diffDays >=0){
						   var diffDayss=diffDays; 
					   }else{
						   var diffDayss="expiry date"
					   }
					  	
				if(diffDays <0){
					$("#datatable tr:eq("+row+") td:eq(9)").html(diffDayss);	
					$("#datatable tr:eq("+row+") td:eq(9)").css('color','red');
				}
				else if(diffDays >=0){
					$("#datatable tr:eq("+row+") td:eq(9)").html(diffDayss);	
					$("#datatable tr:eq("+row+") td:eq(9)").css('color','black');
				}		  
			}
		}
	});
}

</script>
</html>