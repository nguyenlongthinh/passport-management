package com.dao;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.configuration.LoginDetail;
import com.controller.CsvWriterController;
import com.model.User;

public class LoginDao {
	String fileAdmin =  System.getProperty("user.home") + "/passport/data/admin.csv";
	String line = "";
	String cvsSplitBy = ",";

	
	@Autowired
    ServletContext context;
	public List<LoginDetail> getAllAdminLogin() {
		List<LoginDetail> listall = new ArrayList<>();
		try (BufferedReader br = new BufferedReader(new FileReader(fileAdmin))) {
			while ((line = br.readLine ()) != null) {
				// use comma as separator
				String[] country = line.split(cvsSplitBy);
				listall.add(new LoginDetail(country[0], country[1]));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return listall;
	}
	
	// update info user
			public String updatePass(String username, String password) {
				int kt = 0;
				
				List<LoginDetail> listall = new ArrayList<LoginDetail>();

				try (BufferedReader br = new BufferedReader(new FileReader(fileAdmin))) {
					while ((line = br.readLine()) != null) {
						// use comma as separator:
						String[] country = line.split(cvsSplitBy);
						listall.add(new LoginDetail(country[0], country[1]));
					}

				} catch (Exception e) {
					System.out.println("loi o day"+e.getMessage());
				}
				// su dung vong lap chay qua kiem tra:
				for (int i = 0; i < listall.size(); i++) {
					if (listall.get(i).getUsername().equals(username) ) {
						kt= i;
					}
				}
				// xoa user tai vi tri:
				listall.remove(kt);
				LoginDetail user = new LoginDetail(username,password);
				listall.add(kt, user);
				File file = new File(fileAdmin);
				// tien hanh xoa file csv:
				file.delete();
				// Them lai vao co so du lieu:
				for (LoginDetail user2 : listall) {
					CsvWriterController.writeCsvFileAdmin(fileAdmin, user2.getUsername(), user2.getPassword());

				}

				return null;
			}
	
}
	
