package com.model;

public class Snooze {
	public String time;
	public String list;
	
	public Snooze() {
	}

	public Snooze(String time, String list) {
		this.time = time;
		this.list = list;
	}
	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getList() {
		return list;
	}

	public void setList(String list) {
		this.list = list;
	}
	
	

}
