package com.controller;

import java.util.List;
import java.util.Random;
import java.util.TimerTask;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.DatatypeConverter;

import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;

import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.apache.pdfbox.tools.imageio.ImageIOUtil;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import org.apache.xmlbeans.impl.store.Path;
import org.omg.CORBA.portable.InputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.ResourceUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.controller.CsvWriterController;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.model.Snooze;
import com.model.User;


import com.dao.*;

@Controller
@RequestMapping(value = "/")
public class UserController {
	String fileName = System.getProperty("user.home") + "/passport/data/user.csv";
	String fileNameTimer = System.getProperty("user.home") + "/passport/data/timer.csv";
	String fileIMG = System.getProperty("user.home") + "/passport/img";
	@Autowired
	ServletContext context;

	//Roll back data
	@ResponseBody
	@RequestMapping(value = "/rollBackData", method = RequestMethod.GET)
	public String rollBackData(@RequestParam(value="id") String id)
	{
		UserDao udao = new UserDao(); 
		String userinfo = udao.getUser(id);
		return userinfo;
	}
	
	
	//Update data (info user + info passport)
			@ResponseBody
			@RequestMapping(value = "/updateUserInfo", method = RequestMethod.GET)
			public String updateUserInfo(HttpServletRequest request) throws JsonProcessingException {
				
				String expireDate ;
				
				String namerq = request.getParameter("name");
				String emailrq = request.getParameter("email");
				String recipientrq = request.getParameter("emailpm");
				
				String projectrq = request.getParameter("project");
				String dobrq = request.getParameter("dob");
				String passrq = request.getParameter("passport");
				String issuerq = request.getParameter("issuedate");
				String expirerq = request.getParameter("expire");
				String snoozerq = request.getParameter("snooze");
				String idrq = request.getParameter("id");
				String remainingrq = request.getParameter("remaining");
				
				String name = namerq.replaceAll("<br>", "");
				String email = emailrq.replaceAll("<br>", "");
				String recipent = recipientrq.replaceAll("<br>", "");
				String project = projectrq.replaceAll("<br>", "");
				String dob = dobrq.replaceAll("<br>", "");
				String pass = passrq.replaceAll("<br>", "");
				String issue = issuerq.replaceAll("<br>", "");
				String id = idrq.replaceAll("<br>", "");
				String expire = expirerq.replaceAll("<br>", "");
				
				String remaining = remainingrq.replaceAll("<br>", "");
				
				String snooze = snoozerq.replaceAll("<br>", "");
				Integer snoozee=Integer.parseInt(snooze);
				
				System.out.println(issue);
				UserDao udao = new UserDao();
				expireDate = udao.updateAndReturnExp(id, name, dob,email,recipent, project, pass, issue,expire,remaining,snoozee);

				return expireDate;
			}
			
			
    //Convert issue to exp
	@ResponseBody
	@RequestMapping(value = "/iToE", method = RequestMethod.GET)
	public String iToE(HttpServletRequest request) throws ParseException{
			
			String issue = request.getParameter("issue");
			
			UserDao udao = new UserDao();
			String exp = udao.addYear(issue);
			System.out.println(exp);
			return exp;
		}

    //Convert issue to exp
	@ResponseBody
	@RequestMapping(value = "/testexp", method = RequestMethod.GET)
	public String testexp(HttpServletRequest request) throws ParseException{
			
			String exp = request.getParameter("expire");

			System.out.println(exp);
			return exp;
		}

		
	//Go confirm Snooze User:
	@RequestMapping(value = "/goconfirm")
	public String goConfirm(ModelMap mmMap,HttpServletRequest request){
		String id = request.getParameter("id");
		String reson = request.getParameter("re");
		mmMap.put("id",id);
		if(reson!=""){
			if(reson.equals("expdate")){
				mmMap.put("reson","Sap het ngay het han");
			}
			if(reson.equals("isuedatenull")){
				mmMap.put("reson","Isuedate Null");
			}
			if(reson.equals("passportnull")){
			    mmMap.put("reson","Passport Rong");
			}
		}
		//get User id:
		UserDao userDao = new UserDao();
		User user = userDao.getUserById(id);
		if(user!=null){
		   mmMap.put("users",user);
		}
		SnoozeDao snDao = new SnoozeDao();
		Snooze snooze = snDao.getSnooze();
		//Sent Snooze:
		mmMap.put("snooze",snooze.getTime());
		return "confirm";
	}
	//Update Snooze:
	@RequestMapping(value = "/updateSnooze",method = RequestMethod.GET)
	@ResponseBody
	public String updateSnooze(@RequestParam (value = "i") String id,@RequestParam(value="snooze") String snooze){
		//get all users:
		int kt =0;
		List<User> list = new ArrayList<>();
		UserDao userDao = new UserDao();
	    list = userDao.getAllUser();
	    for(int i = 0;i<list.size();i++){
	    	if(list.get(i).getId().equals(id)){
	    		kt = i;
	    	}
	    }
	    //recive user:
	    User userre = list.get(kt);
	    //remove user:
	    list.remove(kt);
	    User usernew = new User(userre.getId(),userre.getName(),userre.getDob(),userre.getEmail(),userre.getEmailpm(),userre.getProject(),userre.getPassport(),userre.getIsueDate(),userre.getExpDate(),Integer.parseInt(snooze),userre.getPath());
	    list.add(kt,usernew);
	    File file = new File(fileName);
		    if(file.exists()){
		    	file.delete();
		    }
	    //Write CSV:
	    for (User user2:list){
	    	CsvWriterController.writeCsvFile(fileName, user2.getId(), user2.getName(), user2.getDob(), user2.getEmail(),user2.getEmailpm(),
		    user2.getProject(), user2.getPassport(), user2.getIsueDate(), user2.getExpDate(),user2.getRemaining(),user2.getSnooze(),user2.getPath() );
	    }
	    return "Updated Snooze!";
	    
	}
	//Set value Snooze:
    @RequestMapping(value = "/setSnooze")
    @ResponseBody
    public String setSnooze(@RequestParam(value ="snooze") String snooze){
    	SnoozeDao snDao = new SnoozeDao();
    	Snooze snooze2 = new Snooze(snooze,snDao.getSnooze().getList());
    	snDao.createSnooze(snooze2);
    	return snooze;
    }
	
	
	//EXPORT EXCEL
	//Export all rows in table
	@ResponseBody
	@RequestMapping(value = "/exportFileAll", method = RequestMethod.GET)
	public String exportFileAll () throws IOException{

		UserDao udao = new UserDao();
		udao.exportToExcelAll(context.getRealPath("/resources/excel"));//case export all
		
		return "success";
	}
	//Export for rows in table
	@ResponseBody
	@RequestMapping(value = "/exportfile", method = RequestMethod.POST)
	public String Export (@RequestParam(value = "datas[]") List<String> datas, @RequestParam(value = "token") int token) throws IOException{

		UserDao udao = new UserDao();
		udao.exportToExcel(context.getRealPath("/resources/excel"), datas, token);//case export for table
		
		return "success";
	}
	
	
	//Edit expire date
	@ResponseBody
	@RequestMapping(value = "/editExpTime", method = RequestMethod.GET)
	public String editExpTime (@RequestParam(value = "time") String time) {
		
		ExpTimeDao  edao = new ExpTimeDao();
		String updTime;
		if(edao.checkTimer(fileNameTimer)){
			updTime = edao.updateExpTime(time, fileNameTimer);
		} else {
			updTime = edao.createExpTime(time);
		}//updTime =  "success" or "faile"
		
		return updTime;
	}
	//delete User
	@RequestMapping(value = "/deleteUser", method = RequestMethod.GET)
	@ResponseBody
	public String deleteUser(@RequestParam(value = "id") String id) throws JsonProcessingException {
		String csvFile = System.getProperty("user.home") + "/passport/data/user.csv";
		UserDao userDao = new UserDao();
		List<User> users = userDao.getAllUser();
		int kt = 0;
		for (int i = 0; i < users.size(); i++) {
			if (users.get(i).getId().equals(id)) {
				kt = i;
			}

		}
		// delete:
		users.remove(kt);
		File file = new File(csvFile);
		file.delete();
		
		for (User user2 : users) {
			
			if(user2.getProject().equals("")){
				user2.setProject("null");
			}
			if(user2.getPassport().equals("")){
				user2.setPassport("null");
			}	
			if(user2.getIsueDate().equals("")){
				user2.setIsueDate("null");
			}
			if( user2.getExpDate().equals("")){
				user2.setExpDate("null");
			}
			if( user2.getPath().equals("")){
				user2.setPath("null");
			}
			
			CsvWriterController.writeCsvFile(csvFile, user2.getId(), user2.getName(), user2.getDob(), user2.getEmail(),user2.getEmailpm(),
					user2.getProject(), user2.getPassport(), user2.getIsueDate(), user2.getExpDate(),user2.getRemaining(),user2.getSnooze(),user2.getPath());
		}
		String jsons = userDao.getJson(users);
		return jsons;
	}
	
	//load expire day loadExpDay
	@RequestMapping(value = "/loadExpDay", method = RequestMethod.GET)
	@ResponseBody
	public int loadExpDay() {
		
		// int expTime;
		
		//get expire day
		// ExpTimeDao edao = new ExpTimeDao();
		// if(edao.checkTimer("/app/store/timer.csv")){
		// 	expTime = edao.getTimer();
		// } else {
		// 	edao.createExpTime("60");//default 60 day
		// 	expTime = edao.getTimer();
		// }
		String line = "";
		int expTime = 0;
		try (BufferedReader br = new BufferedReader(new FileReader(fileNameTimer))) {
			while ((line = br.readLine()) != null) {
				expTime = Integer.parseInt(line);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return expTime;
	}
	
	// function pagination
	public static void pagination(HttpServletRequest request, ModelMap mm, List<User> listUser) {
		PagedListHolder<User> productList = null;
		int page = 1;
		if (request.getParameter("page") != null)
			page = Integer.parseInt(request.getParameter("page"));
		productList = new PagedListHolder<User>();
		productList.setSource(listUser);
		productList.setPageSize(5);
		productList.setPage(page);
		int noOfPages = productList.getPageCount();
		mm.put("lists", productList);
		mm.put("userList", productList);
		mm.put("noOfPages", noOfPages - 1);
		mm.put("currentPage", page);
	}

//	@RequestMapping(value = "/", method = RequestMethod.GET)
//	public String test(Model model) {
//		User user = new User();
//		
//
//		return "index";
//	}
	@RequestMapping(value = "/exam", method = RequestMethod.GET)
	public String ex(Model model) {
		User user = new User();
		model.addAttribute("userForm", user);

		return "exam";
	}
	//Load Snooze value:
	@RequestMapping(value = "/loadSnooze")
	@ResponseBody
	public String loadSnooze(){
		SnoozeDao snDao = new SnoozeDao();
		Snooze snooze = snDao.getSnooze();
		if(snooze!=null){
		  return snooze.getTime();
		}
		else{
			return null;
		}
		
	}
	// check load data
    @RequestMapping(value = "/loaddata")
    @ResponseBody
    public String Loaddata(@RequestParam(value="data") String data) throws JsonProcessingException {
        String jsons = null;
        String fileName = System.getProperty("user.home") + "/passport/data/snooze.csv";
        Random r = new Random();
        int randomInt = r.nextInt(1000) + 1;
        if(data.equals("index")){
            UserDao userDao = new UserDao();
            List<User> users =userDao.getAllUser();
            jsons = userDao.getJson(users);
            //Load Snooze day:
            SnoozeDao snoozeDao = new SnoozeDao();
            Snooze snooze = new Snooze();
            if(!snoozeDao.checkSnooze(fileName)){
               snooze = new Snooze("10",String.valueOf(randomInt));
               snoozeDao.createSnooze(snooze);
            } 
       }
       return jsons;
    }
 	
	
	@RequestMapping(value = "/test")
	@ResponseBody
	public String upfile(HttpServletRequest request, MultipartHttpServletRequest mRequest){
		MultipartFile mFile = mRequest.getFile("image");
		return request.getParameter("id")+mFile.getOriginalFilename();		
	}
	
	
	//Insert User:
		@RequestMapping(value = "/insert", method = RequestMethod.POST)
		@ResponseBody
		public String Add(HttpServletRequest request, ModelMap mm, MultipartHttpServletRequest mRequest) throws ParseException, IOException {
			MultipartFile mFile = mRequest.getFile("image");
			String id = request.getParameter("id");
			String name = request.getParameter("name");
			String dob = request.getParameter("dob");
			String email = request.getParameter("email");	
			String emailpm = request.getParameter("emailpm");	
			String project = request.getParameter("project");
			String passport = request.getParameter("passport");
			String isuedate = request.getParameter("isuedate");
			String image = "";
			String remaining="0";
			String replace=emailpm.replaceAll(",",";");
			//System.out.println(replace);
			
			
			if(mFile != null){
				try {
					 UserDao udao = new UserDao();
					 image = mFile.getOriginalFilename();
					 String ms = udao.uploadFileImg(mFile, id, fileIMG);
					 System.out.println(ms);
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
			else{
				 image = "No Image";
			}
			String expdate = "";
			
			if(project.equals("")){
			    project="";
			}
			if(emailpm.equals("")){
			    emailpm="";
			}
			
			if(image.equals("")){
				image="No Image";
			}	
			if(passport.equals("")){
				passport="";
			}
			if(isuedate.equals("")){
				isuedate="";
				expdate="";
				remaining="N/A";
				
			}
			
			if(!isuedate.equals("")){
				try {

					DateFormat parser = new SimpleDateFormat("dd/MM/yyyy");
					Date date = (Date) parser.parse(isuedate);
					DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
					System.out.println(formatter.format(date));
					Calendar c = Calendar.getInstance();
					c.setTime(date);
					c.add(Calendar.YEAR, 10);
					Date newDate = c.getTime();
					expdate= formatter.format(newDate);
					Date date1 = new Date();
				    boolean before = date1.before(newDate);
				    if(before == true){
				    	remaining="Expired";
				    }
				    else {
				        remaining = String.valueOf((int)( (date.getTime() - newDate.getTime()) / (1000 * 60 * 60 * 24)));
				    }
					
				} catch (ParseException e) { 
					e.printStackTrace();
				}
			}
			String jsons = null;
			String line = "";
			String cvsSplitBy = ",";
			try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
				  br.readLine();
				while ((line = br.readLine()) != null) {
					// use comma as separator
					String[] user = line.split(cvsSplitBy);
					if (user[0].equals(id)) {
						return jsons="exists";
					}
				}

			} catch (Exception e) {
			}
			
			


	        CsvWriterController.writeCsvFile(fileName,id,name,dob ,email,replace,project,passport,isuedate,expdate,remaining,0,image);
	       
	        List<User> listall = new ArrayList<>();
	        listall.add(new User(id,name,dob ,email,replace,project,passport,isuedate,expdate,0,image));

	        //Wirte json list:
	        UserDao udao = new UserDao();
	        jsons = udao.getJson(listall);
	   
	        return jsons;
	    }
		
		
	//Load data List:
    @RequestMapping(value = "/load", method = RequestMethod.GET)
    @ResponseBody
	public String loadData(@RequestParam(value = "ms") String ms) throws JsonProcessingException{
    	  UserDao userDao = new UserDao();
          User user = userDao.getUserById(ms);
          ObjectMapper mapper = new ObjectMapper();
          String jsons = mapper.writeValueAsString(user);
          return jsons;

	}
    //Show Image
    @RequestMapping(value = "/show")
    @ResponseBody
    public String showImg(@RequestParam(value = "id") String id, HttpServletResponse response) throws IOException{
    	UserDao userDao = new UserDao();
    	User user = userDao.getUserById(id);
    	ObjectMapper mapper = new ObjectMapper();
    	String jsons = mapper.writeValueAsString(user);    	
    	System.out.println("ok here");
    	return jsons;
    }
    
    @RequestMapping(value = "/showIMG")
    @ResponseBody
    public String showImage(@RequestParam(value = "path") String path) throws IOException{
    	String b64 ="";
		String path_img = System.getProperty("user.home") + "/passport/img/"+path;
    	try{
    			if(path.substring(path.length()-3,path.length()).toLowerCase().equals("pdf") ){
    				File f = new File(System.getProperty("user.home") + "/passport/img/"+path.substring(0,path.length()-4)+".jpg");
    				if(!f.exists()){
    	    	      PDDocument document = PDDocument.load(new File(path_img));
    	    	      PDFRenderer pdfRenderer = new PDFRenderer(document);
    	    	      for (int page = 0; page < document.getNumberOfPages(); ++page) {
    	    	          BufferedImage bim = pdfRenderer.renderImageWithDPI(
    	    	            page, 300, ImageType.RGB);
    	    	          ImageIOUtil.writeImage(
    	    	            bim, String.format(System.getProperty("user.home") + "/passport/img/"+path.substring(0,path.length()-4)+".jpg", page + 1, "jpg"), 300);
    	    	      }
    	    	      document.close();
    				}
//    	    	      File file = new File(path_img);
//    	    			// tien hanh xoa file csv:
//    	    			file.delete();
    	    	       path_img = System.getProperty("user.home") + "/passport/img/"+path.substring(0,path.length()-4)+".jpg";
    				    				
    			}
    	        BufferedImage bImage = ImageIO.read(new File(path_img));//give the path of an image
    	        ByteArrayOutputStream baos = new ByteArrayOutputStream();
    	        ImageIO.write( bImage, "jpg", baos );
    	        baos.flush();
    	        byte[] imageInByteArray = baos.toByteArray();
    	        baos.close();                                   
    	        b64 = DatatypeConverter.printBase64Binary(imageInByteArray);
    	    }catch(IOException e){
    	      System.out.println("Error: "+e);
    	    }   	

    	return b64;
    }
    //Reload and upload img:

    @RequestMapping (value = "/reloadimg",method = RequestMethod.POST)

    @ResponseBody

    public String reloadImg(MultipartHttpServletRequest request) throws IOException{
        System.out.println("Work there 1");

    	MultipartFile file = request.getFile("image");

        UserDao udao = new UserDao();
        System.out.println("Work there 2");

        String ms = udao.uploadFileImg(file,"",context.getRealPath("/resources/img"));       
        //Upload file:
        System.out.println("ms: "+ms);
        System.out.println("reloadimg: "+request.getContextPath());
        return request.getContextPath();

    }
    
    
//    Import CSV:
    @RequestMapping(value = "/uploadFile",method=RequestMethod.POST)
    @ResponseBody
    public String importCSV(MultipartHttpServletRequest request) throws IOException{
    	MultipartFile mFile = request.getFile("file1");
    	
    	String filename = mFile.getOriginalFilename();
    	UserDao userDao  = new UserDao();
    	userDao.uploadFileCSV(mFile,filename);
    	return mFile.getOriginalFilename();
    }
    
  //update img:

    @RequestMapping(value = "/updateIMG", method = RequestMethod.POST)
    @ResponseBody
    public String updateIMG(MultipartHttpServletRequest request) throws IOException{

        request.setCharacterEncoding("UTF-8");
        System.out.println(request.getCharacterEncoding());
        MultipartFile file = request.getFile("image");
        String filename = "";
        String id = request.getParameter("id");
        String csvFile = System.getProperty("user.home") + "/passport/data/user.csv";
        
        if(file == null){
            filename = "No image";
        }
        else {
             //upload image:
             UserDao udao = new UserDao();
             filename = new String(file.getOriginalFilename().getBytes ("UTF-8"));
             filename = id+filename.substring(filename.length() - 4, filename.length());
             String ms = udao.uploadFileImg(file, id, fileIMG);
             System.out.println(ms);
        }

        //update IMG:
        UserDao userDao = new UserDao();

        //File user origin:
        User user = userDao.getUserById(id);
        ObjectMapper mapper = new ObjectMapper();

        //Get list user:
        List<User> listall = userDao.getAllUser();
        int kt = 0;
        for(int i= 0;i<listall.size();i++){
            if(listall.get(i).getId().equals(user.getId())){
               kt=i;
            }
        }

        listall.remove(kt);

        //Create User new:
        User usernew = new User(id, user.getName(),user.getDob(), user.getEmail(),user.getEmailpm(),user.getProject(), user.getPassport(), user.getIsueDate(), user.getExpDate(),user.getRemaining(),user.getSnooze(),filename);
        listall.add(kt,usernew);
        File file2 = new File(csvFile);
        file2.delete();

        //fecth:
           for (User user2 : listall) {           
                if(user2.getProject().equals("")){
                    user2.setProject("null");
                }

                if(user2.getPassport().equals("")){
                    user2.setPassport("null");
                }   

                if(user2.getIsueDate().equals("")){
                    user2.setIsueDate("null");
                }

                if( user2.getExpDate().equals("")){
                    user2.setExpDate("null");
                }

                CsvWriterController.writeCsvFile(csvFile, user2.getId(), user2.getName(), user2.getDob(), user2.getEmail(),user2.getEmailpm(),
                        user2.getProject(), user2.getPassport(), user2.getIsueDate(), user2.getExpDate(),user2.getRemaining(), user2.getSnooze(),user2.getPath());

            }      

        String jsons = mapper.writeValueAsString(usernew);
        return jsons;

    }

	//check csv valid
		@RequestMapping(value = "/checkcsv", method = RequestMethod.GET)
		@ResponseBody
		public String CheckCSV(@RequestParam(value = "csv", required = true) String csv) {
			String cut = csv.split("\\.")[1];
			
			if(!cut.equals("csv")){
				return "not";
			}else{
				return "ok";
			}
		}
	@RequestMapping(value = "/femail", method = RequestMethod.GET)
	@ResponseBody
	public String FilterEmail(HttpServletRequest request, ModelMap mm) throws ParseException {
		String fil = request.getParameter("choosemail");
		List<User> listall = new ArrayList<User>();
		String flag = "noexist";
		String line = "";
		String cvsSplitBy = ",";
		try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
			while ((line = br.readLine()) != null) {
				// use comma as separator
				String[] user = line.split(cvsSplitBy);
				if (user[0].equals(fil)) {
					flag = "exist";
				}
			}
			// mm.put("lists", listall);
			return flag;

		} catch (Exception e) {
		}

		return flag;
	}
}