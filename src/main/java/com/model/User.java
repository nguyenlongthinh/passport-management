package com.model;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

//import java.util.Date;

public class User {
	private String id;
	private String name;
	private String dob;
	private String email;
	private String emailpm;
	private String project;
	private String passport;
	private String isuedate;
	private String expdate;
	private String remaining;
	private int snooze;
	private String path;


	public User()
	{
		
	}
	
	public User(String id, String name)
	{
		this.id = id;
		this.name = name;
	}
	
	
	
	public User(String id, String name, String dob, String email, String emailpm, String project, String passport,
			String isuedate, String expdate, String remaining, int snooze, String path) {
		this.id = id;
		this.name = name;
		this.dob = dob;
		this.email = email;
		this.emailpm = emailpm;
		this.project = project;
		this.passport = passport;
		this.isuedate = isuedate;
		this.expdate = expdate;
		this.remaining = remaining;
		this.snooze = snooze;
		this.path = path;
	}

	public User(String id, String name,String dob,String email, String emailpm, String project,String passport, String isueDate,
			String expDate,int snooze, String path) {

		this.id = id;
		this.name = name;
		this.dob = dob;
		this.email = email;
		this.emailpm=emailpm;
		this.project = project;
		this.passport = passport;
		this.isuedate = isueDate;
		this.expdate = expDate;
		this.snooze = snooze;
		this.path = path;	
		
	}
    
	public User(String id, String name,String dob ,String email,String project, String passport, String isuedate,String expdate, String path, int snooze) {
		this.id = id;
		this.name = name;
		this.project = project;
		this.dob = dob;
		this.email = email;
		this.passport = passport;
		this.isuedate = isuedate;
		this.expdate = expdate;
		this.path = path;
		this.snooze = snooze;
	}

	public User(String id, String name, String project, String dob, String email) {
		super();
		this.id = id;
		this.name = name;
		this.project = project;
		this.dob = dob;
		this.email = email;
	}

	public User(String id, String name, String project, String dob, String email,String emailpm) {
		super();
		this.id = id;
		this.name = name;
		this.project = project;
		this.dob = dob;
		this.email = email;
		this.emailpm=emailpm;
	}


	
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProject() {
		return project;
	}

	public void setProject(String project) {
		this.project = project;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getEmailpm() {
		return emailpm;
	}

	public void setEmailpm(String emailpm) {
		this.emailpm = emailpm;
	}

	public String getPassport() {
		return passport;
	}

	public void setPassport(String passport) {
		this.passport = passport;
	}

	public String getIsueDate() {
		return isuedate;
	}

	public void setIsueDate(String isueDate) {
		this.isuedate = isueDate;
	}

	public String getExpDate() {
		return expdate;
	}

	public void setExpDate(String expDate) {
		this.expdate = expDate;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
	

	public String getRemaining() {
		return remaining;
	}

	public void setRemaining(String remaining) {
		this.remaining = remaining;
	}

	public int getSnooze() {
		return snooze;
	}

	public void setSnooze(int snooze) {
		this.snooze = snooze;
	}

	@Override
	public String toString() {
		return "User [email=" + email
				+ ", passport=" + passport + ", isueDate=" + isuedate + ", expdate=" + expdate + "]";
	}
	
	
	

}