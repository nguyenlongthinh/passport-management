package com.controller;

import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.controller.CsvWriterController;
import com.model.User;


@Controller
@RequestMapping(value = "/test")
public class AjaxController {
	String fileName =  System.getProperty("user.home") + "/passport/data/user.csv";
	@RequestMapping(value = "/list")
	public String show(ModelMap mm, HttpServletRequest request, HttpSession session) {
		// open file csv:
		List<User> listall = new ArrayList<User>();
		PagedListHolder<User> productList = null;
		String csvFile = fileName;
		String line = "";
		String cvsSplitBy = ",";
		String page = request.getParameter("page");
		List<User> listsession = (List<User>) session.getAttribute("lists");
		if (listsession == null) {
			try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
				while ((line = br.readLine()) != null) {
					// use comma as separator
					String[] country = line.split(cvsSplitBy);
					listall.add(new User(country[0], country[1], country[2], country[3], country[4]));
				}

			} catch (Exception e) {
			}

			productList = new PagedListHolder<User>();
			productList.setSource(listall);
			productList.setPageSize(5);
			mm.put("pages", productList.getPageCount());
			mm.put("val", 1);
			try {
				if (page.equals("")) {
					productList.setPage(0);
					mm.put("val", 1);
				} else {
					productList.setPage(Integer.parseInt(page) - 1);
					mm.put("val", Integer.parseInt(page));
				}
			} catch (Exception e) {
				e.getMessage();
				// TODO: handle exception
			}
			// productList.setPage(0);
			mm.put("lists", productList);
		} else {
			productList = new PagedListHolder<User>();
			productList.setSource(listsession);
			productList.setPageSize(5);
			mm.put("pages", productList.getPageCount());
			mm.put("val", 1);
			try {
				if (page.equals("")) {
					productList.setPage(0);
					mm.put("val", 1);

				} else {
					productList.setPage(Integer.parseInt(page) - 1);
					mm.put("val", Integer.parseInt(page));
				}
			} catch (Exception e) {
				e.getMessage();
				// TODO: handle exception
			}
			// productList.setPage(0);
			mm.put("lists",productList);
		}
		return "test";
	}

}
