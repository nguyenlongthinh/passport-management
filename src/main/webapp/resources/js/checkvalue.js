 //check page
    $(document).ready(function() {   
            $('#checkpage').change(function() { 
                if($('#checkpage').val() > totalpage){
                      alertBox('Invalid!','danger'); 
                      document.getElementById('checkpage').value=totalpage;
                      pager.showPage(totalpage);
                                    }
                else if($('#checkpage').val() < 1){
                      alertBox('Invalid!','danger'); 
                      document.getElementById('checkpage').value=1;
                      pager.showPage(1);
                                    } 
                else {
                	pager.showPage($('#checkpage').val());
                	 var val = this.value.replace(/[^\d]/g, "");
                     if(val != this.value)
                         this.value = val;
                     if($('#checkpage').val() < 1){
                     	document.getElementById('checkpage').value = 1;
                     }
                     if($('#checkpage').val() >= totalpage){
                     	document.getElementById('checkpage').value = totalpage;
                     }
                     if($('#checkpage').val()%3 == 0){
                     	document.getElementById("listPage1").getElementsByTagName("a")[0].innerHTML =  parseInt($('#checkpage').val()) - 2;
                     	document.getElementById("listPage2").getElementsByTagName("a")[0].innerHTML = parseInt($('#checkpage').val()) - 1;
                     	document.getElementById("listPage3").getElementsByTagName("a")[0].innerHTML = parseInt($('#checkpage').val());
                     	
                     	document.getElementById("listPage1").setAttribute("class","page-item");	
                     	document.getElementById("listPage2").setAttribute("class","page-item");
                     	document.getElementById("listPage3").setAttribute("class","page-item active");	
                     }
                     else if($('#checkpage').val()%3 == 2){
                     	document.getElementById("listPage1").getElementsByTagName("a")[0].innerHTML = parseInt($('#checkpage').val()) - 1;
                     	document.getElementById("listPage2").getElementsByTagName("a")[0].innerHTML = parseInt($('#checkpage').val());
                     	document.getElementById("listPage3").getElementsByTagName("a")[0].innerHTML = parseInt($('#checkpage').val()) + 1;
                     	
                     	document.getElementById("listPage1").setAttribute("class","page-item");	
                     	document.getElementById("listPage3").setAttribute("class","page-item");	
                     	document.getElementById("listPage2").setAttribute("class","page-item active");	
                     }
                     else if($('#checkpage').val()%3 == 1){
                     	document.getElementById("listPage1").getElementsByTagName("a")[0].innerHTML = parseInt($('#checkpage').val());
                     	document.getElementById("listPage2").getElementsByTagName("a")[0].innerHTML = parseInt($('#checkpage').val()) + 1;
                     	document.getElementById("listPage3").getElementsByTagName("a")[0].innerHTML = parseInt($('#checkpage').val()) + 2;
                     	
                     	document.getElementById("listPage2").setAttribute("class","page-item");
                     	document.getElementById("listPage3").setAttribute("class","page-item");	
                     	document.getElementById("listPage1").setAttribute("class","page-item active");      	
                     }
                }
            });
    });
    
    //CHECK ID
function checkId() {
	document.getElementById("errorid").innerHTML = '';       	
	 var str = document.getElementById("id").value;
	 if(str != ''){
		 var re = /^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*$/;
         var res = re.test(str);
         if(res == false){
//        	 $("#id").val('');
        	 $('#errorid').html("<br/><span style='color:red'>Invalid ID</span>");
         }
         else {
         	var input, filter, table, tr, td, i, data;
            input = document.getElementById("id");
            filter = input.value.toUpperCase();
            table = document.getElementById("datatable");
            tr = table.getElementsByTagName("tr");
            for (i = 1; i < tr.length; i++) {
              td = tr[i].getElementsByTagName("td")[0];   
                if ((td.innerHTML.toUpperCase() == filter)) {
                    $('#errorid').html("<span style='color:red'>ID exist! Please enter new ID!!!</span>");
//                    $("#id").val('');
 	               break;

                }
            }
         }
	 }             
}
$(document).ready(function() {
        $('#id').change(function() {
        	checkId();
        });
    });

// CHECK EMAIL
function checkEmail() {
	document.getElementById("erroremail").innerHTML = '';   
	var str = document.getElementById("testemail").value;
	if( str != ''){
		var re = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/;
	    var res = re.test(str);
	    if(res == false){
//	    	$("#testemail").val('');
	    	$('#erroremail').html("<br/><span style='color:red'>Invalid Email</span>");
	    }
	    else {
	    	var input, filter, table, tr, td, i, data;
	       input = document.getElementById("testemail");
	       filter = input.value.toUpperCase();
	       table = document.getElementById("datatable");
	       tr = table.getElementsByTagName("tr");
	       for (i = 1; i < tr.length; i++) {
	         td = tr[i].getElementsByTagName("td")[3];                 
	           if ((td.innerHTML.toUpperCase() == filter)) {
	               $('#erroremail').html("<span style='color:red'>Email exist! Please enter new Email!!!</span>");
//	               $("#testemail").val('');
	               break;
	           }
	           else document.getElementById("erroremail").innerHTML = '';   
	         }
	    }        
	}        
}
 $(document).ready(function() { 
	 $('testemail').change(function() {
		 checkEmail();
	 });
	 
 });
 
//CHECK EMAILPM
 function checkEmailPm() {
// 	document.getElementById("erroremailpm").innerHTML = $('.bootstrap-tagsinput').text();
 	
 }
 
 $(document).ready(function() { 
	 $('#testemailpm').on('beforeItemAdd', function(event) {
		  var str = event.item;
		 	if( str != ''){
		 		var re = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/;
		 	    var res = re.test(str);
		 	    if(res == false){
		 	    	$('#erroremailpm').html("<br/><span style='color:red'>Invalid Email</span>");
		 	    	event.cancel = true;
		 	    }  
		 	    else $('#erroremailpm').html("");

		 	}        
		});
	 $('#editEmailPm').on('beforeItemAdd', function(event) {
		  var str = event.item;
		 	if( str != ''){
		 		var re = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/;
		 	    var res = re.test(str);
		 	    if(res == false){
		 	    	$('#erroreditEmailPm').html("<br/><span style='color:red'>Invalid Email</span>");
		 	    	event.cancel = true;
		 	    }  
		 	    else $('#erroreditEmailPm').html("");

		 	}        
		});
 })
 
 
//CHECK DATE OF BIRTH
 function checkDOB() {
	 document.getElementById("errordatepicker1").innerHTML = '';
	 var str = document.getElementById("datepicker1").value;
	 if(str != ''){
		 var re = /^((0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2})|((19|20)\d{2})$/ ; 
		 var res = re.test(str);
	     if(res == false){
//	    	 	$("input[name=dob]").val('');
		     	$('#errordatepicker1').html("<br/><span style='color:red'>Invalid Date of Birth</span>");
	     }
	     else {
	    	 var YOB = parseInt(str.substring(str.length-4, str.length));
		     var currentYear = parseInt((new Date()).getFullYear());
		     var count = currentYear - YOB;
		     if(!(count >  0)){
			     $('#errordatepicker1').html("<br/><span style='color:red'>Invalid Date of Birth</span>");
		     }
	     }
	     
	 }	 
 }
$(document).ready(function() {
    $('#datepicker1').change(function() {
    	checkDOB();
    });
});

//CHECK ISSUES DATE
function checkIssue() {
	document.getElementById("errordatepicker").innerHTML = '';
	 var str = document.getElementById("datepicker").value;
	 if(str != ''){
		 var re = /^((0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2})$/ ; 
		 var res = re.test(str);
	   if(res == false){
//		   $("datepicker").val('');
   	   	   $('#errordatepicker1').html("<br/><span style='color:red'>Invalid Date </span>");
	   }
	 }	 
}
$(document).ready(function() {
    $('#datepicker').change(function checkIssue() {
    	checkIssue();
    });
});
//CHECK PASSPORT
function checkPassport() {
	document.getElementById("errorpassport").innerHTML = '';   
	 var str = document.getElementById("passport").value;
	 if(str != ''){
		var re = /^[A-Z][0-9]{7}$/;
	    var res = re.test(str);
	    if(res == false){
//	    	$("#passport").val('');
		       $('#errorpassport').html("<br/><span style='color:red'>Invalid Passport</span>");
	    }
	    else {
	       var input, filter, table, tr, td, i, data;
	       input = document.getElementById("passport");
	       filter = input.value.toUpperCase();
	       table = document.getElementById("datatable");
	       tr = table.getElementsByTagName("tr");
	       for (i = 1; i < tr.length; i++) {
	         td = tr[i].getElementsByTagName("td")[6];              
	           if ((td.innerHTML.toUpperCase() == filter)) {
	               $('#errorpassport').html("<span style='color:red'>Passport exist! Please enter new Passport!!!</span>");
//	               $("#passport").val('');
		           break;
	           }
	           else document.getElementById("errorpassport").innerHTML = '';               
	       }
	    }         
	 }      
}
$(document).ready(function() {
    $('#passport').change(function() {
    	checkPassport();
        });
    });

//CHECK img
$(document).ready(function() {//    
$('#pic').change(function() {
	 var str = document.getElementById("pic").innerHTML;
	 if(str != ''){
		 var formatStr = str.substring(str.length-3, str.length).toUpperCase();
	     if ((formatStr != "PNG") && (formatStr != "PDF") && (formatStr != "JPG")) {
	            alertBox('Image Invalid!','danger'); 
	            $("#pic").val('');
	            }
	     else $('#errorimg').html("");
	 	}	 
    });
});
$(document).ready(function() {//    
	$('#image').change(function() {
		 var str = $('#image').val();
		 if(str != ''){
			 var formatStr = str.substring(str.length-3, str.length).toUpperCase();
		        if ((formatStr != "PNG") && (formatStr != "PDF") && (formatStr != "JPG")) {
		            alertBox('Image Invalid!','danger');     
		            $("#image").val('');
		            }
		        else $('#errorimg').html("");
		 }		 
    });
});

//File Update
function checkLoadImg() {
    var i = $("#i").val();
    var str = document.getElementById("im").files[0].name;
	if(str != ''){
		var formatStr = str.substring(str.length-3, str.length).toUpperCase();
		 if ((formatStr != "PNG") && (formatStr != "PDF") && (formatStr != "JPG")) {
	    	   alertBox('File is not image!','danger'); 
	           $("#im").val('');
	           $("#upimg").html('Change Your Image!!');
	           }
	       else{
	    	   $("#upimg").html('Change Your Image!!');
	           $("#title").text($("#im").val());
	           getLoadImg(i,str);
       }
	}
}


//CHECK EDIT EMAIL
function checkEditEmail() {
	document.getElementById("erroreditEmail").innerHTML = '';   
	 var str = document.getElementById("editEmail").value;
	 if(str != ''){
		 var re = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/;
		 var res = re.test(str);
		 if(res == false)
		 	$('#erroreditEmail').html("<br/><span style='color:red'>Invalid Email</span>");
		 else {
		 	var input, filter, table, tr, td, i, data;
		    input = document.getElementById("editEmail");
		    filter = input.value.toUpperCase();
		    table = document.getElementById("datatable");
		    tr = table.getElementsByTagName("tr");
		    for (i = 1; i < tr.length; i++) {
		      td = tr[i].getElementsByTagName("td")[3];                 
		        if ((td.innerHTML.toUpperCase() == filter)) {
		            $('#erroreditEmail').html("<span style='color:red'>Email exist! Please enter new Email!!!</span>");
		               break;
		        }
		        else document.getElementById("erroreditEmail").innerHTML = '';   
		      }
		 }          		 
	 }  
}
$(document).ready(function() { 
	 $('#editEmail').change(function() {
		 checkEditEmail();
	 });
});
//CHECK DATE OF BIRTH
function checkeditDOB() {
	 document.getElementById("erroreditDOB").innerHTML = '';
	 var str = document.getElementById("editDOB").value;
	 if(str != ''){
		 var re = /^((0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2})|((19|20)\d{2})$/ ; 
		 var res = re.test(str);
	 if(res == false)
	  	$('#erroreditDOB').html("<br/><span style='color:red'>Invalid Date of Birth</span>");
	 }	 
	 else {
    	 var YOB = parseInt(str.substring(str.length-4, str.length));
	     var currentYear = parseInt((new Date()).getFullYear());
	     var count = currentYear - YOB;
	     if(!(count >  0)){
		     $('#erroreditDOB').html("<br/><span style='color:red'>Invalid Date of Birth</span>");
	     }
     }
}
$(document).ready(function() {
 $('#editDOB').change(function() {
	 checkeditDOB();
 });
});

//CHECK ISSUES DATE
function checkEditIssue() {
	document.getElementById("erroreditIssue").innerHTML = '';
	 var str = document.getElementById("editIssue").value;
	 if(str != ''){
		 var re = /^((0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2})$/ ; 
		 var res = re.test(str);
	if(res == false)
		$('#erroreditIssue').html("<br/><span style='color:red'>Invalid Date </span>");
	 }	
}
$(document).ready(function() {
 $('#editIssue').change(function checkIssue() {
	 checkEditIssue();
	 var a = document.getElementById("editIssue").value;
		if(a == ''){
			document.getElementById("editExpire").innerHTML = '';    
		}
		else {
		  var remaining=  $("#editIssue").val();   
       var date1 =remaining.split('/');         
       var exDate = new Date((parseInt(date1[2])+10).toString(),date1[0]-1,date1[1]);   
	    	$("#editExpire").val(exDate.toLocaleDateString());		
	    	}		
		
		var expDate = document.getElementById("editExpire").value;
		if(expDate == ''){
			document.getElementById("editRemaining").innerHTML = 'N/A';    
		}
		else {
			var nowDate = new Date();
         var date2 =expDate.split('/');         
         var exDate2 = new Date(date2[2],date2[0]-1,date2[1]);   
         var difftime1 = exDate2.getTime() - nowDate.getTime();
         var diffDays1 = Math.ceil(difftime1 / (1000 * 3600 * 24)); 
         if(diffDays1 >= 0){
              var diffDayss1 = diffDays1; 
              $("#editRemaining").val(diffDayss1);
           }else{
               var diffDayss1 = "Expired";
               $("#editRemaining").val(diffDayss1);
           }         
	       }
 });
});
//CHECK PASSPORT
function checkEditPassport() {
	document.getElementById("erroreditPassport").innerHTML = '';   
	 var str = document.getElementById("editPassport").value;
	 if(str != ''){
		 var re = /^[A-Z][0-9]{7}$/;
		 var res = re.test(str);
		 if(res == false)
		 	$('#erroreditPassport').html("<br/><span style='color:red'>Invalid Passport</span>");
		 else {
		 	var input, filter, table, tr, td, i, data;
		    input = document.getElementById("editPassport");
		    filter = input.value.toUpperCase();
		    table = document.getElementById("datatable");
		    tr = table.getElementsByTagName("tr");
		    for (i = 1; i < tr.length; i++) {
		      td = tr[i].getElementsByTagName("td")[6];              
		        if ((td.innerHTML.toUpperCase() == filter)) {
		            $('#erroreditPassport').html("<span style='color:red'>Passport exist! Please enter new Passport!!!</span>");
		        }
		        else document.getElementById("erroreditPassport").innerHTML = '';               
		    }
		 }           
	 }	 
}
$(document).ready(function() {
 $('#editPassport').change(function() {
	 checkEditPassport();
     });
 });


//CHECK EDIT EXPIRE CHANGE
function checkEditExpire() {
	document.getElementById("erroreditExpire").innerHTML = '';
	 var str = document.getElementById("editExpire").value;
	 if(str != ''){
		 var re = /^((0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2})$/ ; 
		 var res = re.test(str);
	if(res == false)
		$('#erroreditExpire').html("<br/><span style='color:red'>Invalid Date </span>");
	 }
}
$(document).ready(function() { 
	 $('#editExpire').change(function() {
		 checkEditExpire();
		var a = document.getElementById("editExpire").value;
		if(a == ''){
			document.getElementById("editRemaining").innerHTML = 'N/A';    
		}
		else {
			var remaining=  $("#editExpire").val();   
			var now = new Date();
            var date1 =remaining.split('/');         
            var exDate = new Date(date1[2],date1[0]-1,date1[1]);   
            var difftime = exDate.getTime() - now.getTime();
            var diffDays = Math.ceil(difftime / (1000 * 3600 * 24)); 
            if(diffDays >= 0){
                 var diffDayss = diffDays; 
                 $("#editRemaining").val(diffDayss);
              }else{
                  var diffDayss = "Expired";
                  $("#editRemaining").val(diffDayss);
              }         
	       }
	 });
});
//CHECK SNOOZE
function checkEditSnooze() {
	document.getElementById("erroreditSnooze").innerHTML = '';   
	 var str = document.getElementById("editSnooze").value;
	 var re = /^(?:[1-9]|0[1-9]|365)$/;
	 var res = re.test(str);
	 if(res == false)
	 	$('#erroreditSnooze').html("<br/><span style='color:red'>Invalid Snooze</span>");           
}
$(document).ready(function() {
 $('#editSnooze').change(function() {
	 checkEditSnooze();
     });
 });
