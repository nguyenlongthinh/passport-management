    $(document).ready(function () {    	
    	$("#datatable").on("mouseover", "tbody tr", function(event) {
            var table = document.getElementById("datatable");
//            alert(table.rows[$(this).index()+1].cells[0].innerHTML);  
            
            var left  = (event.pageX)+"px";
    	    var top  = (event.pageY - 90)+"px";

    	    var div = document.getElementById("editRow");
//    	    div.style.display = "block";    
    	    div.style.left = left;
    	    div.style.top = top;
    	    
    	    $("#editRow").toggle();
    	    //Set function for each button
    	    var rowIndex = $(this).index() ;
    	    var idOfRow = table.rows[rowIndex].cells[0].innerHTML;
    	    document.getElementById("editInfo").setAttribute("onclick","editInfo("+rowIndex+")");
    	    document.getElementById("deleteInfo").setAttribute("onclick","loadInfoDelete("+idOfRow+","+rowIndex+")");
    	    document.getElementById("submitInfo").setAttribute("onclick","updateInfo("+rowIndex+")");    	    
    	    return false;    		
		});
    });
    
    //UPDATE INFO USER
    function editInfo(i){
        var table = document.getElementById("datatable");
	    var idOfRow = table.rows[i+1].cells[0].innerHTML;
	    var nameOfRow = table.rows[i+1].cells[1].innerHTML;
	    var dobOfRow = table.rows[i+1].cells[2].innerHTML;
	    var emailOfRow = table.rows[i+1].cells[3].innerHTML;
	    var recipientsOfRow = table.rows[i+1].cells[4].innerHTML;
	    var porjectOfRow = table.rows[i+1].cells[5].innerHTML;
	    var passportOfRow = table.rows[i+1].cells[6].innerHTML;
	    var issueOfRow = table.rows[i+1].cells[7].innerHTML;
	    var expireOfRow = table.rows[i+1].cells[8].innerHTML;
	    var remainOfRow = table.rows[i+1].cells[9].innerHTML;
//	    remainOfRow.substring(30,remainOfRow.length-4)
	    var snoozeOfRow = table.rows[i+1].cells[10].innerHTML;
	    var imageofRow = table.rows[i+1].cells[11].innerHTML;
	    $("#erroreditId").html('');
	    $("#erroreditName").html('');
	    $("#erroreditDOB").html('');
	    $("#erroreditEmail").html('');
	    $("#erroreditEmailPm").html('');
	    $("#erroreditProject").html('');
	    $("#erroreditPassport").html('');
	    $("#erroreditIssue").html('');
	    $("#erroreditExpire").html('');
	    $("#erroreditRemaining").html('');
	    $("#erroreditSnooze").html('');
	    
	    $("#editId").val(idOfRow);
	    $("#editName").val(nameOfRow);
	    $("#editDOB").val(dobOfRow);
	    $("#editEmail").val(emailOfRow);
	    $("#editEmailPm").val(recipientsOfRow);
	    $("#editProject").val(porjectOfRow);
	    $("#editPassport").val(passportOfRow);
	    $("#editIssue").val(issueOfRow);
	    $("#editExpire").val(expireOfRow);
	    $("#editRemaining").val(remainOfRow);
	    $("#editSnooze").val(snoozeOfRow);
	    document.getElementById("editImage").innerHTML = imageofRow;
	    }
    
//SAVE INFO UPDATE
    function updateInfo(i){   
        var row = $("#datatable").find("#tr-"+i).index();     
        var id = $("#editId").val();
        var name =  $("#editName").val();
        var dob = $("#editDOB").val(); 
        var email = $("#editEmail").val();
        var emailpm = $("#editEmailPm").val();        
        var project = $("#editProject").val();
        var passport = $("#editPassport").val();
        var issuedate =  $("#editIssue").val();
        var expire = $("#editExpire").val();
        var remaining =  $("#editRemaining").val();
        var snooze =  $("#editSnooze").val();
        
        //check valid value
        if(checkAwsome(name, email, passport, issuedate, row))
            {
            return false;
            }
        //update
        $.ajax({
            url : 'updateUserInfo',
            type : 'GET',
            data : {
                id : id,
                name : name,
                email : email,
                emailpm : emailpm,
                dob : dob,
                project :  project,
                passport : passport,
                issuedate : issuedate,
                expire : expire,
                remaining:remaining,
                snooze : snooze
                    },
            success : function(data) {
                if (data) {
                	var table = document.getElementById("datatable");
            	    table.rows[i+1].cells[0].innerHTML = id;
            	    table.rows[i+1].cells[1].innerHTML = name;
            	    table.rows[i+1].cells[2].innerHTML = dob;
            	    table.rows[i+1].cells[3].innerHTML= email;
            	    table.rows[i+1].cells[4].innerHTML = emailpm;
            	    table.rows[i+1].cells[5].innerHTML = project;
            	    table.rows[i+1].cells[6].innerHTML = passport;
            	    table.rows[i+1].cells[7].innerHTML = issuedate;
            	    table.rows[i+1].cells[8].innerHTML = expire;
            	    table.rows[i+1].cells[9].innerHTML = remaining;
            	    table.rows[i+1].cells[10].innerHTML = snooze;

                } else {
                    alertBox("Update! Fail !!", "danger");
                }
            }
        });
        
        return false;
    }
    
    //RELOAD INFO USER 
    function reloadInfo(i){
        var yn = confirm("Your change will not save. Continue?");
        if(yn)
            {
            //UPDATE
            updateInfo(i);
            }
        if(!yn)
            {
            var row = i+1;
            //id
            var id = $("#datatable tr:eq("+row+") td:eq(0)").html();
            $.ajax({
                url : 'rollBackData',
                type : 'GET',
                data : {
                    id : id
                },
                success : function(data) 
                {
                    if(data)
                        {
                        var infos = data.split(',');
                        //test infos
                        for (b in infos) 
                        {
                            if(infos[b] == "null")
                                {
                                infos[b] = "";//if == "null" convert to ""
                                }
                        }
                        //update info
                        $("#datatable tr:eq("+row+") td:eq(1)").html("");
                        $("#datatable tr:eq("+row+") td:eq(2)").html("");
                        $("#datatable tr:eq("+row+") td:eq(4)").html("");
                        $("#datatable tr:eq("+row+") td:eq(5)").html("");
                        $("#datatable tr:eq("+row+") td:eq(6)").html("");
                        $("#datatable tr:eq("+row+") td:eq(7)").html("");
                        $("#datatable tr:eq("+row+") td:eq(8)").html("");
                        $("#datatable tr:eq("+row+") td:eq(9)").html("");
                        $("#datatable tr:eq("+row+") td:eq(10)").html("");                      

                        //set info
                        $("#datatable tr:eq("+row+") td:eq(1)").html(infos[1]);
                        $("#datatable tr:eq("+row+") td:eq(2)").html(infos[2]);
                        $("#datatable tr:eq("+row+") td:eq(4)").html(infos[4]);
                        $("#datatable tr:eq("+row+") td:eq(5)").html(infos[5]);
                        $("#datatable tr:eq("+row+") td:eq(6)").html(infos[6]);
                        $("#datatable tr:eq("+row+") td:eq(7)").html(infos[7]);
                        $("#datatable tr:eq("+row+") td:eq(8)").html(infos[8]);
                        $("#datatable tr:eq("+row+") td:eq(9)").html(infos[9]);
                        $("#datatable tr:eq("+row+") td:eq(10)").html(infos[10]);
                        
                        if((infos[9] == "N/A") || (infos[9] == "Expired")) 
                        $("#datatable tr:eq("+row+") td:eq(9)").css('color','red');
                        
                        //SET DEFAULT
                        //set default button
                        //show link in current row
                        $("#datatable tbody tr").find("td:last-child").find("a").show();
                        
                        //hide button saving, reloading
                        $("#datatable tbody tr td:last-child").find("a:eq(1), a:eq(3)").hide();
                        
                        //set default html(for date picker)
                        $("#datatable tbody tr td:nth-child(3), td:nth-child(8), td:nth-child(9)").each(function() {
                            
                            if($(this).find('input').length){
                                $(this).html($(this).find('input').val());                                
                            }
                        });
                        //set default for all td(style)
                        $("#datatable").find("tbody>tr").removeClass();
                        $("#datatable").find("tbody>tr td").removeClass();
                        
                        //set default aff td (contentedit table)
                        $("#datatable").find("tbody>tr td").prop('contenteditable', false);
                        $("#datatable tr td:last-child").addClass("showAction");

                        
                        }
                }
            });
            }
        return false;
    }
    
    //UPDATE FORM
    $("#userEditInfoForm")
    .submit(function(e) {
    $("#content").html("");
    e.preventDefault();
        $.ajax({
        type : "GET",
        url : "updateUserInfo",
        data : {
        name : $('#recipient-name').val(),
        project : $('#recipient-project').val(),
        dob : $('#recipient-dob').val(),
        pass : $('#recipient-pass').val(),
        issue : $('#recipient-issue').val(),
        img : $('#recipient-img').val(),
        id : $('#recipient-id').val(),
        email : $('#recipient-email').val(),
        row : $('#recipient-row').val()
            },
        success : function(result) {
        alertBox('Update Successful!','success'); 
                                
        var list = JSON.parse(result[0]);
        var html = '';
        for (var i = 0; i < list.length; i++) {
        var counter = list[i];
        //tru remainning day
         var now = new Date();
         var date1 =list[i].expDate.split('/');           
         var exDate = new Date(date1[2],date1[0]-1,date1[1]);   
         var difftime = exDate.getTime() - now.getTime();
         var diffDays = Math.ceil(difftime / (1000 * 3600 * 24)); 
            if(counter[i].expDate == ""){
                var diffDayss= "N/A";
            }
            else if(diffDays >= 0){
               var diffDayss = diffDays; 
           }else{
               var diffDayss = "Expired";
           }
        var trimmedString = counter.path;
        if( counter.path.length > 14 )
        var trimmedString = counter.path.substring(0, 14) + "..." ;
        if (counter.id == $('#recipient-id').val()) {
        html = '<tr id="updated" style="background: #c5d9ea">';} else {
        html = '<tr>';}
        html += '<td>' + counter.id+ '</td>';
        html += '<td>' + counter.name+ '</td>';
        html += '<td>' + counter.project+ '</td>';
        html += '<td>' + counter.dob+ '</td>';
        html += '<td>' + counter.email+ '</td>';
        html += '<td>' + counter.passport+ '</td>';
        html += '<td>' + counter.isueDate+ '</td>';
        html += '<td>' + counter.expDate+ '</td>';
        html+='<td >';
        if(diffDayss == diffDays){
            html += diffDayss;
        }
        else{
            html+='<div style="color:red">'+diffDayss+'</div>';     
        }
        html+='</td>';
        html+='<td>'+counter.snooze+'</td>';
        html += '<td>' + trimmedString+ '</td>';
        html += '<td class="showAction"><a id="'+ i+ '" onclick="loadInfoEdit('+ counter.id+ ')"  data-toggle="modal" data-target="#exampleModal" title="Chỉnh sửa" style="margin-right: 3%"><span class="glyphicon glyphicon-pencil"></span> </a> <a id="'+ i
        + '" data-toggle="modal" data-target="#modaldelete" onclick="loadInfoDelete(\'' + counter.id + '\','+i+')"  title="Xoa" style="margin-right: 3%"><span class="glyphicon glyphicon-remove"></span> </a></td>';
        html += '</tr>';
        $("#content").append(html);
        }
    },
    error : function(result) {
        alert('errorff' + dataform);
        }
    });
});
    
    
    //DELETE
     var indx = 0;
     //LOAD DELETE:
     function loadInfoDelete(id,index) {
          $("#setid").text(id);
          indx = index;
      }
      //Delete User:
      function deleteUser() {
              var index = indx;
              var id = $("#setid").text();
              $.ajax({
              url : 'deleteUser',
              type : 'GET',
              data : {
              id : String(id)
              },
              success : function(data) {
                      //remove row:
                      $("#datatable").find("#tr-"+index).remove();
                    //set default button
                      //show link in current row
                      $("#datatable tbody tr").find("td:nth-child(10)").find("a").show();
                      
                      //hide button to edit
                      $("#datatable tbody tr td:nth-child(10)").find("a:eq(1), a:eq(3)").hide();
                      
                      //set default html(for date picker)
                      $("#datatable tbody tr td:nth-child(3), td:nth-child(7)").each(function() {
                          
                          if($(this).find('input').length){
                              $(this).html($(this).find('input').val());
                          }
                      });
                      
                      //set border error
                      $("#datatable tr").removeClass();//set nonborder all tr
                      $("#datatable tr td").removeClass();//set nonborder all td column 7
                      
                      //set default for all td(style)
//                      $("#datatable").find("tbody>tr:eq("+row+") td").addClass("default-table");
                      //border
//                      $("#datatable tr:eq("+row+")").addClass("write-border");
                      
                      //set default all td (contentedit table)
                      $("#datatable").find("tbody>tr td").prop('contenteditable', false);
                      $("#datatable tr td:last-child").addClass("showAction");

                      pager.destroy('pageNavPosition');
                      var table, rows;
                      table = document.getElementById("datatable");
                      rows = table.getElementsByTagName("TR");
                      
                      //warning
                      pager = new Pager(rows.length,$("#itemperpage").val());             
                      pager.init('datatable'); 
                      pager.showPageNav('pager', 'pageNavPosition'); 
                      pager.showPage(1);
                      }
               });
//              $('#modaldelete').modal('hide'); 
     }
  
