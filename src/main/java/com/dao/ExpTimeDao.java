package com.dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class ExpTimeDao {

	String fileName =  System.getProperty("user.home") + "/passport/data/timer.csv";
	
	//check timer exist true-exist false-noexist
	public boolean checkTimer(String fileTimer){
		File f = new File(fileTimer);
		if(f.exists() && !f.isDirectory()) { 
			return true;
		} else {
			return false;
		}
	}
	
	//get time expire day
	public int getTimer(){
		String line = "";
		int timer = 0;
		try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
			while ((line = br.readLine()) != null) {
				timer = Integer.parseInt(line);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return timer;
	}
	
	//create new timer csv
	public String createExpTime(String timer){
		//notification
		String note = "success";
		FileWriter fw = null;
		try {
			
			fw = new FileWriter(fileName);
			//Write a new user object list to the CSV file
			fw.append(timer);
			
		} catch (Exception e) {
			System.out.println("Error in CsvFileWriter !!!");
			note="faile";
			e.printStackTrace();
		} finally {
			
			try {
				fw.flush();
				fw.close();
			} catch (IOException e) {
				System.out.println("Error while flushing/closing fileWriter !!!");
				note="faile";
                e.printStackTrace();
			}
			
		}
		return note;
	}
	
//	update timer expire date
	public String updateExpTime(String timer, String fileNameTimer){
		//notification
		String note = "success";
		File f = new File(fileName);
		
		if(this.checkTimer(fileNameTimer)){
			try
			{
				//delete file
				f.delete();
				
				//create new file
			    FileWriter fw = new FileWriter(fileName,true); //the true will append the new data
				//Write a new user object list to the CSV file
				fw.append(timer);
		
			    fw.close();
			}
			catch(IOException ioe)
			{
			    System.err.println("IOException: " + ioe.getMessage());
			    note = "faile";
			}
		} else {
			note="faile";
		}
		
		return note;
	}
}
