package com.dao;

import com.controller.CsvWriterController;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.model.User;

import java.util.List;
import java.util.regex.Pattern;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sound.midi.Soundbank;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.formula.ptg.Ptg;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLDecoder;

import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

public class UserDao {
	String fileName = System.getProperty("user.home") + "/passport/data/user.csv";
	String line = "";
	String cvsSplitBy = ",";

	
	@Autowired
    ServletContext context;
	
	public String getPath() throws UnsupportedEncodingException {

		String path = this.getClass().getClassLoader().getResource("").getPath();

		String fullPath = URLDecoder.decode(path, "UTF-8");

		String pathArr[] = fullPath.split("/target/classes/");
		return pathArr[0];

	}
	
	public String getPath2() throws UnsupportedEncodingException {
		   File file = new File("");
		   return "";

		}
	// ham tra ve chuoi Json:
	public String getJson(List<User> users) throws JsonProcessingException {
		// Su dung Json:
		ObjectMapper mapper = new ObjectMapper();
		String jsonArray = mapper.writeValueAsString(users);
		return jsonArray;
	}

	// ham tra ve List cac User:
	public List<User> getAllUser() {
		int i = 0;
		List<User> listall = new ArrayList<>();
		try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
			  br.readLine();
			  
			while ((line = br.readLine ()) != null) {
				// use comma as separator
				String[] country = line.split(cvsSplitBy);
				String sourceImg = System.getProperty("user.home") + "/passport/img/"+country[0]+".png";
				String sourceImg1 = System.getProperty("user.home") + "/passport/img/"+country[0]+".jpg";
				String sourceImg2 = System.getProperty("user.home") + "/passport/img/"+country[0]+".pdf";
				System.out.println(sourceImg2);
				i++;
				if(country[11].equals("No Image")){
					File f = new File(sourceImg);
					File f1 = new File(sourceImg1);
					File f2 = new File(sourceImg2);

					if(f.exists()){
						listall.add(new User(country[0], country[1], country[2], country[3], country[4], country[5], country[6],
							    country[7], country[8],country[9],Integer.parseInt(country[10]),country[0]+".png"));
						System.out.println(country[0]+".png");					
					}
					else if(f1.exists()){
						listall.add(new User(country[0], country[1], country[2], country[3], country[4], country[5], country[6],
							    country[7], country[8],country[9],Integer.parseInt(country[10]),country[0]+".jpg"));
						System.out.println(country[0]+".jpg");
					}
					else if(f2.exists()){
						System.out.print("position = "+i+"adad");
						listall.add(new User(country[0], country[1], country[2], country[3], country[4], country[5], country[6],
							    country[7], country[8],country[9],Integer.parseInt(country[10]),country[0]+".pdf"));		
					}
					else {listall.add(new User(country[0], country[1], country[2], country[3], country[4], country[5], country[6],
						    country[7], country[8],country[9],Integer.parseInt(country[10]),country[11]));

					}
				}				
				else{
					listall.add(new User(country[0], country[1], country[2], country[3], country[4], country[5], country[6],
						    country[7], country[8],country[9],Integer.parseInt(country[10]),country[11]));
				}					
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		File file = new File(fileName);
		// tien hanh xoa file csv:
		file.delete();
		// Them lai vao co so du lieu:
		for (User user2 : listall) {
			CsvWriterController.writeCsvFile(fileName, user2.getId(), user2.getName(), user2.getDob(), user2.getEmail(),user2.getEmailpm(),
					user2.getProject(), user2.getPassport(), user2.getIsueDate(), user2.getExpDate(),user2.getRemaining(),user2.getSnooze(), user2.getPath());
		}
		return listall;
	}
	
	// get user by id
	public String getUser(String id) {
		// open csv
		String user = "";
		try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
			  br.readLine();
			while ((line = br.readLine()) != null) {
				// use comma as separator
				String[] users = line.split(cvsSplitBy);
				if (users[0].equals(id)) {
					user = line;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return user;
	}

	// update info user
		public String updateAndReturnExp(String id, String name, String dob, String email,String emailpm, String project,
				String passport, String isueDate,String expire,String remaining,int snooze) {
			String path = null;
			int kt = 0;
			//passport
			if(passport == null || passport == "")
			{
				passport = "";
			}
			if(project == null || project == "")
			{
				project = "";
			}
			//if issuedate null => expdate null
			if(isueDate == null || isueDate == "")
			{
				expire = "";
				isueDate = "";
				remaining="0";
			} 
			List<User> listall = new ArrayList<User>();
			try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
				  br.readLine();
				while ((line = br.readLine()) != null) {
					// use comma as separator:
					String[] country = line.split(cvsSplitBy);
					listall.add(new User(country[0], country[1], country[2], country[3], country[4], country[5], country[6],
						    country[7], country[8],country[9],Integer.parseInt(country[10]),country[11]));
				}
			} catch (Exception e) {
				System.out.println("loi o day"+e.getMessage());
			}
			// su dung vong lap chay qua kiem tra:
			for (int i = 0; i < listall.size(); i++) {
				if (listall.get(i).getId().equals(id)) {
					kt= i;
					path = listall.get(i).getPath();
//					snooze = listall.get(i).getSnooze();
				}
			}
			// xoa user tai vi tri:
			listall.remove(kt);
			User user = new User(id, name, dob, email,emailpm, project, passport, isueDate,expire,remaining, snooze, path);
			listall.add(kt, user);
			
			System.out.println(user);
			File file = new File(fileName);
			// tien hanh xoa file csv:
			file.delete();
			// Them lai vao co so du lieu:
			for (User user2 : listall) {
				CsvWriterController.writeCsvFile(fileName, user2.getId(), user2.getName(), user2.getDob(), user2.getEmail(),user2.getEmailpm(),
						user2.getProject(), user2.getPassport(), user2.getIsueDate(), user2.getExpDate(),user2.getRemaining(),user2.getSnooze(), user2.getPath());
			}
			return id;
		}

	// Upload IMG:
    public String uploadFileImg(MultipartFile file, String id, String path) throws IOException {
        System.out.println("Work here 1");

        DefaultResourceLoader loader = new DefaultResourceLoader(); 
        String ms = "";
        File dir = new File(new UserDao().getPath());
        ClassLoader classLoader = getClass().getClassLoader();
        System.out.println("Work here 2");
        if (!file.isEmpty()) {
            try {
                byte[] bytes = file.getBytes();
                File upFile = new File(path);
                System.out.println(upFile.getAbsolutePath());
                if (!upFile.exists())
                upFile.mkdirs();	                

                // Create the file on server
                File serverFile = new File(upFile.getAbsolutePath() + File.separator + file.getOriginalFilename());
                
                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
                stream.write(bytes);
                stream.close();
                System.out.println("123321"+System.getProperty("user.home") + "/passport/img/" + file.getOriginalFilename());
                if(id != ""){
                	File oldfile =new File(System.getProperty("user.home") + "/passport/img/" + file.getOriginalFilename());
                    File newfile =new File(System.getProperty("user.home") + "/passport/img/" + id +file.getOriginalFilename().substring(file.getOriginalFilename().length() - 4, file.getOriginalFilename().length()));
                    oldfile.renameTo(newfile);
                }
        		
                return "Upload file OK";

            } catch (Exception e) {
                return e.getMessage();
            }

        } else {
            return "UPload file Faile";
        }
        
    }
	
	    static void xls(File inputFile, File outputFile) 
	    {
	            // For storing data into CSV files
	            StringBuffer data = new StringBuffer();
	            try 
	            {
	            FileOutputStream fos = new FileOutputStream(outputFile);

	            // Get the workbook object for XLS file
	            HSSFWorkbook workbook = new HSSFWorkbook(new FileInputStream(inputFile));
	            
	            // Get first sheet from the workbook
	            HSSFSheet sheet = workbook.getSheetAt(0);
	            Cell cell;
	            Row row;
	            
	            // Iterate through each rows from first sheet
	            Iterator<Row> rowIterator = sheet.iterator();
	            int numrow = 0;
	            while (rowIterator.hasNext()) 
	            {	            		
	                    row = rowIterator.next();

	                    // For each row, iterate through each columns
	                    int i = 0;
	                    Iterator<Cell> cellIterator = row.cellIterator();
	                    String issue_temple = "";
                        String expire_temple = "";
                        String remaining_temple = "";
	                    while (cellIterator.hasNext()) 
	                    {                   		
	                            cell = cellIterator.next();       
	                            System.out.println("i="+i);
	                            switch (cell.getCellType()) 
	                            {
	                            case Cell.CELL_TYPE_BOOLEAN:	                            		
	                            		i++;
	                                    data.append(cell.getBooleanCellValue() + ",");
	                                    break;                                   

	                            case Cell.CELL_TYPE_NUMERIC:
		                            	if(i == 2 || i == 7 || i == 8){		 
		                            		if( cell.toString() == ""){
		                            			data.append(cell + ",");			                            		
			                            		i++;
		                            		}
		                            		else {
		                            			DateFormat df1 = new SimpleDateFormat("dd-MMM-yyyy"); // for parsing input
			                            		DateFormat df2 = new SimpleDateFormat("MM/dd/yyyy");  // for formatting output
			                            		Date d;
		                            			if(cell.toString().matches("(.*).0(.*)") == true){
		                            				i++;
		                            				if(i == 3){			                            					
		                            					if(cell.toString().matches("(.*)-(.*)") == true)
		                            						{
		                            						d = df1.parse(cell.toString());
			                            					String outputDate = df2.format(d);
			                            					data.append(outputDate + ",");
		                            						}
		                            					else {
		                            						data.append(cell.toString().substring(0, 4) + ",");
		                            					}
		                            					
		                            				}
		                            				else if(i== 8){
		                            					d = df1.parse(cell.toString());
		                            					String outputDate = df2.format(d);	
		                            					Date nowdate = new Date();
				                            			Calendar c = Calendar.getInstance();
				                            			c.setTime(d);				                            			
				                            			c.add(Calendar.YEAR, 10);				                            			
				                            			issue_temple = outputDate;
				                            			expire_temple = df2.format(c.getTime());
				                            			remaining_temple = String.valueOf((int)( (c.getTime().getTime() - nowdate.getTime()) / (1000 * 60 * 60 * 24)));
				                            			System.out.print("remdADADain = "+outputDate);
		                            					data.append(outputDate + ",");
		                            				}		                            					
		                            				break;
		                            			}		                            				 
		                            			else  d = df1.parse(cell.toString());
				                            		String outputDate = df2.format(d);		                            		
				                            		if(i == 7){
			                            			Date nowdate = new Date();
			                            			Calendar c = Calendar.getInstance();
			                            			c.setTime(d);
			                            			c.add(Calendar.YEAR, 10);
			                            			issue_temple = outputDate;
			                            			expire_temple = df2.format(c.getTime());
			                            			remaining_temple = String.valueOf((int)( (c.getTime().getTime() - nowdate.getTime()) / (1000 * 60 * 60 * 24)));
			                            			System.out.print("remain = "+remaining_temple);		                            			
		                            		}
		                            		else if(i == 8){
		                            			Date nowdate = new Date();		                            			
		                            			expire_temple = outputDate;
		                            			remaining_temple = String.valueOf((int)( (d.getTime() - nowdate.getTime()) / (1000 * 60 * 60 * 24)));
		                            			System.out.print("remain = "+remaining_temple);		                            			
		                            		}
		                            		data.append(outputDate + ",");		                            		
		                            		i++;
		                            		
		                            		}
		                            	}
		                            	else {
		                            		i++;	
		                            		String temple_String = new BigDecimal(cell.getNumericCellValue()).toString();
		                            		if(temple_String.length() == 5){
		                            			temple_String = "0" + new BigDecimal(cell.getNumericCellValue()).toString();
		                            		}
		                                    data.append( temple_String + ",");
		                            	}
	                                    break;	                                    

	                            case Cell.CELL_TYPE_STRING:
	                            		System.out.println(numrow);
	                            		if(numrow == 0){
	                            			if(i == 2 || i == 7 || i == 8){
			                            		data.append(cell + ",");			                            		
			                            		i++;
				                            }	                            			
			                            	else {
			                            		i++;
			                                    data.append(cell.getStringCellValue() + ",");
			                            	}	           
	                            		}
	                            		else{
	                            			if(i == 2 || i == 7 || i == 8){
	                            				if( cell.toString().equals("")){
			                            			data.append(cell + ",");			                            		
				                            		i++;
			                            		}
	                            				else {
		                            				try {	                            			                             
		                            					  DateFormat df1 = new SimpleDateFormat("dd-MMM-yyyy"); // for parsing input
		                            					  DateFormat df2 = new SimpleDateFormat("MM/dd/yyyy");  // for formatting output
		                            					  String outputDate = df2.format(df1.parse(cell.toString()));
		                            					  Date d = df2.parse(outputDate);                          						
		                            					  if(i == 7){
		                            						    Date nowdate = new Date();
		                            						    Calendar c = Calendar.getInstance();
		                            						    c.setTime(d);
		                            						    c.add(Calendar.YEAR, 10);
		                            						    issue_temple = outputDate;
		                            						    expire_temple = df2.format(c.getTime());
		                            						    remaining_temple = String.valueOf((int)( (c.getTime().getTime() - nowdate.getTime()) / (1000 * 60 * 60 * 24)));
		                            					  }
		                            					  else if(i == 8){
		                            						  Date nowdate = new Date();                                      
		                            					      expire_temple = outputDate;
		                            					      remaining_temple = String.valueOf((int)( (d.getTime() - nowdate.getTime()) / (1000 * 60 * 60 * 24)));
		                            					  }                            					  
		                            						  data.append(outputDate + ",");                                      
		 	                            					  i++;                            					  
			                            				}
			                            				catch(ParseException pe){
			                            					  DateFormat df1 = new SimpleDateFormat("dd/MM/yyyy"); // for parsing input
			                            					  DateFormat df2 = new SimpleDateFormat("MM/dd/yyyy");  // for formatting output
			                            					  System.out.print("Date ="+cell);

			                            					  if(!(cell.toString().indexOf('/') > 0)){
				                            					  System.out.print("parse ở đây"+cell.toString());
			  	                            					  df1 = new SimpleDateFormat("dd MMM yyyy"); // for parsing input
			                            					  }
			                            					 String outputDate = df2.format(df1.parse(cell.toString()));
			                            					 data.append(outputDate + ",");
			                            					 Date nowdate = new Date();
			                            					 Date d_temple = df2.parse(outputDate);
		                         						     Calendar c = Calendar.getInstance();
		                         						    c.setTime(d_temple);
		                         						    c.add(Calendar.YEAR, 10);
		                         						    issue_temple = outputDate;
		                         						    expire_temple = df2.format(c.getTime());
		                         						    remaining_temple = String.valueOf((int)( (c.getTime().getTime() - nowdate.getTime()) / (1000 * 60 * 60 * 24)));
			                            					 i++;
			                            				}
	                            				}
	                            			}
	                            			else {
	                            				data.append(cell + ",");                                     
	                   					 		i++;
	                            			}	                            				        
	                            		}		                            	                 		
	                                    break;

	                            case Cell.CELL_TYPE_BLANK:
	                            		if(i == 8 && issue_temple != ""){
	                            			data.append(expire_temple + ",");
	                            			i++;
	                            		}
	                            			
	                            		else if(i == 9 && expire_temple == ""){
	                            			data.append("N/A" + ",");
	                            			i++;
	                            		}
	                            			
	                            		else if(i == 9 && expire_temple != ""){
	                            			i++;
	                            			if(Integer.parseInt(remaining_temple) >= 0) data.append(remaining_temple + ",");
	                            			else data.append("Expired" + ",");
	                            		}	                            			
	                            		else if(i == 10){
	                            			data.append("0" + ",");
	                            			i++;
	                            		}
	                            			
	                            		else if(i == 11){
	                            			data.append("No Image" + ",");
	                            			i++;
	                            		}	                            			
	                            		else {
	                            			data.append(",");
	                            			i++;
	                            		}
	                            		
	                            		
	                                    break;	                            

	                            default:
	                            		i++;
	                            		if(i == 12)
	                            			data.append("No Image,");
	                            		else data.append(cell + ",");
	                            }
	                            
	                    }
	                    data.append('\n');
	                    numrow++;
	            }
	            fos.write(data.toString().getBytes());
	            fos.close();
	            }

	            catch (FileNotFoundException e) 
	            {
	                    e.printStackTrace();
	            }

	            catch (IOException e) 
	            {
	                    e.printStackTrace();

	            } catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

	            }

	    

	    // Upload CSV:

	        public String uploadFileCSV(MultipartFile file, String path) throws IOException {

	            DefaultResourceLoader loader = new DefaultResourceLoader(); 

	            String ms = "";

	            File dir = new File(new UserDao().getPath());

	            ClassLoader classLoader = getClass().getClassLoader();

	            if (!file.isEmpty()) {

	                File convFile = new File(file.getOriginalFilename());

	                convFile.createNewFile(); 

	                FileOutputStream fos = new FileOutputStream(convFile); 

	                fos.write(file.getBytes());

	                try {

	                    String pathstring = System.getProperty("user.home");

	                    byte[] bytes = file.getBytes();

	                    File user = new File(pathstring +File.separator + "/passport/data/user.csv");

	                    File upFile = new File(pathstring);
	                    
	                    System.out.println(pathstring);

	                    if (!user.exists())

	                    user.delete();

	                    

	                    // Create the file on server

	                    File serverFile = new File(upFile.getPath());

	                    xls(convFile, user);

//	                  BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(user));

//	                  stream.write(bytes);

//	                  stream.close();

//	                  System.out.println(upFile.getAbsolutePath() +  File.separator + file.getOriginalFilename());

	                    return "Upload file OK";

	                } catch (Exception e) {

	                    return e.getMessage();

	                }

	            } else {

	                return "Upload file Faile";

	            }

	        }
		
		public static double[][] importData(String fileName, int tabNumber) throws InvalidFormatException, IOException{
	  double[][] data;
	 
	        //Create Workbook from Existing File
	        InputStream fileIn = new FileInputStream(fileName);
	        Workbook wb = WorkbookFactory.create(fileIn);
	        Sheet sheet = wb.getSheetAt(tabNumber);
	 
	        //Define Data & Row Array and adjust from Zero Base Numer
	        data = new double[sheet.getLastRowNum()+1][];
	        Row[] row = new Row[sheet.getLastRowNum()+1];
	        Cell[][] cell = new Cell[row.length][];
	 
	        //Transfer Cell Data to Local Variable
	        for(int i = 0; i < row.length; i++)
	        {
	            row[i] = sheet.getRow(i);
	 
	            //Note that cell number is not Zero Based
	            cell[i] = new Cell[row[i].getLastCellNum()];
	            data[i] = new double[row[i].getLastCellNum()];
	 
	            for(int j = 0; j < cell[i].length; j++)
	            {
	                cell[i][j] = row[i].getCell(j);
	                data[i][j] = cell[i][j].getNumericCellValue();
	            }
	 
	        }
	 
	        fileIn.close();
	        System.out.println(data);
	        return data;
	    }
	 
		
	//EXPORT EXCELS
	//Export all rows to excel
	public void exportToExcelAll(String path) throws FileNotFoundException{

		//set file
				String outputFile = path;

//				covert .csv to arraylist<arraylist>
		        ArrayList<ArrayList<String>> arList = new ArrayList<ArrayList<String>>();
//		        convert row csv to array list
		        ArrayList<String> al = null;

		        String thisLine;
		        DataInputStream myInput = new DataInputStream(new FileInputStream(fileName));
		        
		        //read file csv
		        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
		        	  br.readLine();
					while ((thisLine = br.readLine()) != null) {
						al = new ArrayList<String>();
						// use comma as separator
						String[] user = thisLine.split(",");
						for (int j = 0; j < user.length; j++) {
			                // My Attempt (BELOW)
							
			                String edit = user[j].replace('\n', ' ');
			                al.add(edit);
			            }
						arList.add(al);
					}
					
					try {
			            HSSFWorkbook hwb = new HSSFWorkbook();
			            HSSFSheet sheet = hwb.createSheet("Passport management");
			            
			            final HSSFCellStyle style = hwb.createCellStyle();
			            style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			            style.setBorderTop(HSSFCellStyle.BORDER_THIN);
			            style.setBorderRight(HSSFCellStyle.BORDER_THIN);
			            style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			            
			            HSSFRow header = sheet.createRow(0);
			            header.createCell(0).setCellValue("ID");
			            header.createCell(1).setCellValue("Name");
			            header.createCell(2).setCellValue("Day of birth");
			            header.createCell(3).setCellValue("Email");
			            header.createCell(4).setCellValue("Recipients");
			            header.createCell(5).setCellValue("Project");
			            header.createCell(6).setCellValue("Passport No.");
			            header.createCell(7).setCellValue("Issue Date");
			            header.createCell(8).setCellValue("Expire Date");
			            header.createCell(9).setCellValue("Remaining Day");
			            header.createCell(10).setCellValue("Snooze");
			            header.createCell(11).setCellValue("Image Path");
			            
			            
			            //chay qua tung dong cua arraylist
			            //tao dong moi tren excel
			            //tren moi dong tao cell roi add gia tri dzo
			            for (int k = 0; k < arList.size(); k++) {
			                ArrayList<String> ardata = (ArrayList<String>) arList.get(k);
			                //tao dong moi tren excel
			                HSSFRow row = sheet.createRow(k+1);

			                for (int p = 0; p < ardata.size(); p++) {
			                    System.out.print(ardata.get(p));
			                    //tao o moi tren excel
			                    HSSFCell cell = row.createCell(p);
			                    //gam gia tri cho cell vua tao
			                    cell.setCellValue(ardata.get(p).toString());
			                    cell.setCellStyle(style);
			                }
			            }
                        File file = new File(path);
                        if(!file.exists()){
                           file.mkdirs();
                        }
                       FileOutputStream fileOut = new FileOutputStream(file.getAbsolutePath()+"/user.xls");
				       hwb.write(fileOut);
				       fileOut.close();
				       System.out.println("Success"+outputFile);
                       
			            
			        } catch (Exception ex) {
			        }

				} catch (Exception e) {

		}
	}
	
	//Export rows in table to excel 
		public void exportToExcel(String path, List<String> datas, int token) throws FileNotFoundException{

			//set file
					String outputFile = path;
  
			        try {
			            HSSFWorkbook hwb = new HSSFWorkbook();
			            HSSFSheet sheet = hwb.createSheet("Passport management");
			            HSSFRow header = sheet.createRow(0);
			            header.createCell(0).setCellValue("ID");
			            header.createCell(1).setCellValue("Name");
			            header.createCell(2).setCellValue("Day of birth");
			            header.createCell(3).setCellValue("Email");
			            header.createCell(4).setCellValue("Recipients");
			            header.createCell(5).setCellValue("Project");
			            header.createCell(6).setCellValue("Passport No.");
			            header.createCell(7).setCellValue("Issue Date");
			            header.createCell(8).setCellValue("Expire Date");
			            header.createCell(9).setCellValue("Remaining Day");
			            header.createCell(10).setCellValue("Snooze");
			            header.createCell(11).setCellValue("Image Path");
			            
			            if(token==1)
			            {//export a row in current table
			            	
			            	//create new row
			        		HSSFRow row = sheet.createRow(1);
			        		
			            	for (int i = 0; i < datas.size(); i++) 
			            	{
			        			//create new cell
			                    HSSFCell cell = row.createCell(i);
	
				                    if(i==0)
				                    {//first cell
				                    	datas.set(i, datas.get(i).substring(2, datas.get(i).length()-1));
				                    } else if(i==datas.size()-1) 
				                    {//last cell
				                    	datas.set(i, datas.get(i).substring(1, datas.get(i).length()-2));
				                    } else
				                    {//other cell
				                    	datas.set(i, datas.get(i).substring(1, datas.get(i).length()-1));
				                    }
				                    //set value for cell
				                    cell.setCellValue(datas.get(i).toString());
				        	}
			            	
			            } else
			            	
			                {
			            	//export rows in current table
			            	
			            	for (int i = 0; i < datas.size(); i++) 
			            	{
			            		//create new row
				        		HSSFRow row = sheet.createRow(i+1);
				        		//Split to array String
				        		String info[] = datas.get(i).split(",");
				        		
				        		for(int j=0; j < info.length; j++)
				        		{
				        			//create new cell
				                    HSSFCell cell = row.createCell(j);
				                    if(j==0)
				                    {//first cell
				                    	info[j] = info[j].substring(2, info[j].length()-1);
				                    } else if(j==info.length-1)
				                    {//last cell
				                    	info[j] = info[j].substring(1, info[j].length()-2);
				                    } else
				                    {//other cell
				                    	info[j] = info[j].substring(1, info[j].length()-1);
				                    }
				                    //set value for cell
				                    cell.setCellValue(info[j].toString());
				        		}
				        	}
			            }
			            //save file to path
			            File file = new File(path);
	                      if(!file.exists())
	                      {
	                         file.mkdirs();
	                      }
	                   //export file
	                   FileOutputStream fileOut = new FileOutputStream(file.getAbsolutePath()+"/user.xls");
				       hwb.write(fileOut);
				       fileOut.close();
				       System.out.println("Success"+outputFile);
			            
			        } catch (Exception ex) {
			        }
			        
		}
		
	//get user by id
    public User getUserById(String id){
    	// open csv
    			User user = null;
    			try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
    				  br.readLine();
    				while ((line = br.readLine()) != null) {
    					// use comma as separator
    					String[] users = line.split(cvsSplitBy);
    					if (users[0].equals(id)) {
    						user = new User(id, users[1], users[2], users[3], users[4], users[5], users[6], users[7], users[8], users[9],Integer.parseInt(users[10]),users[11]);
    					}
    				}

    			} catch (Exception e) {
    				e.printStackTrace();
    			}
    			return user;
    }
    
    //Function add 10 year
    public String addYear(String issue) throws ParseException{
    	DateFormat parser = new SimpleDateFormat("dd/MM/yyyy");
		Date date = (Date) parser.parse(issue);
		DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.YEAR, 10);
		Date newDate = c.getTime();
		String expdate = formatter.format(newDate);
    	return expdate;
    }
   
    
	public List<User> getPagedUsers(int pageNo, int recordOffset) {
		List<User> list = getAllUser();
		List<User> list2 = new ArrayList<User>();
		int j = 0;
		int no = pageNo;
		if (no == 1) {
			j = 1;
		} else {
			if (recordOffset == 30) {
				j = ((no - 1) * 30) + 1;
			} else if (recordOffset == 20) {
				j = ((no - 1) * 20) + 1;
			} else {
				j = ((no - 1) * 10) + 1;
			}
		}

		for (int i = j - 1; i < (j - 1) + recordOffset; i++) {
			if (list.size() > i) {
				list2.add(list.get(i));
			}
		}
		return list2;
	}

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out = response.getWriter();
		try {
			String strPageNo = request.getParameter("pageNo");
			String styleClass = "";
			int pageNo = Integer.parseInt(strPageNo);
			String strOffset = request.getParameter("size");
			int recordOffset = Integer.parseInt(strOffset);
			User studentMaster = new User();
			List<User> user = getPagedUsers(pageNo, recordOffset);
			if (!user.isEmpty()) {
				for (int i = 0; i < user.size(); i++) {
					if (i % 2 == 0) {
						styleClass = "bgColor-white";
					} else {
						styleClass = "bgColor-Green";
					}
					out.print("<tr class='" + styleClass + "'>");
					out.print("<td align=\"center\">" + ((pageNo - 1) * 10 + (i + 1)) + "</td>");
					out.print("<td align=\"center\">" + user.get(i).getId() + "</td>");
					out.print("<td align=\"center\">" + user.get(i).getEmail() + "</td>");
					out.print("<td align=\"center\">" + user.get(i).getName() + "</td>");
					out.print("<td align=\"center\">" + user.get(i).getDob() + "</td>");
					out.print("<td align=\"center\">" + user.get(i).getProject() + "</td>");
					out.print("<td align=\"center\">" + user.get(i).getPassport() + "</td>");
					out.print("<td align=\"center\">" + user.get(i).getIsueDate() + "</td>");
					out.print("<td align=\"center\">" + user.get(i).getExpDate() + "</td>");
					out.print("<td align=\"center\">" + "</td>");

					out.print("</tr>");
				}
			} else {
				out.print("<tr>");
				out.print(
						"<td id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-left\" style=\"color: red;font-weight: bold\" colspan=\"6\">No Record Found!</td>");
				out.print("</tr>");
			}
			int totalPages = 1;
			if (user.size() > 0) {
				int rowCount = getAllUser().size();
				totalPages = (int) Math.ceil(Math.ceil(rowCount) / recordOffset);
				if (totalPages == 0) {
					totalPages = 1;
				}
			}
			out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
		} finally {
			out.close();
		}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	public String getServletInfo() {
		return "Short description";
	}
	public static void main(String [] args) throws IOException{
		   System.out.println(new UserDao().getPath());
	   }

}
